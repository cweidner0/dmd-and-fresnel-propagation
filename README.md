# DMD and Fresnel propagation

Initial goal:
Given Rb-87 atoms trapped in a three-dimensional optical lattice and imaged with a 0.7 NA microscope objective.
We can also project potentials onto the atoms using DMDs and propagate that potential through space using Fresnel diffraction.

Now:
The collaborations folder contains files used for the following:
DMD potential generation work with F.C. Langbein and students (Anastasia), Cardiff University
Sierpinski carpet lattice work with A.E.B. Nielsen, MPQ Dresden and Aarhus University
Single-shot atom location detection work with A. Alberti, Bonn University

The code is thus much more general now than its initial goals. The codebase is mostly MATLAB, but there exist
some Mathematica code and other files that were used for testing, etc. Gitignore is ignoring .mat files.

The tomography and deconvolution folder contains files used for the tomography paper, including a few failed explorations (e.g. the linear algebraic
description of the atoms' positions).

The testfiles folder just contains a bunch of tests used in the past.

The general folder is probably where one should start if they want to put together a new project, unless you wish to use deconvolution
in any way, in which case you can find relevant files in the tomography/deconvolution folder.

Once you start a new project, you should copy the relevant starter files into a given folder and go from there.
Do not pollute the files in the general folder.

If you are going to add code to the repository:
-BRANCH THE REPO BEFORE MAKING CHANGES
-COMMENT YOUR CODE
-ORGANIZE YOUR CODE
-EXPLAIN YOUR CODE IN A RELEVANT FILE
-DO NOT PUT GB OF DATA INTO THE REPO. IT BREAKS THE REPO AND MAKES ME SAD.
-BE CAREFUL WHEN YOU MERGE.

I am not a git expert, and I don't expect that most of the users of this code are, but it is your responsibility
to not nuke anything or destroy anything. I can and will revoke access if I need to.

Questions/comments? Write Carrie at cweidner@phys.au.dk

Note: if you use this code, it is YOUR responsibility to perform checks to ensure that your code aligns with
experimental or theoretical results. This may seem obvious, but it's easy to get fooled by code that diffracts
too quickly or too slowly.

This code does make use of some code written by others that is freely available on the internet. This code is cited below:

M. Ferrer, “MATLAB Central File Exchange: Fresnel propagation using the transfer function,”
https://www.mathworks.com/matlabcentral/fileexchange/72389-fresnel-propagation-using-the-transfer-function ,
(2021), accessed: 2021-02-03.

J. Burkardt, “Laguerre polynomials,”
https://people.sc.fsu.edu/~jburkardt/m_src/laguerre_polynomial/laguerre_polynomial.html ,
(2021), accessed: 2021-02-04.
