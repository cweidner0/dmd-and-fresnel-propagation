%Script to simulate the images from the 0.92 NA microscope objective used
%in A. Alberti's group
%This script puts one atom in the center of the lattice which makes it easy
%to look at PSFs
%Carrie Weidner
%Aarhus University
%3 May 2020

close all; clc; clear all;
rng('shuffle')

file = 'test_img_proc';

%% Options

% add phase front (model aberrations)
do_phase_front = 1;

n_imgs = 1; % number of images to generate

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 �m field of view)
px_x = 1024;
% the total span of each image is about 63 �m, so 32 �m in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
zlim = 0.5;
dz = zlim/2;
% zlim = LAMBDA_LATT_SC; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
% dz = LAMBDA_LATT_SC/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = -4*zlim:dz:0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    % add Zernike polynomials phase front
    % https://en.wikipedia.org/wiki/Zernike_polynomials
    % scale by what I assume is the exit pupil a = zp*tan(asin(NA)))
    % update 200814: this should be f, as per W. Alt's suggestion
    % where zp = 150 �m is the working distance and NA is 0.92
    %     sc1 = f*tan(asin(NA));
    %     sc2 = zp*tan(asin(NA));
    sc = max(max(X2D));
    XZ = X2D/sc;
    YZ = Y2D/sc;
    %     XZ = X2D/sqrt(max(max(X2D))/2);
    %     YZ = Y2D/sqrt(max(max(Y2D))/2);
    %     XZ = X2D;
    %     YZ = Y2D;
    Rmax = max(max(XZ));
    R2D_sq = (XZ).^2 + (YZ).^2; % radius squared
    R2D_sq_minus = (XZ).^2 - (YZ).^2; % this guy pops up a lot too
    Zp20 = sqrt(3)*(2*R2D_sq - 1); %defocus
    Zp2n2 = 2*sqrt(6)*XZ.*YZ; % oblique astigmatism
    Zp22 = sqrt(6)*R2D_sq_minus; % vertical astigmatism
    %     Zp3n1 = sqrt(8)*(3*R2D_sq*YZ-2*YZ); % y-coma
    Zp31 = sqrt(8)*(3*R2D_sq.*XZ-2*XZ); % x-coma
    Zp33 = sqrt(8)*(4*XZ.^3 - 3*R2D_sq.*XZ); % trefoil
    Zp40 = sqrt(5)*(6*R2D_sq.^2 - 6*R2D_sq + 1); % primary spherical aberration
    Zp42 = sqrt(10)*(4*R2D_sq - 3).*R2D_sq_minus; % vertical secondary astigmatism
    Zp = {Zp20, Zp22, Zp31, Zp33, Zp40, Zp42}; %0.05
    
    phase_factor = zeros(size(X2D));
    
    ratio = 14096.7; % calibration ratio
    
%     coeffs = [0.013,0,0,0,0,0]*ratio;
    coeffs = [70.1,0,0,0,0,0];
    %     coeffs = [0.07, -0.01, -0.004, -0.001, 0.006, -0.015];
    
    for i = 1:length(Zp)
        Zp_cont = coeffs(i)*Zp{i};
        phase_factor = phase_factor + Zp_cont;
        Zp_cont(R2D_sq > Rmax^2) = NaN;
        delta(i) = max(max(Zp_cont)) - min(min(Zp_cont));
    end

    E = exp(1i*2*pi*phase_factor);
    
end

% actually generate PSF
% this PSF is the typical Bessel beam multiplied by a small hyperbolic
% tangent component that mimics the asymmetry shown in Fig. 3 of
% https://arxiv.org/pdf/1611.02159.pdf
% PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);%.*(2*(tanh(Y2D/4) + 1))/2;
a = 1.35; % Gaussian beam waist (scaled units)
zR = pi*a^2/lambda;
w = a*sqrt(1 + (z/zR).^2);
I_of_z = (a./w).^2;
PSF = E.*exp(-X2D.^2/a - Y2D.^2/a);
% below is a simple test to get apodization to work, not currently used
% f_eff = 11.96e-3;
% A_sine = 1./sqrt(1 - (X2D.^2 + Y2D.^2)/f_eff^2); % apodization fcn
% pupil_fcn = sqrt(A_sine);
%
% PSF = abs(fftshift(fft2(pupil_fcn.*E))).^2;

%% placing atoms

% where on our pixel grid do we see atoms?
% Because this is adapted from previous code, we call this the DMD
% IT'S REALLY ATOMS DON'T FALL FOR MY LIES
% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!
n_planes = len_z;
spx = 1; %was 7
%place the atoms only on valid lattice sites
n_px_grid = 5; % how many pixels in one lattice site?
atom_pix = 400; %where we load atoms, total extent px_x pixels
lim = atom_pix*dx/2;
xlim_atoms = lim;
x_atoms = -xlim_atoms+dx:dx:xlim_atoms;
x_latt = LAMBDA_LATT_REAL/X_SC/2; %distance between lattice sites
sites = -lim:x_latt:lim; % where are the lattice sites within our limits?
diffs = abs(x - sites');
[~, site_inds] = min(diffs, [], 2);
len_sites = length(site_inds);
site_mesh = meshgrid(site_inds, site_inds); % make a mesh of all available sites

%% Fresnel propagation
% run this multiple times per image
for kk = 1:n_imgs
    n_atoms_plane = zeros(1,n_planes);
    n_atoms_plane(ceil(n_planes/2)) = 1;
    %     n_atoms_plane(1) = 1;
    n_atoms_total = sum(n_atoms_plane);
    
    atom_locs = cell(1,len_z);
    
    I0 = cell(1,len_z);
    for i = 1:len_z
        I0{i} = zeros(size(X2D));
    end
    for i = 1:len_z
        if n_atoms_plane(i) < 0
            n_atoms_plane(i) = 0;
        end
        % if px size is lattice/10, atom loc plane should be 41
        % if lattice/20 then 21
        % atom loc plane corresponds to the index of site inds at the
        % middle of the FoV
        atom_loc_plane = [21;21]*n_atoms_plane(i); %trying to get it to snap to a grid
        atom_locs{i} = atom_loc_plane;
        for ii = 1:len_sites
            for jj = 1:len_sites
                if any(all([ii;jj] == atom_loc_plane))
                    atom_px = zeros(size(X2D));
                    atom_px(site_inds(ii)+1, site_inds(jj)+1) = sqrt(round(0.*randn(1) + 1));
                    E0 = conv2(PSF, atom_px/(spx^2), 'same');
                    %                     E0 = E0/max(max(E0)); %normalizes
                    for mm = 1:len_z
                        if mm == i
                            % in plane, just find the intensity
                            I0{mm} = I0{mm} + abs(E0).^2;
                        else
                            % Do Fresnel propagation
                            dz = z(i) - z(mm);
                            % MODIFY THIS IN THE ABSENCE OF ABERRATIONS TO
                            % CALIBRATE DIFFRACTION SPEED
                            % for lattice/10 it's 5.066
                            % for lattice/20 it's 20.264 (4x higher)
                            % for the AA bessel beam it's 15
                            I0{mm} = I0{mm} + abs(propTF(E0,len_x,lambda,15*dz)).^2; %from file exchange
                        end
                    end
                end
            end
        end
    end
    
    Iplanes = zeros(len_x, len_y, len_z);
    for i = 1:len_z
        Iplanes(:,:,i) = I0{i};
    end
    
    %% Plotting
    
    % OPTIONS FOR FRESNEL PLOTTING
    % this works best if tot = len(z)
    nx = 2; %number of plots along the x-direction
    ny = 3; %number of plots along y-direction
    tot = nx*ny; %total number of plots
    if tot > len_z
        % basically, you can't plot any more points than you have calculated
        % this will leave blank plots but it's better than stupid errors
        tot = len_z;
    end
    
    % plotting limits
    dim_x = xlim_px/32;
    dim_y = ylim_px/32;
    plotlim_x = dim_x;
    plotlim_y = dim_y;
    [~, xlim_neg_ind] = min(abs(x + dim_x));
    [~, xlim_pos_ind] = min(abs(x - dim_x));
    [~, ylim_neg_ind] = min(abs(y + dim_y));
    [~, ylim_pos_ind] = min(abs(y - dim_y));
    ind0_x = ceil(len_x/2);
    ind0_y = ceil(len_y/2);
    
    image_aspect = plotlim_x/plotlim_y;
    
    % plot the magnitude of the field at all Fresnel points
    % and export the images
    %     Lmax = 750;
    PSFs = cell(1,n_planes);
    
    Lmax = 1;
    for i = 1:n_planes
        img = Iplanes(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i);
        %             img = img/max(max(Iplanes(:,:,ceil(n_planes/2))));
        PSFs{i} = img;
        clim  =[0 1];
        [cmap_nonlin, ticks, labels] = nonlinearCmap(parula(500), 0.1, clim, 2, 0.1);
    end
    
    diff = PSFs{1} - PSFs{5};
    diff2 = PSFs{2} - PSFs{4};
    maxdiffs = [max(max(diff)), max(max(diff2))];
    maxes = [max(max(PSFs{1})), max(max(PSFs{2})), max(max(PSFs{3})), max(max(PSFs{4})), max(max(PSFs{5}))]
    I_of_z
    figure('Position', [0 0 500 1000])
    subplot(4,2,1)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{3}, 90))
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Plane 0 (focus)')
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(a)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colormap(cmap_nonlin)
    caxis([0 1])
    colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
    subplot(4,2,2)
    hold on
    plot(-2:2, I_of_z, 'r.', 'MarkerSize', 12)
    plot(-2:2, maxes, 'k.', 'MarkerSize', 12)
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(2) - 0.1*(yy(2)-yy(1)),'(b)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('Plane number (n)')
    ylabel('Maximum intensity')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    grid on
    subplot(4,2,4)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{1}, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(d)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Plane -2')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colormap(cmap_nonlin)
    caxis([0 1])
    colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
    subplot(4,2,6)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{5}, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)), '(f)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Plane +2')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colormap(cmap_nonlin)
    caxis([0 1])
    colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
    subplot(4,2,8)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(diff, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(h)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Difference Planes \pm 2')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colorbar
    colormap(gca, parula(90))
    subplot(4,2,3)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{2}, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(c)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Plane -1')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colormap(cmap_nonlin)
    caxis([0 1])
    colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
    subplot(4,2,5)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{4}, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(e)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Plane +1')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colormap(cmap_nonlin)
    caxis([0 1])
    colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
    subplot(4,2,7)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(diff2, 90))
    xx = xlim;
    yy = ylim;
    text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(g)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Difference Planes \pm 1')
    pbaspect([1 image_aspect 1])
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    colorbar
    colormap(gca, parula(90))
    
end



