function I = diffract(dz, PSF, lambda)

global x y X2D

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% set up grid to convolve with
atom_px = zeros(size(X2D));
atom_px(x0_ind, y0_ind) = 1;

% convolve
spx = 1;
len_x = length(x);
E0 = conv2(PSF, atom_px/(spx^2), 'same');

I0 = abs(E0).^2;
norm_sc = max(max(I0));
% norm_sc = 1;
I0 = I0/norm_sc;

if dz == 0
    I = I0;
else
    I = abs(propTF(E0,len_x,lambda,15*dz)).^2/norm_sc;
end