% This function builds on the FastPeakFind.m (from Matlab repo)
% That was not fully suitable, especially not for the low SNR signals. That
% function also did some flattening of the images before it started to
% filter. Anyways, this is simpler and works better.
% Inputs:
%   rawImg:     The raw image where we want to find peaks.
%   thres:      Is the threshold at which we should cut the image
%   filt:       Is the filter matrix one should use, set to zero for no
%   filtering
%   PSF:        The PSF to use in deconvolution
%   minArea:    The minimal area used in the peakfinder to identify atoms
%   edg:        to eliminate edge cases, i.e. not to run into the image
%               boundary, if the peak found is on the edge.
%
% 180717 - Ott� El�asson - Add the other option of simple peak finding, case control with variable
%   alg. Clean aslo a bit the thresholding part use now the fourier filter
%   function and the filt variable is the convolution kernel.
% 180731 - Robert Heck - bug (variable name clash df & edg in edge removal) corrected
%  bug (if none of detected areas matches criteria output was all detected
%  peaks) corrected. Now output is a zero array in all these cases.
% 180810 - edit output such that also std of peak count is given out
% 200318 - CW edit for use with deconvolution method, also huge cleanup, made
% filtering optional
%
% Latest update: 200318
% Carrie Weidner


function  [cent,cent_map,img,img_nothresh] = hires_deconv_peak_find_200318(rawImg,thres,filt,edge_skip,minArea,PSF,doFilt)

% Filter with the Fourier filter.
if doFilt == 1
    disp('filtering')
    [img_nothresh, ~] = hires_fourier_filter_180716(rawImg,filt);
else
    img_nothresh = rawImg;
end

% Deconvolve images
img_nothresh = deconvlucy(img_nothresh, PSF);

% img_nothresh = img; % image before thresholding
img=img_nothresh.*(img_nothresh>thres);


%% And the peak finder itself. Borrowed from FastPeakFind.m (from Matlab repo)

% peak find - using the local maxima approach - 1 pixel resolution

% d will be noisy on the edges, and also local maxima looks
% for nearest neighbors so edge must be at least 1. We'll skip 'edge' pixels.
% i.e. skip "edge_skip" edge pixels, put it for now just to 3.

img_size = size(img); %image size
[x, y]=find(img(edge_skip:img_size(1)-edge_skip,edge_skip:img_size(2)-edge_skip));

% initialize outputs
test_size = 1000; % if we detect 1000 items we deserve a damn Nobel
cent = zeros(3,test_size); % array of centers (to be filled)
num_cents = 0; % number of centers found
cent_map=zeros(img_size); % amplitude matrix with ones where the center is

x = x + edge_skip-1;
y = y + edge_skip-1;
for j=1:length(y)
    if (img(x(j),y(j)) >= img(x(j)-1,y(j)-1 )) &&...
            (img(x(j),y(j)) > img(x(j)-1,y(j))) &&...
            (img(x(j),y(j)) >= img(x(j)-1,y(j)+1)) &&...
            (img(x(j),y(j)) > img(x(j),y(j)-1)) && ...
            (img(x(j),y(j)) > img(x(j),y(j)+1)) && ...
            (img(x(j),y(j)) >= img(x(j)+1,y(j)-1)) && ...
            (img(x(j),y(j)) > img(x(j)+1,y(j))) && ...
            (img(x(j),y(j)) >= img(x(j)+1,y(j)+1))
            
        
        num_cents = num_cents + 1;
        cent(:,num_cents) = [y(j); x(j); img(x(j), y(j))]; %row with y value, x value, amplitude
        cent_map(x(j),y(j))= cent_map(x(j),y(j)) + img(x(j), y(j)); % if matrix output is desired
        
    end
end

% %% Check for edge cases, and if find them put to 0 (OE)
% 
% % Use the edg, area of pkCnt calculation as a guide
% while ~isempty(find(abs(cent-256)>(256-edge_skip-1),1))
%     edg = find(abs(cent-256)>(256-edge_skip-1),1);
%     if mod(edg,2) == 1
%         cent(edg:(edg+1)) = [];
%         num_cents = num_cents - 1;
%         test_size = test_size - 1;
%     else
%         cent((edg-1):edg) = [];
%         num_cents = num_cents - 1;
%         test_size = test_size - 1; % there is probably a cleaner way to do this
%     end
% end

%% Remove zero entries in center matrix (CW)

cent(:,num_cents + 1:test_size) = [];

% %% Now make a selection based on the areas of the islands (OE)
% % Remove all the minima in islands smaller than a threshold.
% % This is a way to combine the quality of the 2 different
% % algorithms.
% % I have 3 elements:
% %   - The centers from the max-min finder above
% %   - The boundaries given by visboundaries
% % The algorithm will
% %   1. Select the boundaries, that enclose a given area.
% %   2. Find points within those regions from the max-min search.
% 
% % Get the boundaries of the areas found. The Xb and Yb vectors are
% % the x and y coord of boundaries. The different islands are
% % separated by a NaN.
% 
% h = bwboundaries(logical(img));
% % Condition the loop here below that something is found on the image, if
% % not, make sure the cent is empty.
% if ~isempty(h)
%     bound = cell(0);
%     k=0;
%     for ii = 1:size(h,1)
%         if polyarea(h{ii}(:,1),h{ii}(:,2)) >= minArea
%             k=k+1;
%             bound{k,1} = h{ii};
%         end
%     end
%     
%     
%     % Then loop over the points found by the max min algorithm before, keep
%     % those that are inside a boundary. Must also loop over the boundaries.
%     % When a point that is within a boundary is found, put it in the
%     % WhiteList.
%     WhiteList = [];
%     if ~isempty(bound)
%         
%         for jj = 1:size(cent,1)
%             xx = cent(jj,2);
%             yy = cent(jj,1);
%             for ii = 1:size(bound,1)
%                 if inpolygon(xx,yy,bound{ii}(:,1),bound{ii}(:,2))
%                     WhiteList = [WhiteList jj];
%                 end
%             end
%         end
%         
%         % Just in case the same point has been picked out more than once. Then
%         % throw away all the points that don't fulfill the Area criteria.
% 
%     end
%     WhiteList = unique(WhiteList);
%     cent = cent(:,Whitelist);
% 
% %% Calculate the total count in a (n+1)^2 region
% % Need to make sure before entering the for loop that the vector is not
% % empty, otherwise I get some errors.
% % If empty return just [0 0 0]
% if ~isempty(cent)
%     for k = 1:size(cent,1)
%         xx = cent(k,2); % coord of maximum found
%         yy = cent(k,1);
%         
%         % Peak count of (n1) by (n+1) around peak, n = 1,2,3 in raw image
%         cent(k,3) = sum(sum(rawImg((xx-1:xx+1),(yy-1:yy+1))));
%         cent(k,4) = sum(sum(rawImg((xx-2:xx+2),(yy-2:yy+2))));
%         cent(k,5) = sum(sum(rawImg((xx-3:xx+3),(yy-3:yy+3))));
%         
%         % Peak count of (n1) by (n+1) around peak, n = 1,2,3 in filtered image
%         cent(k,6) = sum(sum(img_nothresh((xx-1:xx+1),(yy-1:yy+1))));
%         cent(k,7) = sum(sum(img_nothresh((xx-2:xx+2),(yy-2:yy+2))));
%         cent(k,8) = sum(sum(img_nothresh((xx-3:xx+3),(yy-3:yy+3))));
%         cent(k,9) = mean(mean(img_nothresh((xx-1:xx+1),(yy-1:yy+1))));
%         cent(k,10) = std(std(img_nothresh((xx-1:xx+1),(yy-1:yy+1))));
% 
%         
%     end
%   
% else
%     cent = zeros(1,10)';
% end
% 
% else
%     cent = zeros(1,10)';
% end
% 
% %% Transpose cent matrix
% % For stupid practical Matlab reasons it is better to make the cent matrix
% % not "column based", but "line based". It is because of the nice vector
% % type indexing of a matrix.
% % cent = cent';