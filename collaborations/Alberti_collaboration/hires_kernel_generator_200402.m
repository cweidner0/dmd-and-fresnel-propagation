% The function generates a convolution kernel to use for the Fourier
% filtering of atom images. One can choose between 3 types of filters
% 1. circle, input u is the radius
% 2. rectangle, input u is height, input v is width
% 3. Gaussian, input u is hard bound radius, input v is sigma of Gaussian.

% 190529: Update to switch between Luca or iXon resolution
% 190702: Add the IDS camera to the option
% 200402: Add circle and Gaussian high-pass filtering

% Latest update: 200402
% C Weidner

function kernel = hires_kernel_generator_200402(shape,u,v,cam)

% Choosing camera
switch cam
    case 'iXon'
        chipX = 512;
        chipY = 512;
    case 'Luca'
        chipX = 1002;
        chipY = 1004;
    case 'IDS'
        chipX = 1024;
        chipY = 1280;
        
    otherwise
        disp('You have misspelled something. Choose between: "iXon", "Luca" or "IDS". Please try again.')
end

kernel = zeros(chipX,chipY);

switch shape
    case 'circ'
        % make circle
        radi = u;
        for x = 1:chipX
            for y = 1:chipY
                r = sqrt((x-chipX/2)^2+(y-chipY/2)^2);
                if r<radi
                    kernel(y,x) = 1;
                end
            end
        end
        
    case 'circhi'
        % make circle
        radi = u;
        for x = 1:chipX
            for y = 1:chipY
                r = sqrt((x-chipX/2)^2+(y-chipY/2)^2);
                if r>radi
                    kernel(y,x) = 1;
                end
            end
        end
        
    case 'rect'
        % make rectangle
        heig = u;
        widt = v;
        kernel((chipX/2+1)-heig/2:chipX/2+heig/2,(chipY/2+1)-widt/2:(chipY/2)+widt/2) = ones(heig,widt);
    
    case 'gauss'
        % make gaussian with a hard boundary
        gRad = u;
        sigm = v;
        
        X = 1:chipX; X = X';
        Y = 1:chipY;
        kernel = exp(-(X-chipX/2).^2/sigm^2)*exp(-(Y-chipX/2).^2/sigm^2);
        
        hardCut = zeros(chipX,chipY);

        
        for x = 1:chipX
            for y = 1:chipY
                r = sqrt((x-chipX/2)^2+(y-chipY/2)^2);
                if r<gRad
                    hardCut(x,y) = 1;
                end
            end
        end

        kernel = kernel.*hardCut;
        
    case 'gausshi'
        % make gaussian with a hard boundary
        gRad = u;
        sigm = v;
        
        X = 1:chipX; X = X';
        Y = 1:chipY;
        kernel = 1-exp(-(X-chipX/2).^2/sigm^2)*exp(-(Y-chipX/2).^2/sigm^2);
        
    otherwise
        disp('You have misspelled something. Choose between: "circ", "rect" or "gauss". Will return a zero kernel.')
end

end

