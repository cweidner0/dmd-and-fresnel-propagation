%Script to optimize how different an atom signal looks between planes
%in A. Alberti's group
%NB: wavefront aberration scaling is still a bit fishy
%Carrie Weidner
%Aarhus University
%28 December 2020

close all; clc;
rng('shuffle')

%% Options

% to be added

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants and space

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 µW power
% and the effective depth of the trap is about 4 µK, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 µW

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 µm field of view)
px_x = 1024;
% the total span of each image is about 63 µm, so 32 µm in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
% zlim = 2;
% dz = zlim/2;
zlim = LAMBDA_LATT_SC/2; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = LAMBDA_LATT_SC/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = -4*zlim:dz:0;
len_z = length(z);

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% image limits
global xlim_neg_ind xlim_pos_ind ylim_neg_ind ylim_pos_ind
global xlim_neg_ind_fit xlim_pos_ind_fit ylim_neg_ind_fit ylim_pos_ind_fit

dim_x = xlim_px/16;
dim_y = ylim_px/16;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));

dim_x = xlim_px/32;
dim_y = ylim_px/32;
[~, xlim_neg_ind_fit] = min(abs(x + dim_x));
[~, xlim_pos_ind_fit] = min(abs(x - dim_x));
[~, ylim_neg_ind_fit] = min(abs(y + dim_y));
[~, ylim_pos_ind_fit] = min(abs(y - dim_y));

%% get PSF

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.92; %Specified NA of objective (unitless)
% objective radius from surface 4 in suppl. material
a = 1e-2/X_SC; %radius of objective (1 cm into scaled units)
f = 12e-3/X_SC; %working distance (150 µm to scaled units)
zp = 150e-6/X_SC; %distance from the objective

PSF = make_bessel(NA, a, zp, f, lambda, 0, 0);

%% build GA

n_coeff = 14;
n_children = 10;
n_iter = 1000;

% fitness weights
a1 = 1;
a2 = 1;

% preallocate relevant vectors
avg = zeros(1, n_iter);
best = zeros(1, n_iter);
coeff_best = zeros(n_coeff, n_iter);

% generate initial population
children = randn(n_coeff, n_children);
fitness = zeros(1, n_children);

%constants for the mutation/creep
mut_bound = 1;
creeprate = 1;

% tanh prefactors
pref1 = 0.25;
pref2 = 0.5;

for i1 = 1:n_iter
    for i2 = 1:n_children
        coeffs = pref1*tanh(pref2*children(:,i2));
        E = get_phase_front(coeffs);
        E0 = E.*PSF;
        [In1, I0, I1] = diffract(dz, E0, lambda);
        fitness(i2) = get_fitness(In1, I0, I1, coeffs, a1, a2);
    end
    avg(i1) = mean(fitness);
    best(i1) = min(fitness);
    coeff_best(:,i1) = pref1*tanh(pref2*children(:,find(fitness == min(fitness), 1)));
    children = gen_alg(fitness, children, mut_bound, creeprate, n_coeff);
end

save('Zernike_GA_7.mat')