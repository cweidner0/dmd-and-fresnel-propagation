function child = mutate(A1, mut_bound, ind)
%mutates a single index (w/in bandwidth set by "ind")
%of a child vector "A1"
%mutation bound is defined by "mut_bound"

temp = randperm(ind);
ind2 = temp(1);

temp = randperm(mut_bound,1);
temp2 = rand(1);
if temp2 < 0.5
    mut_amt = -temp(1) + rand(1);
else
    mut_amt = temp(1) + rand(1);
end


child = A1;
child(ind2) = mut_amt;


