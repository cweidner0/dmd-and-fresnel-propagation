function E = get_phase_front(coeffs)

global X2D Y2D

%% add Zernike polynomials phase front

    % https://en.wikipedia.org/wiki/Zernike_polynomials
    % scale by what I assume is the exit pupil a = zp*tan(asin(NA)))
    % update 200814: this should be f, as per W. Alt's suggestion
    % where zp = 150 µm is the working distance and NA is 0.92
%     sc1 = f*tan(asin(NA));
%     sc2 = zp*tan(asin(NA));
    sc = max(max(X2D));
    XZ = X2D/sc;
    YZ = Y2D/sc;

    Rmax = max(max(XZ));
    R2D_sq = (XZ).^2 + (YZ).^2; % radius squared
    R2D_sq_minus = (XZ).^2 - (YZ).^2; % this guy pops up a lot too
    Zp1n1 = 2*XZ; % x-tilt
    Zp11 = 2*YZ; % y-tilt
    Zp20 = sqrt(3)*(2*R2D_sq - 1); %defocus
    Zp2n2 = 2*sqrt(6)*XZ.*YZ; % oblique astigmatism
    Zp22 = sqrt(6)*R2D_sq_minus; % vertical astigmatism
    Zp3n3 = sqrt(8)*(4*YZ.^3 - 3*R2D_sq.*YZ); % y-trefoil
    Zp3n1 = sqrt(8)*(3*R2D_sq*YZ-2*YZ); % y-coma
    Zp31 = sqrt(8)*(3*R2D_sq.*XZ-2*XZ); % x-coma
    Zp33 = sqrt(8)*(4*XZ.^3 - 3*R2D_sq.*XZ); % x-trefoil
    Zp4n4 = 4*sqrt(10)*R2D_sq_minus.*XZ.*YZ; % oblique quadrafoil
    Zp4n2 = 2*sqrt(10)*(4*R2D_sq - 3).*XZ.*YZ; % oblique secondary astigmatism
    Zp40 = sqrt(5)*(6*R2D_sq.^2 - 6*R2D_sq + 1); % primary spherical aberration
    Zp42 = sqrt(10)*(4*R2D_sq - 3).*R2D_sq_minus; % vertical secondary astigmatism
    Zp44 = sqrt(10)*(XZ.^4 -6*(XZ.^2).*(YZ.^2) + YZ.^4); % vertical quadrafoil
    Zp = {Zp1n1, Zp11, Zp2n2, Zp20, Zp22, Zp3n3, Zp3n1, Zp31, Zp33, Zp4n4, Zp4n2, Zp40, Zp42, Zp44};
    
    phase_factor = zeros(size(X2D));
%     ratio = 12101.5; % calibration ratio
    
    for i = 1:length(Zp)
        Zp_cont = coeffs(i)*Zp{i};
        phase_factor = phase_factor + Zp_cont;
    end
    
    E = exp(1i*2*pi*phase_factor);

end