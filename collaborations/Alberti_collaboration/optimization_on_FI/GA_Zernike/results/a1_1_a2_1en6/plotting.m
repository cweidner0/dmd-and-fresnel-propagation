%% loading

close all; clear all; clc;

figure(1)
figure(2)
figure(3)

n_runs = 10;

best_list = zeros(1,n_runs);
avg_list = zeros(1,n_runs);

coeff_ends = zeros(14, n_runs);

for i = 1:n_runs
    load(['Zernike_GA_', num2str(i), '.mat']);
    
    figure(1)
    subplot(2,5,i)
    hold on
    plot(avg)
    plot(best)
    xlabel('Iteration')
    ylabel('Fitness')
    set(gca, 'YScale', 'log')
    legend('Average', 'Best')
    title(['Run ', num2str(i)])
%     axis([0 1000 1e-3 1e1])
    
    E = get_phase_front(coeff_best(:,n_iter));
    E0 = E.*PSF;
    [In1, I0, I1] = diffract(dz, E0, lambda);
    
    figure(2)
    subplot(2,5,i)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)-...
        I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(22)
    subplot(2,5,i)
    imagesc(I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    title(['Run ', num2str(i)])
    
    figure(222)
    subplot(2,5,i)
    imagesc(angle(E))
    title(['Run ', num2str(i)])
    
    avg_list(i) = avg(end);
    best_list(i) = best(end);
    
    coeff_ends(:,i) = coeff_best(:,n_iter);
end

figure(3)
hold on
plot(avg_list, '.', 'MarkerSize', 12)
plot(best_list, '.', 'MarkerSize', 12)
xlabel('Run')
ylabel('Fitness')
legend('Average', 'Best')
set(gca, 'YScale', 'log')

figure
imagesc(coeff_ends)
colorbar
xlabel('Run')
ylabel('Coefficients')