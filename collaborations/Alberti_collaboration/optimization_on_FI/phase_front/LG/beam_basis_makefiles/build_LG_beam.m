close all; clc;
rng('shuffle')

%% Constants and space

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 µW power
% and the effective depth of the trap is about 4 µK, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 µW

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 µm field of view)
px_x = 1024;
% the total span of each image is about 63 µm, so 32 µm in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
% zlim = 2;
% dz = zlim/2;
zlim = LAMBDA_LATT_SC/2; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = LAMBDA_LATT_SC/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = -4*zlim:dz:0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);

R2D = sqrt(X2D.^2 + Y2D.^2); %scaled radius, for aberration)
PHI2D = atan(Y2D./X2D);

RZ = Inf;
w0 = 800e-9/X_SC; % assume 500 nm waist to start
z0 = pi*w0^2/lambda;
zz = 0*z0/z0;
wz = w0*sqrt(1 + zz^2);
Phi_Gouy = atan(zz);

RHO2D = R2D/wz;

%dnX is delta n = X
load('LG_out_RP_test_dn4_dm2.mat');

% LG_L = [1,3,5,7,9];
% LG_P = [0,1,2,3,4];

LG_m = [0,2];
LG_n = [0,4];

LG_L = abs(LG_m);
LG_P = (LG_n - abs(LG_m))/2;

L = cell(1,length(LG_out));

% see here for definition of LG modes
% https://webee.technion.ac.il/~yoav/publications/rotate.pdf
for i = 1:2
%     C = sqrt(2*factorial(LG_P(i))/(pi*factorial(LG_P(i)+abs(LG_L(i)))));
    C = factorial(LG_P(i))*factorial(LG_L(i))/((sqrt(2)^LG_L(i))*factorial(LG_P(i) + LG_L(i)));
    G = (w0/wz)*exp(-RHO2D.^2).*exp(-1i*(RHO2D.^2)*zz).*exp(1i*Phi_Gouy);
    R = sqrt(2)*(RHO2D.^LG_L(i));%.*LG_out{i};
    Phi = exp(1i*LG_m(i)*PHI2D);
    Zn = exp(1i*(LG_n(i))*Phi_Gouy);
    AA = C*G.*R.*Phi.*Zn;
%     AA = C*R;
    if i == 1
        AA(isnan(AA)) = sqrt(2);
        AA(isinf(AA)) = sqrt(2);
    else
        AA(isnan(AA)) = 0;
        AA(isinf(AA)) = 0;
    end
    L{i} = AA;
end

a1 = 1;
a2 = 6;

figure
imagesc(angle(a1*L{1} + a2*L{2}))
colorbar
figure
imagesc(abs(a1*L{1} + a2*L{2}))
colorbar

phase_test = a1*L{1} + a2*L{2};

% figure
% imagesc(angle(L{1} + L{2} + L{3} + L{4} + L{5}))
% colorbar
% figure
% imagesc(abs(L{1} + L{2} + L{3} + L{4} + L{5}))
% colorbar

% 
save('LG_out_RP_test_beam_dn2.mat', 'L')
