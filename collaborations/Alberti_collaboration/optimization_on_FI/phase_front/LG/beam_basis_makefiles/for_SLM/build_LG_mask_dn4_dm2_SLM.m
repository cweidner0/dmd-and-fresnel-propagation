%% SCRIPT TO GENERATE A PHASE MASK TO LOAD ONTO THE SLM
% USE THIS FOR THE LG00 + LG42 COMBINATION

% assumes SLM short axis is 1050 pixels

% C. Weidner
% 10 April 2021

close all; clear all; clc;

%% Constants and space

% SCALING
% FOR SLM MASK GENERATION THIS DOESN'T MATTER TOO MUCH, AND WHAT IS WRITTEN
% HERE (USED FOR SIMULATIONS) WORKS, SO I WILL NOT TWEAK IT

% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 µW power
% and the effective depth of the trap is about 4 µK, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 µW

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (set by SLM)
px_x = 1050;
xlim_px = px_x*dx/2;
ylim_px = xlim_px;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);

R2D = sqrt(X2D.^2 + Y2D.^2); % scaled radius
PHI2D = atan(Y2D./X2D); % polar angle

% generate the LG modes
RZ = Inf;
w0 = 1025e-9/X_SC; % this value (empirically chosen) fills the 1050x1050 area
% Rayleigh length
z0 = pi*w0^2/lambda;
% Gouy phase (this is just a clever way to write zero)
zz = 0;
Phi_Gouy = atan(zz);

RHO2D = R2D/w0;

%dnX is delta n = X
% load the Laguerre-Gauss polynomials (generated offline because they're
% slow to build)
load('LG_out_dn4_dm2_SLM.mat');

% the indices of the different LG modes
LG_m = [0,2];
LG_n = [0,4];

% translate m and n into L and P
LG_L = abs(LG_m);
LG_P = (LG_n - abs(LG_m))/2;

L = cell(1,length(LG_out));

% see here for definition of LG modes
% https://webee.technion.ac.il/~yoav/publications/rotate.pdf
for i = 1:2
    C = factorial(LG_P(i))*factorial(LG_L(i))/((sqrt(2)^LG_L(i))*factorial(LG_P(i) + LG_L(i)));
    G = (w0/w0)*exp(-RHO2D.^2).*exp(-1i*(RHO2D.^2)*zz).*exp(1i*Phi_Gouy);
    R = sqrt(2)*(RHO2D.^LG_L(i));
    Phi = exp(1i*LG_m(i)*PHI2D);
    Zn = exp(1i*(LG_n(i))*Phi_Gouy);
    AA = C*G.*R.*Phi.*Zn;
    % kill NaNs and Infs arising from division by zero, etc.
    if i == 1
        AA(isnan(AA)) = sqrt(2); %cleverly avoiding discontinuities
        AA(isinf(AA)) = sqrt(2);
    else
        AA(isnan(AA)) = 0;
        AA(isinf(AA)) = 0;
    end
    L{i} = AA;
end

% actually make the phase front here
n_coeff = 2;
% coefficients (see my report for details)
coeff = [1 1];
% this (E) is the phase front! This is what you want to load to the SLM!
% (with appropriate zero padding in the long direction, if needed)
E = get_phase_front(coeff, L, n_coeff);

% plot the phase mask
figure(6)
imagesc(E)
colorbar
xlabel('x (px)')
ylabel('y (px)')
title('Phase front')