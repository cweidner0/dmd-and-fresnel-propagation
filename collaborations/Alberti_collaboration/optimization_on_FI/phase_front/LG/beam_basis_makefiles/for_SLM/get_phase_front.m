function E_out = get_phase_front(child, L, n_coeff)

global X2D

E_out = zeros(size(X2D));

% build up the phase front with our LG basis L and coefficients coeff
for i = 1:n_coeff
    E_out = E_out + child(i)*L{i};
end

E_out = angle(E_out); % just get the phase