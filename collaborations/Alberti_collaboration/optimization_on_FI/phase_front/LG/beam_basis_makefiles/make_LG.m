global X2D Y2D

R = 3;
a = 0.00001;
bb = 400;

r = sqrt(X2D.^2 + Y2D.^2)/R;

%LGm_n

LG1_1 = zeros(size(X2D));
LG3_5 = zeros(size(X2D));
LG5_9 = zeros(size(X2D));
LG7_13 = zeros(size(X2D));
LG9_17 = zeros(size(X2D));

for i = 1:length(x)
    for j = 1:length(y)
        ri = sqrt(x(i)^2 + y(j)^2)/R;
        aa = lm_polynomial( 1, 1, 1, ri );
        LG1_1(i,j) = aa(2);
%         aa = lm_polynomial( 1, 5, 3, ri );
%         LG3_5(i,j) = aa(6);
%         aa = lm_polynomial( 1, 9, 5, ri );
%         LG5_9(i,j) = aa(10);
%         aa = lm_polynomial( 1, 13, 7, ri );
%         LG7_13(i,j) = aa(14);
%         aa = lm_polynomial( 1, 91, 17, ri );
%         LG9_17(i,j) = aa(18);
    end
end

b1 = 0;
L1 = 1;
P1 = 0;
E1_1 = exp(1i*L1*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*P1 + abs(L1) + 1)*b1).*LG1_1;
E1_1(isnan(E1_1)) = 0;
E1_1(X2D.^2 + Y2D.^2 > bb*R) = 0;

figure
imagesc(mod(angle(E1_1), 2*pi))
colorbar

% L3 = 3;
% P3 = 1;
% E3_5 = exp(1i*L3*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*P3 + abs(L3) + 1)*b1).*LG3_5;
% E3_5(isnan(E3_5)) = 0;
% E3_5(X2D.^2 + Y2D.^2 > bb*R) = 0;
% 
% L5 = 5;
% P5 = 2;
% E5_9 = exp(1i*L5*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*P5 + abs(L5) + 1)*b1).*LG5_9;
% E5_9(isnan(E5_9)) = 0;
% E5_9(X2D.^2 + Y2D.^2 > bb*R) = 0;
% 
% L7 = 7;
% P7 = 3;
% E7_13 = exp(1i*L7*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*P7 + abs(L7) + 1)*b1).*LG7_13;
% E7_13(isnan(E7_13)) = 0;
% E7_13(X2D.^2 + Y2D.^2 > bb*R) = 0;
% 
% L9 = 9;
% P9 = 4;
% E9_17 = exp(1i*L9*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*P9 + abs(L9) + 1)*b1).*LG9_17;
% E9_17(isnan(E9_17)) = 0;
% E9_17(X2D.^2 + Y2D.^2 > bb*R) = 0;
% 
% LG_out = {angle(E1_1), angle(E3_5), angle(E5_9), angle(E7_13), angle(E9_17)};
% 
% save('LG_out_RP_1.mat', 'LG_out')