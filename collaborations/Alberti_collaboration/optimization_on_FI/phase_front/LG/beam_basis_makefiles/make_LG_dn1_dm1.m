global X2D

%LGm_n

LG0_0 = zeros(size(X2D));
LG1_1 = zeros(size(X2D));
LG2_2 = zeros(size(X2D));
LG3_3 = zeros(size(X2D));
LG4_4 = zeros(size(X2D));


for i = 1:length(x)
    for j = 1:length(y)
        ri = RHO2D(i,j);
        aa = lm_polynomial( 1, 0, 0, ri );
        LG0_0(i,j) = aa(1);
        aa = lm_polynomial( 1, 1, 1, ri );
        LG1_1(i,j) = aa(2);
        aa = lm_polynomial( 1, 2, 2, ri );
        LG2_2(i,j) = aa(3);
        aa = lm_polynomial( 1, 3, 3, ri );
        LG3_3(i,j) = aa(4);
        aa = lm_polynomial( 1, 4, 4, ri );
        LG4_4(i,j) = aa(5);
    end
end

LG0_0(isnan(LG0_0)) = 0;
LG1_1(isnan(LG1_1)) = 0;
LG2_2(isnan(LG2_2)) = 0;
LG3_3(isnan(LG3_3)) = 0;
LG4_4(isnan(LG4_4)) = 0;

LG_out = {LG0_0, LG1_1, LG2_2, LG3_3, LG4_4};

save('LG_out_dn1_dm1_w800.mat', 'LG_out')

% figure(6)
% imagesc(E)
% colorbar
% xlabel('x (px)')
% ylabel('y (px)')
% title('Phase front')