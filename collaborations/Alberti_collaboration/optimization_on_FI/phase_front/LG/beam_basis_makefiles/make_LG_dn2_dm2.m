global X2D

%LGm_n

LG0_0 = zeros(size(X2D));
LG2_2 = zeros(size(X2D));
LG4_4 = zeros(size(X2D));
LG6_6 = zeros(size(X2D));
LG8_8 = zeros(size(X2D));


for i = 1:length(x)
    for j = 1:length(y)
        ri = RHO2D(i,j);
        aa = lm_polynomial( 1, 0, 0, ri );
        LG0_0(i,j) = aa(1);
        aa = lm_polynomial( 1, 2, 2, ri );
        LG2_2(i,j) = aa(3);
        aa = lm_polynomial( 1, 4, 4, ri );
        LG4_4(i,j) = aa(5);
        aa = lm_polynomial( 1, 6, 6, ri );
        LG6_6(i,j) = aa(7);
        aa = lm_polynomial( 1, 8, 8, ri );
        LG8_8(i,j) = aa(9);
    end
end

LG0_0(isnan(LG0_0)) = 0;
LG2_2(isnan(LG2_2)) = 0;
LG4_4(isnan(LG4_4)) = 0;
LG6_6(isnan(LG6_6)) = 0;
LG8_8(isnan(LG8_8)) = 0;

LG_out = {LG0_0, LG2_2, LG4_4, LG6_6, LG8_8};

save('LG_out_RP_test_dn2_dm2.mat', 'LG_out')