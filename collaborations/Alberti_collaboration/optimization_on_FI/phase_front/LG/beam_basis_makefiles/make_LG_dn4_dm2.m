global X2D

%LGm_n

LG0_0 = zeros(size(X2D));
LG4_2 = zeros(size(X2D));
LG8_4 = zeros(size(X2D));
LG12_6 = zeros(size(X2D));
LG16_8 = zeros(size(X2D));


for i = 1:length(x)
    for j = 1:length(y)
        ri = RHO2D(i,j);
        aa = lm_polynomial( 1, 0, 0, ri );
        LG0_0(i,j) = aa(1);
        aa = lm_polynomial( 1, 4, 2, ri );
        LG4_2(i,j) = aa(5);
        aa = lm_polynomial( 1, 8, 4, ri );
        LG8_4(i,j) = aa(9);
        aa = lm_polynomial( 1, 12, 6, ri );
        LG12_6(i,j) = aa(13);
        aa = lm_polynomial( 1, 16, 8, ri );
        LG16_8(i,j) = aa(17);
    end
end

LG0_0(isnan(LG0_0)) = 0;
LG4_2(isnan(LG4_2)) = 0;
LG8_4(isnan(LG8_4)) = 0;
LG12_6(isnan(LG12_6)) = 0;
LG16_8(isnan(LG16_8)) = 0;

LG_out = {LG0_0, LG4_2, LG8_4, LG12_6, LG16_8};

save('LG_out_dn4_dm2_SLM.mat', 'LG_out')