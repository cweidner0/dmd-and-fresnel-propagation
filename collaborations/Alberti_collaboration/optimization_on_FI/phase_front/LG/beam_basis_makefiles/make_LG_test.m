global X2D

%LGm_n

LG0_0 = zeros(size(X2D));
LG1_2 = zeros(size(X2D));

LG1_1 = zeros(size(X2D));
LG3_5 = zeros(size(X2D));
LG5_9 = zeros(size(X2D));
LG7_13 = zeros(size(X2D));
LG9_17 = zeros(size(X2D));

for i = 1:length(x)
    for j = 1:length(y)
        ri = RHO2D(i,j);
        aa = lm_polynomial( 1, 0, 0, ri );
        LG0_0(i,j) = aa(1);
        aa = lm_polynomial( 1, 2, 1, ri );
        LG1_2(i,j) = aa(3);
%         aa = lm_polynomial( 1, 1, 1, ri );
%         LG1_1(i,j) = aa(2);
%         aa = lm_polynomial( 1, 5, 3, ri );
%         LG3_5(i,j) = aa(6);
%         aa = lm_polynomial( 1, 9, 5, ri );
%         LG5_9(i,j) = aa(10);
%         aa = lm_polynomial( 1, 13, 7, ri );
%         LG7_13(i,j) = aa(14);
%         aa = lm_polynomial( 1, 91, 17, ri );
%         LG9_17(i,j) = aa(18);
    end
end

LG0_0(isnan(LG0_0)) = 0;
LG1_2(isnan(LG1_2)) = 0;

LG1_1(isnan(LG0_0)) = 0;
LG3_5(isnan(LG2_4)) = 0;

LG_out = {LG0_0, LG1_2};
% LG_out = {LG1_1, LG3_5, LG5_9, LG7_13, LG9_17};

save('LG_out_RP_test_dn1.mat', 'LG_out')