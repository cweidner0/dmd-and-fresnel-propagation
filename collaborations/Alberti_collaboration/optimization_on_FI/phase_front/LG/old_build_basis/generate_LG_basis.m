load('LG_out.mat')

basis{1} = LG_out{5};
basis{length(basis) + 1} = circshift(LG_out{5}, [-10, 0]);
basis{length(basis) + 1} = circshift(LG_out{5}, [10, 0]);
basis{length(basis) + 1} = circshift(LG_out{5}, [0,-10]);
basis{length(basis) + 1} = circshift(LG_out{5}, [0, 10]);
basis{length(basis) + 1} = circshift(LG_out{5}, [-30, 0]);
basis{length(basis) + 1} = circshift(LG_out{5}, [30, 0]);
basis{length(basis) + 1} = circshift(LG_out{5}, [0, -30]);
basis{length(basis) + 1} = circshift(LG_out{5}, [0, 30]);

basis{length(basis) + 1} = LG_out{6};
basis{length(basis) + 1} = circshift(LG_out{6}, [-10, 0]);
basis{length(basis) + 1} = circshift(LG_out{6}, [10, 0]);
basis{length(basis) + 1} = circshift(LG_out{6}, [0,-10]);
basis{length(basis) + 1} = circshift(LG_out{6}, [0, 10]);
basis{length(basis) + 1} = circshift(LG_out{6}, [-30, 0]);
basis{length(basis) + 1} = circshift(LG_out{6}, [30, 0]);
basis{length(basis) + 1} = circshift(LG_out{6}, [0, -30]);
basis{length(basis) + 1} = circshift(LG_out{6}, [0, 30]);

basis{length(basis) + 1} = LG_out{3};
basis{length(basis) + 1} = circshift(LG_out{3}, [-10, 0]);
basis{length(basis) + 1} = circshift(LG_out{3}, [10, 0]);
basis{length(basis) + 1} = circshift(LG_out{3}, [0,-10]);
basis{length(basis) + 1} = circshift(LG_out{3}, [0, 10]);
basis{length(basis) + 1} = circshift(LG_out{3}, [-30, 0]);
basis{length(basis) + 1} = circshift(LG_out{3}, [30, 0]);
basis{length(basis) + 1} = circshift(LG_out{3}, [0, -30]);
basis{length(basis) + 1} = circshift(LG_out{3}, [0, 30]);

basis{length(basis) + 1} = LG_out{4};
basis{length(basis) + 1} = circshift(LG_out{4}, [-10, 0]);
basis{length(basis) + 1} = circshift(LG_out{4}, [10, 0]);
basis{length(basis) + 1} = circshift(LG_out{4}, [0,-10]);
basis{length(basis) + 1} = circshift(LG_out{4}, [0, 10]);
basis{length(basis) + 1} = circshift(LG_out{4}, [-30, 0]);
basis{length(basis) + 1} = circshift(LG_out{4}, [30, 0]);
basis{length(basis) + 1} = circshift(LG_out{4}, [0, -30]);
basis{length(basis) + 1} = circshift(LG_out{4}, [0, 30]);

save('LG_basis.mat', 'basis')