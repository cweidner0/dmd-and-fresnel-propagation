global X2D Y2D

R = 3;
a = 0.00001;
bb = 400;

r = sqrt(X2D.^2 + Y2D.^2)/R;

LG00 = zeros(size(X2D));
LG10 = zeros(size(X2D));
LG20 = zeros(size(X2D));
LG30 = zeros(size(X2D));
LG01 = zeros(size(X2D));
LG11 = zeros(size(X2D));
LG21 = zeros(size(X2D));
LG31 = zeros(size(X2D));
LG02 = zeros(size(X2D));
LG12 = zeros(size(X2D));
LG22 = zeros(size(X2D));
LG32 = zeros(size(X2D));
LG03 = zeros(size(X2D));
LG13 = zeros(size(X2D));
LG23 = zeros(size(X2D));
LG33 = zeros(size(X2D));

for i = 1:length(x)
    for j = 1:length(y)
        ri = sqrt(x(i)^2 + y(j)^2)/R;
        aa = lm_polynomial( 1, 3, 0, ri );
        LG00(i,j) = aa(1);
        LG10(i,j) = aa(2);
        LG20(i,j) = aa(3);
        LG30(i,j) = aa(4);
        aa = lm_polynomial( 1, 3, 1, ri );
        LG01(i,j) = aa(1);
        LG11(i,j) = aa(2);
        LG21(i,j) = aa(3);
        LG31(i,j) = aa(4);
        aa = lm_polynomial( 1, 3, 2, ri );
        LG02(i,j) = aa(1);
        LG12(i,j) = aa(2);
        LG22(i,j) = aa(3);
        LG32(i,j) = aa(4);
        aa = lm_polynomial( 1, 3, 3, ri );
        LG03(i,j) = aa(1);
        LG13(i,j) = aa(2);
        LG23(i,j) = aa(3);
        LG33(i,j) = aa(4);
    end
end

b1 = 0;
b2 = 0;
L0 = 0;
L1 = 1;
L2 = 2;
L3 = 3;
L4 = 4;
L5 = 5;
p0 = 0;
p1 = 1;
p2 = 2;
p3 = 3;
p4 = 4;
p5 = 5;

E00_1 = exp(1i*L0*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L0) + 1)*b1).*LG00;
E00_2 = exp(1i*L0*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L0) + 1)*b2).*LG00;
E10_1 = exp(1i*L0*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L0) + 1)*b1).*LG10;
E10_2 = exp(1i*L0*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L0) + 1)*b2).*LG10;
E20_1 = exp(1i*L0*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L0) + 1)*b1).*LG20;
E20_2 = exp(1i*L0*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L0) + 1)*b2).*LG20;
E30_1 = exp(1i*L0*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L0) + 1)*b1).*LG30;
E30_2 = exp(1i*L0*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L0) + 1)*b2).*LG30;

E01_1 = exp(1i*L1*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L1) + 1)*b1).*LG01;
E01_2 = exp(1i*L1*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L1) + 1)*b2).*LG01;
E11_1 = exp(1i*L1*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L1) + 1)*b1).*LG11;
E11_2 = exp(1i*L1*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L1) + 1)*b2).*LG11;
E21_1 = exp(1i*L1*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L1) + 1)*b1).*LG21;
E21_2 = exp(1i*L1*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L1) + 1)*b2).*LG21;
E31_1 = exp(1i*L1*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L1) + 1)*b1).*LG31;
E31_2 = exp(1i*L1*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L1) + 1)*b2).*LG31;

E02_1 = exp(1i*L2*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L2) + 1)*b1).*LG02;
E02_2 = exp(1i*L2*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L2) + 1)*b2).*LG02;
E12_1 = exp(1i*L2*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L2) + 1)*b1).*LG12;
E12_2 = exp(1i*L2*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L2) + 1)*b2).*LG12;
E22_1 = exp(1i*L2*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L2) + 1)*b1).*LG22;
E22_2 = exp(1i*L2*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L2) + 1)*b2).*LG22;
E32_1 = exp(1i*L2*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L2) + 1)*b1).*LG32;
E32_2 = exp(1i*L2*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L2) + 1)*b2).*LG32;

E03_1 = exp(1i*L3*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L3) + 1)*b1).*LG03;
E03_2 = exp(1i*L3*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p0 + abs(L3) + 1)*b2).*LG03;
E13_1 = exp(1i*L3*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L3) + 1)*b1).*LG13;
E13_2 = exp(1i*L3*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p1 + abs(L3) + 1)*b2).*LG13;
E23_1 = exp(1i*L3*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L3) + 1)*b1).*LG23;
E23_2 = exp(1i*L3*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p2 + abs(L3) + 1)*b2).*LG23;
E33_1 = exp(1i*L3*atan(Y2D./X2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L3) + 1)*b1).*LG33;
E33_2 = exp(1i*L3*atan(X2D./Y2D) + 1i*k*(a*r.^2) + 1i*(2*p3 + abs(L3) + 1)*b2).*LG33;

E00_1(isnan(E00_1)) = 0;
E00_2(isnan(E00_2)) = 0;
E10_1(isnan(E10_1)) = 0;
E10_2(isnan(E10_2)) = 0;
E20_1(isnan(E20_1)) = 0;
E20_2(isnan(E20_2)) = 0;
E30_1(isnan(E30_1)) = 0;
E30_2(isnan(E30_2)) = 0;

E01_1(isnan(E01_1)) = 0;
E01_2(isnan(E01_2)) = 0;
E11_1(isnan(E11_1)) = 0;
E11_2(isnan(E11_2)) = 0;
E21_1(isnan(E21_1)) = 0;
E21_2(isnan(E21_2)) = 0;
E31_1(isnan(E31_1)) = 0;
E31_2(isnan(E31_2)) = 0;

E02_1(isnan(E02_1)) = 0;
E02_2(isnan(E02_2)) = 0;
E12_1(isnan(E12_1)) = 0;
E12_2(isnan(E12_2)) = 0;
E22_1(isnan(E22_1)) = 0;
E22_2(isnan(E22_2)) = 0;
E32_1(isnan(E32_1)) = 0;
E32_2(isnan(E32_2)) = 0;

E03_1(isnan(E03_1)) = 0;
E03_2(isnan(E03_2)) = 0;
E13_1(isnan(E13_1)) = 0;
E13_2(isnan(E13_2)) = 0;
E23_1(isnan(E23_1)) = 0;
E23_2(isnan(E23_2)) = 0;
E33_1(isnan(E33_1)) = 0;
E33_2(isnan(E33_2)) = 0;

E00_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E00_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E01_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E01_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E02_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E02_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E03_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E03_2(X2D.^2 + Y2D.^2 > bb*R) = 0;

E10_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E10_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E11_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E11_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E12_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E12_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E13_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E13_2(X2D.^2 + Y2D.^2 > bb*R) = 0;

E20_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E20_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E21_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E21_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E22_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E22_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E23_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E23_2(X2D.^2 + Y2D.^2 > bb*R) = 0;

E30_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E30_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E31_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E31_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E32_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E32_2(X2D.^2 + Y2D.^2 > bb*R) = 0;
E33_1(X2D.^2 + Y2D.^2 > bb*R) = 0;
E33_2(X2D.^2 + Y2D.^2 > bb*R) = 0;

LG_out = {angle(E00_1), angle(E00_2), angle(E01_1), angle(E01_2), angle(E02_1), angle(E02_2),...
    angle(E03_1), angle(E03_2), ...
    angle(E10_1), angle(E10_2), angle(E11_1), angle(E11_2), angle(E12_1), angle(E12_2),...
    angle(E13_1), angle(E13_2), ...
    angle(E20_1), angle(E20_2), angle(E21_1), angle(E21_2), angle(E22_1), angle(E22_2),...
    angle(E23_1), angle(E23_2), ...
    angle(E30_1), angle(E30_2), angle(E31_1), angle(E31_2), angle(E32_1), angle(E32_2),...
    angle(E33_1), angle(E33_2)};

save('LG_out_less.mat', 'LG_out')