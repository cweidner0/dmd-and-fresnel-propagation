global X2D

close all

XTEST = X2D(xlim_neg_ind:xlim_pos_ind, ylim_neg_ind:ylim_pos_ind);

xtest = x(xlim_neg_ind:xlim_pos_ind);
ytest = y(ylim_neg_ind:ylim_pos_ind);

%LGm_n

LG1_1 = zeros(size(XTEST));
LG3_5 = zeros(size(XTEST));
LG5_9 = zeros(size(XTEST));

LG7_13 = zeros(size(XTEST));
LG9_17 = zeros(size(XTEST));

for i = 1:length(xtest)
    for j = 1:length(ytest)
        ri = 2*(xtest(i)^2 + ytest(j)^2)/1e1;
        aa = lm_polynomial( 1, 1, 1, ri );
        LG1_1(i,j) = aa(2);
        aa = lm_polynomial( 1, 5, 3, ri );
        LG3_5(i,j) = aa(6);
        aa = lm_polynomial( 1, 9, 5, ri );
        LG5_9(i,j) = aa(10);
        aa = lm_polynomial( 1, 13, 7, ri );
        LG7_13(i,j) = aa(14);
        aa = lm_polynomial( 1, 91, 17, ri );
        LG9_17(i,j) = aa(18);
    end
end

figure
imagesc(abs(LG1_1))
colorbar

figure
imagesc(angle(LG1_1))
colorbar

figure
imagesc(abs(LG3_5))
colorbar

figure
imagesc(angle(LG3_5))
colorbar

figure
imagesc(abs(LG5_9))
colorbar

figure
imagesc(angle(LG5_9))
colorbar


% LG_out = {LG1_1, LG3_5, LG5_9, LG7_13, LG9_17};

% save('LG_out_RP_test_2.mat', 'LG_out')