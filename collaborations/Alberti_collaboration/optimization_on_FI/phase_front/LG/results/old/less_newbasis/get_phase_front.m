function E_out = get_phase_front(child, LG_out, n_coeff)

global X2D

E_out = zeros(size(X2D));

for i = 1:n_coeff
    E_out = E_out + child(i)*LG_out{i};
end