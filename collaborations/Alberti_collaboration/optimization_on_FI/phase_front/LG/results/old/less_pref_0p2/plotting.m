%% loading

close all; clear all; clc;

figure(1)
figure(2)
figure(3)
figure(4)
figure(5)
figure(6)
figure(7)
figure(8)

n_runs = 10;
n_iter = 1000;

best_list = zeros(1,n_runs);
avg_list = zeros(1,n_runs);
px_x = 1024;
E = zeros(px_x, px_x);

for i = 1:n_runs
    load(['phase_front_GA_', num2str(i), '.mat']);
    
    figure(1)
    subplot(2,5,i)
    hold on
    plot(avg)
    plot(best)
    xlabel('Iteration')
    ylabel('Fitness')
    %     set(gca, 'YScale', 'log')
    legend('Average', 'Best')
    title(['Run ', num2str(i)])
    %     axis([0 1000 1e-2 1e1])
    
    % generate phase front
    phi = get_phase_front(children(:,i2), basis, n_coeff);
    E = exp(1i*phi);
    % apply phase front to FFT
    En1diff = ifft2(E.*FEn1);
    E0diff = ifft2(E.*FE0);
    E1diff = ifft2(E.*FE1);
    In1 = abs(En1diff).^2;
    I0 = abs(E0diff).^2;
    I1 = abs(E1diff).^2;
    I0norm = max(max(I0));
    In1 = In1/I0norm;
    I0 = I0/I0norm;
    I1 = I1/I0norm;
    
    figure(2)
    subplot(2,5,i)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)-...
        I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(4)
    subplot(2,5,i)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(5)
    subplot(2,5,i)
    imagesc(I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(6)
    subplot(2,5,i)
    imagesc(angle(E))
    colorbar
    title(['Run ', num2str(i)])
    
    avg_list(i) = avg(end);
    best_list(i) = best(end);
end

figure(3)
hold on
plot(avg_list, '.', 'MarkerSize', 12)
plot(best_list, '.', 'MarkerSize', 12)
xlabel('Run')
ylabel('Fitness')
legend('Average', 'Best')
set(gca, 'YScale', 'log')
