%% loading

close all; clear all; clc;

figure(1)
figure(2)
figure(3)
figure(4)
figure(5)
figure(6)

n_runs = 10;
n_iter = 1000;

best_list = zeros(1,n_runs);
avg_list = zeros(1,n_runs);
FI = zeros(1, n_runs);
px_x = 1024;
E = zeros(px_x, px_x);

waist = 1200;
a1 = 1;
a2 = 0;

load(['LG_out_RP_test_beam_dn1_dm1_fixed_w', num2str(waist), '.mat'])
n_coeff = length(L);

%% from the original code

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 µW power
% and the effective depth of the trap is about 4 µK, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 µW

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 µm field of view)
px_x = 1024;
% the total span of each image is about 63 µm, so 32 µm in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
% zlim = 2;
% dz = zlim/2;
zlim = 0.5*LAMBDA_LATT_SC; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = zlim;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

global X2D Y2D

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);

% image limits
global xlim_neg_ind xlim_pos_ind ylim_neg_ind ylim_pos_ind

dim_x = xlim_px/16;
dim_y = ylim_px/16;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.92; %Specified NA of objective (unitless)
% objective radius from surface 4 in suppl. material
a = 1e-2/X_SC; %radius of objective (1 cm into scaled units)
f = 12e-3/X_SC; %working distance (150 µm to scaled units)
zp = 150e-6/X_SC; %distance from the objective

PSF = make_bessel(NA, a, zp, f, lambda, 0, 0);

[~, E0, ~] = diffract(dz, PSF, lambda);

FE0 = fftshift(fft2(E0));

aa = 0.75

f1 = figure(99);
set(f1, 'Position', [681 0 600*aa 850*aa]);
tiledlayout(5,3, 'Padding', 'none', 'TileSpacing', 'compact'); 

f2 = figure(999);
set(f2, 'Position', [681 -100 600*aa 850*aa]);
tiledlayout(5,3, 'Padding', 'none', 'TileSpacing', 'compact'); 

for i = 1:n_runs
    
    load(['phase_front_GA_dn1_dm1_', num2str(i), '.mat']);
    
    coeff = coeff_best(:,n_iter)/sum(coeff_best(:,n_iter).^2); % normalize children
    E = get_phase_front(coeff, L, n_coeff);
    best_coeffs(:,i) = coeff;
    % apply phase front to FFT
    E = exp(1i*E);
    E0diff = ifft2(E.*FE0);
    [En1diff, E0diff, E1diff] = diffract(dz, E0diff, lambda);
    In1 = abs(En1diff).^2;
    I0 = abs(E0diff).^2;
    I1 = abs(E1diff).^2;
    I0norm = max(max(I0));
    In1 = In1/I0norm;
    I0 = I0/I0norm;
    I1 = I1/I0norm;
    
    logIn1 = log(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind));
    logI1 = log(I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind));
    % logIn1(isinf(logIn1)) = 0;
    % logIn1(isnan(logIn1)) = 0;
    % logI1(isinf(logI1)) = 0;
    % logI1(isnan(logI1)) = 0;
    % first-order derivative of the images with respect to z
    dlogI_dz = abs(logIn1 - logI1);
    FI(i) = sum(sum(I0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind).*(dlogI_dz.^2)));
    
    if i < 6
        figure(99)
    else
        figure(999)
    end
    nexttile
%     subplot(10,3,3*(i-1)+1)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    pbaspect([1 1 1])
    colorbar
%     title('-1')
    nexttile
%     subplot(10,3,3*(i-1)+2)
    imagesc(I0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    pbaspect([1 1 1])
    colorbar
    title(['FI ', num2str(FI(i))])
    nexttile
%     subplot(10,3,3*i)
    imagesc(I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    pbaspect([1 1 1])
    colorbar
%     title('+1')
%     nexttile
    
    best_list(i) = best(end);
    
end

saveas(f1, ['beam_rotation_1_w_', num2str(waist), '_a1_', num2str(a1), '_a2_', num2str(a2), '.png'])
saveas(f2, ['beam_rotation_2_w_', num2str(waist), '_a1_', num2str(a1), '_a2_', num2str(a2), '.png'])

figure(3)
hold on
yyaxis left
set(gca,'ycolor','b')
plot(best_list, 'b.', 'MarkerSize', 12)
ylabel('Best fitness')
yyaxis right
set(gca,'ycolor','r')
plot(FI, 'r.', 'MarkerSize', 12)
xlabel('Run')
ylabel('Fisher information')
set(gca, 'FontSize', 10, 'FontWeight', 'bold')
grid on
legend('Best fitness', 'Fisher information')
saveas(gcf, ['fitness_FI_w_', num2str(waist), '_a1_', num2str(a1), '_a2_', num2str(a2), '.png'])

figure(4)
imagesc(best_coeffs)
colormap jet
colorbar
xlabel('Run')
ylabel('Coefficient')
saveas(gcf, ['coeffs_w_', num2str(waist), '_a1_', num2str(a1), '_a2_', num2str(a2), '.png'])
