function child = creep(A1, creeprate, ind)
%performs a small "creep" of a single index (w/in bandwidth set by "ind")
%of a child vector "A1"
%creep rate is defined by "creeprate"

temp = randperm(ind, 1);
ind2 = temp(1);

r = rand(1);

child = A1;
child(ind2) = child(ind2) + (0.5 - r)*creeprate;