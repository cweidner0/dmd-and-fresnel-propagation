function [En1, E0, E1] = diffract(dz, PSF, lambda)

global x

len_x = length(x);

E0 = PSF;

aa = 0.81;

En1 = propTF(E0,len_x,lambda,-aa*dz);

E1 = propTF(E0,len_x,lambda,aa*dz);