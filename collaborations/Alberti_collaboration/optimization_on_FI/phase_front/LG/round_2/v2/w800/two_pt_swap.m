function child = two_pt_swap(A1, A2, ind)
%swaps two child vectors "A1" and "A2" at two points

len_vec1 = length(A1);

%randomly find swap points
temp = randperm(ind, 2);
ind1 = temp(1);
ind2 = temp(2);

%get stuff in the right order
if ind1 > ind2
    temp = ind1;
    ind1 = ind2;
    ind2 = temp;
end

child = zeros(1, len_vec1);
child(1:ind1) = A1(1:ind1);
child(ind1+1:ind2) = A2(ind1+1:ind2);
child(ind2+1:len_vec1) = A1(ind2+1:len_vec1);