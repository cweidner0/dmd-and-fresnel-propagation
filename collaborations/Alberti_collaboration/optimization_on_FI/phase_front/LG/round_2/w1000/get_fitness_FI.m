function fit = get_fitness_FI(In1, I0, I1)

global xlim_neg_ind_fit xlim_pos_ind_fit ylim_neg_ind_fit ylim_pos_ind_fit

a1 = 1;
a2 = 0.2;

% using classical Fisher information (see 2006 Piestun Opt Exp paper) in
% the ROI
logIn1 = log(In1(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit));
logI1 = log(I1(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit));
% logIn1(isinf(logIn1)) = 0;
% logIn1(isnan(logIn1)) = 0;
% logI1(isinf(logI1)) = 0;
% logI1(isnan(logI1)) = 0;
% first-order derivative of the images with respect to z
dlogI_dz = abs(logIn1 - logI1);
FI = I0(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit).*(dlogI_dz.^2); % Fisher information
% correct for infinities and NaNs (should only have the former)

fit_FI = -sum(sum(FI)); % take the sum over each pixel

% penalize for parts of the image outside of the ROI
In1test = In1;
In1test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;
I0test = I0;
I0test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;
I1test = I1;
I1test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;

fit_out_ROI = (sum(sum(In1test)) + sum(sum(I0test)) + sum(sum(I1test)));

% total fitness is the sum here
fit = a1*fit_FI + a2*fit_out_ROI;