%Script to optimize how different an atom signal looks between planes
%in A. Alberti's group
%NB: wavefront aberration scaling is still a bit fishy
%Carrie Weidner
%Aarhus University
%28 December 2020

close all; clc;
rng('shuffle')

%% Options

% to be added

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants and space

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 µW power
% and the effective depth of the trap is about 4 µK, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 µW

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 µm field of view)
px_x = 1024;
% the total span of each image is about 63 µm, so 32 µm in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
% zlim = 2;
% dz = zlim/2;
zlim = 0.5*LAMBDA_LATT_SC; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = zlim;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = -4*zlim:dz:0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% image limits
global xlim_neg_ind xlim_pos_ind ylim_neg_ind ylim_pos_ind

dim_x = xlim_px/16;
dim_y = ylim_px/16;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));

% image limits
global xlim_neg_ind_fit xlim_pos_ind_fit ylim_neg_ind_fit ylim_pos_ind_fit

dim_x = xlim_px/64;
dim_y = ylim_px/64;
[~, xlim_neg_ind_fit] = min(abs(x + dim_x));
[~, xlim_pos_ind_fit] = min(abs(x - dim_x));
[~, ylim_neg_ind_fit] = min(abs(y + dim_y));
[~, ylim_pos_ind_fit] = min(abs(y - dim_y));

%% get PSF

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.92; %Specified NA of objective (unitless)
% objective radius from surface 4 in suppl. material
a = 1e-2/X_SC; %radius of objective (1 cm into scaled units)
f = 12e-3/X_SC; %working distance (150 µm to scaled units)
zp = 150e-6/X_SC; %distance from the objective

PSF = make_bessel(NA, a, zp, f, lambda, 0, 0);
FPSF = fftshift(fft2(PSF));

center_pt = 513; % center point of the FFT
range = 50; % range we consider around the center point

rng_sub = center_pt-range:center_pt+range;

rng_len = 2*range + 1;
x_rng = linspace(1, rng_len, rng_len);
y_rng = linspace(1, rng_len-1, rng_len);

global x_sub y_sub

[x_sub, y_sub] = meshgrid(x_rng, y_rng);

[En1, E0, E1] = diffract(dz, PSF, lambda);

FEn1 = fftshift(fft2(En1));
FE0 = fftshift(fft2(E0));
FE1 = fftshift(fft2(E1));

%% build GA

% LG polynomials from
% https://people.sc.fsu.edu/~jburkardt/m_src/laguerre_polynomial/laguerre_polynomial.html
% phase front from
% https://iopscience.iop.org/article/10.1088/1367-2630/17/3/033037/pdf
load('LG_out_RP_test_beam_dn1_dm1_fixed_w1200.mat')

n_coeff = length(L); % total length of vector
n_children = 20;
n_iter = 1000;
% n_children = 1;
% n_iter = 1;

% preallocate relevant vectors
avg = zeros(1, n_iter);
best = zeros(1, n_iter);
coeff_best = zeros(n_coeff, n_iter);

% scaling prefactor
pref = 0.5;

% generate initial population
children = pref*randn(n_coeff, n_children);
% children = zeros(n_coeff, 1);
% children(1) = 1;
% children(2) = 1;
fitness = zeros(1, n_children);

%constants for the mutation/creep
mut_bound = pref;
creeprate = pref;

for i1 = 1:n_iter
    disp(['Iteration ', num2str(i1)])
    
    for i2 = 1:n_children
        % generate phase front
        coeff = children(:,i2)/sum(children(:,i2).^2); % normalize children
        E = get_phase_front(coeff, L, n_coeff);
        E = exp(1i*E);
        % apply phase front to FFT
        E0diff = ifft2(E.*FE0);
        [En1diff, E0diff, E1diff] = diffract(dz, E0diff, lambda);
        In1 = abs(En1diff).^2;
        I0 = abs(E0diff).^2;
        I1 = abs(E1diff).^2;
        I0norm = max(max(I0));
        In1 = In1/I0norm;
        I0 = I0/I0norm;
        I1 = I1/I0norm;
        fitness(i2) = get_fitness_FI(In1, I0, I1);
    end
    avg(i1) = mean(fitness);
    best(i1) = min(fitness);
    disp(['Best fitness: ', num2str(best(i1))])
    coeff_best(:,i1) = children(:,find(fitness == min(fitness), 1));
    children = gen_alg(fitness, children, mut_bound, creeprate, n_coeff);
end

save('phase_front_GA_dn1_dm1_1.mat', 'coeff_best', 'avg', 'best')