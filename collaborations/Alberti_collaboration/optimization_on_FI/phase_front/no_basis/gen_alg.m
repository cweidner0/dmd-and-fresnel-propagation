function children = gen_alg(fitness, Am_vec, mut_bound, creeprate,ind)
%from the initial vector of children (Am_vec) and their fitnesses, runs the genetic
%algorithm to produce a new vector of children

%some constants about the number of children, etc.
children = zeros(size(Am_vec));
N = length(fitness);
N_live = 2;
N_die = 6;
inddie = zeros(1, N_die);

%sort things
fitsortlive = sort(fitness, 'ascend');
fitsortdie = sort(fitness, 'descend');

%keep the 2 fittest
for i = 1:N_live
    indlive = find(fitness == fitsortlive(i), 1, 'first');
    children(:,i) = Am_vec(:, indlive);
end

child_len = length(children(:,1));

%kill the 2 least fit
for i = 1:N_die
    inddie(i) = find(fitness == fitsortdie(i), 1, 'first');
end
Am_vec(:, inddie) = [];

%now make children!
for k = 1:N-N_live-N_die
    x1 = 50;
    temp = randperm(x1, N-N_live);
    if mod(temp(k), 4) == 0
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = one_pt_swap(Am_vec(:, temp2(1)), Am_vec(:, temp2(2)), ind);
    elseif mod(temp(k), 3) == 0
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = two_pt_swap(Am_vec(:, temp2(1)), Am_vec(:, temp2(2)), ind);
    elseif mod(temp(k), 2) == 0 && mod(temp(k), 4) ~= 0
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = mutate(Am_vec(:, temp2(1)), mut_bound, ind);
    else
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = creep(Am_vec(:, temp2(1)), creeprate, ind);
    end
end

for k = N-N_die:N
    children(:,k) = randn(child_len, 1);
end