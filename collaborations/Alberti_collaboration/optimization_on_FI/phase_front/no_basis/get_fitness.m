function fit = get_fitness(In1, I0, I1)

global xlim_neg_ind_fit xlim_pos_ind_fit ylim_neg_ind_fit ylim_pos_ind_fit

% form one using 2 norm of matrix differences
% fit = -a1*norm(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind) - ...
% I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind));

% form 2 using classical Fisher information
dI_dz = abs(In1(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit)...
    - I1(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit)); % first-order derivative of the images with respect to z

% take the 1 norm (the sum) of these quantities
% this is (more or less) the classical Fisher information
fit = -norm(dI_dz, 1);

% % penalize for parts of the image outside of the ROI
In1test = In1;
In1test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;
I0test = I0;
I0test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;
I1test = I1;
I1test(xlim_neg_ind_fit:xlim_pos_ind_fit,ylim_neg_ind_fit:ylim_pos_ind_fit) = 0;
fit = fit + (sum(sum(In1test)) + sum(sum(I0test)) + sum(sum(I1test)))/20000;
