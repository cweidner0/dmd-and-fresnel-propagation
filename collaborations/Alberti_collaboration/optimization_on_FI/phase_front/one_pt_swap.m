function child = one_pt_swap(A1, A2, ind)
%swaps two child vectors "A1" and "A2" at one point

len_vec1 = length(A1);

%randomly find swap point
temp = randperm(ind,1);
ind2 = temp(1);

child = zeros(1, len_vec1);
child(1:ind2) = A1(1:ind2);
child(ind2+1:len_vec1) = A2(ind2+1:len_vec1);