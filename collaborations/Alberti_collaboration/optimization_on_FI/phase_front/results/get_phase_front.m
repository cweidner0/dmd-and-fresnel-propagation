function E_out = get_phase_front(child, pref, n_fourier)

global x_sub y_sub

E_out = zeros(size(x_sub));

for i = 1:n_fourier
    E_out = E_out + child(i)*cos(x_sub);
    E_out = E_out + child(i + n_fourier)*sin(x_sub);
    E_out = E_out + child(i + 2*n_fourier)*cos(y_sub);
    E_out = E_out + child(i + 3*n_fourier)*sin(y_sub);
end

E_out = exp(1i*pi*tanh(pref*E_out));