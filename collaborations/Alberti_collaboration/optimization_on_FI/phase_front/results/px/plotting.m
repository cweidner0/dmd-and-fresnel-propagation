%% loading

close all; clear all; clc;

figure(1)
figure(2)
figure(3)
figure(4)
figure(5)
figure(6)
figure(7)
figure(8)

n_runs = 10;
n_iter = 1000;

best_list = zeros(1,n_runs);
avg_list = zeros(1,n_runs);
px_x = 1024;
E = zeros(px_x, px_x);

for i = 1:n_runs
    load(['phase_front_GA_', num2str(i), '.mat']);
    
    figure(1)
    subplot(2,5,i)
    hold on
    plot(avg)
    plot(best)
    xlabel('Iteration')
    ylabel('Fitness')
    set(gca, 'YScale', 'log')
    legend('Average', 'Best')
    title(['Run ', num2str(i)])
    axis([0 1000 1e-2 1e1])
    
    % generate phase front
    E(rng_sub, rng_sub) = get_phase_front(coeff_best(:,n_iter), pref, n_fourier);
    % apply phase front to FFT
    E0 = fft2(E.*FPSF);
    [In1, I0, I1] = diffract(dz, E0, lambda);
    
    figure(2)
    subplot(2,5,i)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)-...
        I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(4)
    subplot(2,5,i)
    imagesc(In1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(5)
    subplot(2,5,i)
    imagesc(I1(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(6)
    subplot(2,5,i)
    imagesc(abs(E(rng_sub, rng_sub)).^2)
    colorbar
    title(['Run ', num2str(i)])
    
    figure(7)
    subplot(2,5,i)
    imagesc(real(E(rng_sub, rng_sub)))
    colorbar
    title(['Run ', num2str(i)])
    
    figure(8)
    subplot(2,5,i)
    imagesc(imag(E(rng_sub, rng_sub)))
    colorbar
    title(['Run ', num2str(i)])
    
    avg_list(i) = avg(end);
    best_list(i) = best(end);
end

figure(3)
hold on
plot(avg_list, '.', 'MarkerSize', 12)
plot(best_list, '.', 'MarkerSize', 12)
xlabel('Run')
ylabel('Fitness')
legend('Average', 'Best')
set(gca, 'YScale', 'log')
