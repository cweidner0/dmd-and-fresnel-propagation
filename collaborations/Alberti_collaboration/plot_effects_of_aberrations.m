%Script to simulate the images from the 0.92 NA microscope objective used
%in A. Alberti's group
%This script puts one atom in the center of the lattice which makes it easy
%to look at PSFs
%Carrie Weidner
%Aarhus University
%3 May 2020

close all; clc; clear all;
rng('shuffle')

file = 'test_img_proc';

%% Options

export_abs = 1; %exports images

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 612e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 �m field of view)
px_x = 1024;
% the total span of each image is about 63 �m, so 32 �m in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
zlim_px = LAMBDA_LATT_SC; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = LAMBDA_LATT_SC/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim_px:dz:zlim_px;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.92; %Specified NA of objective (unitless)
% objective radius from surface 4 in suppl. material
a = 1e-2/X_SC; %radius of objective (1 cm into scaled units)
f = 150e-6/X_SC; %working distance (150 �m to scaled units)
zp = f; %distance from the objective

%% Field parameters

% add Zernike polynomials phase front
% https://en.wikipedia.org/wiki/Zernike_polynomials
% scale by what I assume is the exit pupil (zp*tan(134 deg/2))
% where zp = 150 �m is the working distance
theta = (134/2)*pi/180;
sc = zp*tan(theta);
XZ = X2D/sc;
YZ = Y2D/sc;
R2D_sq = (XZ).^2 + (YZ).^2; % radius squared
R2D_sq_minus = (XZ).^2 - (YZ).^2; % this guy pops up a lot too
Zp20 = sqrt(3)*(2*R2D_sq - 1); %defocus
Zp2n2 = 2*sqrt(6)*XZ.*YZ; % oblique astigmatism
Zp22 = sqrt(6)*R2D_sq_minus; % vertical astigmatism
%     Zp3n1 = sqrt(8)*(3*R2D_sq*YZ-2*YZ); % y-coma
Zp31 = sqrt(8)*(3*R2D_sq.*XZ-2*XZ); % x-coma
Zp33 = sqrt(8)*(4*XZ.^3 - 3*R2D_sq.*XZ); % trefoil
Zp40 = sqrt(5)*(6*R2D_sq.^2 - 6*R2D_sq + 1); % primary spherical aberration
Zp42 = sqrt(10)*(4*R2D_sq - 3).*R2D_sq_minus; % vertical secondary astigmatism
Zp = {Zp20, Zp22, Zp31, Zp33, Zp40, Zp42};

len_Zp = length(Zp);
len_Zp_rng = 50;
Zp_range = linspace(-5, 5, len_Zp_rng);

phase_factor = zeros(size(X2D));

maxdiff = zeros(len_Zp, len_Zp_rng);
maxdiff2 = zeros(len_Zp, len_Zp_rng);
meandiff = zeros(len_Zp, len_Zp_rng);
meandiff2 = zeros(len_Zp, len_Zp_rng);

for ii = 1:len_Zp
    disp(ii)
    for jj = 1:len_Zp_rng
        disp(jj)
        phase_factor = Zp_range(jj)*Zp{ii};
        E = exp(1i*2*pi*phase_factor);
        PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0).*(2*(tanh(Y2D/4) + 1))/2;
        
        
        %% placing atoms
        
        % where on our pixel grid do we see atoms?
        % Because this is adapted from previous code, we call this the DMD
        % IT'S REALLY ATOMS DON'T FALL FOR MY LIES
        % NB: this program explodes if you have a blank grid of pixels
        % to subvert this, turn at least one of the DMD pixels on!
        n_planes = len_z;
        spx = 1; %was 7
        %place the atoms only on valid lattice sites
        n_px_grid = 5; % how many pixels in one lattice site?
        atom_pix = 400; %where we load atoms, total extent px_x pixels
        lim = atom_pix*dx/2;
        xlim_atoms = lim;
        x_atoms = -xlim_atoms+dx:dx:xlim_atoms;
        x_latt = 612e-9/X_SC; %distance between lattice sites
        sites = -lim:x_latt:lim; % where are the lattice sites within our limits?
        diffs = abs(x - sites');
        [~, site_inds] = min(diffs, [], 2);
        len_sites = length(site_inds);
        site_mesh = meshgrid(site_inds, site_inds); % make a mesh of all available sites
        
        %% Fresnel propagation
        
        n_atoms_plane = zeros(1,n_planes);
        n_atoms_plane(ceil(n_planes/2)) = 1;
        n_atoms_total = sum(n_atoms_plane);
        
        atom_locs = cell(1,len_z);
        
        
        I0 = cell(1,len_z);
        for i = 1:len_z
            I0{i} = zeros(size(X2D));
        end
        for i = 1:len_z
            if n_atoms_plane(i) < 0
                n_atoms_plane(i) = 0;
            end
            atom_loc_plane = [21;21]*n_atoms_plane(i); %trying to get it to snap to a grid
            atom_locs{i} = atom_loc_plane;
            for iii = 1:len_sites
                for jjj = 1:len_sites
                    if any(all([iii;jjj] == atom_loc_plane))
                        atom_px = zeros(size(X2D));
                        atom_px(site_inds(iii)+1, site_inds(jjj)+1) = sqrt(round(0.*randn(1) + 1));
                        E0 = conv2(PSF, atom_px/(spx^2), 'same');
                        for mm = 1:len_z
                            if mm == i
                                % in plane, just find the intensity
                                I0{mm} = I0{mm} + abs(E0).^2;
                            else
                                % Do Fresnel propagation
                                dz = z(i) - z(mm);
                                % THE LINE BELOW IS RIGHT DON'T MESS WITH IT
                                I0{mm} = I0{mm} + abs(propTF(E0,len_x,lambda,5*dz)).^2; %from file exchange
                            end
                        end
                    end
                end
            end
        end
        
        %% Add noise
        %     rsq = X2D.^2 + Y2D.^2;
        Iplanes = zeros(len_x, len_y, len_z);
        for i = 1:len_z
            Iplanes(:,:,i) = I0{i};
        end
        aa = 0;
        bb = 0; %set this to one to correctly model shot noise
        % add shot noise
        Iplanes = Iplanes + bb*sqrt(Iplanes).*randn(size(Iplanes)) + abs(sqrt(aa)*randn(size(Iplanes)));
        
        %% Plotting
        
        % OPTIONS FOR FRESNEL PLOTTING
        % this works best if tot = len(z)
        nx = 2; %number of plots along the x-direction
        ny = 3; %number of plots along y-direction
        tot = nx*ny; %total number of plots
        if tot > len_z
            % basically, you can't plot any more points than you have calculated
            % this will leave blank plots but it's better than stupid errors
            tot = len_z;
        end
        
        % plotting limits
        dim_x = xlim_px/16;
        dim_y = ylim_px/16;
        plotlim_x = dim_x;
        plotlim_y = dim_y;
        [~, xlim_neg_ind] = min(abs(x + dim_x));
        [~, xlim_pos_ind] = min(abs(x - dim_x));
        [~, ylim_neg_ind] = min(abs(y + dim_y));
        [~, ylim_pos_ind] = min(abs(y - dim_y));
        ind0_x = ceil(len_x/2);
        ind0_y = ceil(len_y/2);
        
        image_aspect = plotlim_x/plotlim_y;
        
        PSFs = cell(1,n_planes);
        
        Lmax = 1;
        for i = 1:5
            img = Iplanes(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i);
            PSFs{i} = img;
        end
        % find differences between the \pm1 and \pm2 planes
        diff = sqrt((PSFs{2} - PSFs{4}).^2);
        maxdiff(ii,jj) = max(max(diff));
        meandiff(ii,jj) = mean(mean(diff));
        diff2 = sqrt((PSFs{1} - PSFs{5}).^2);
        maxdiff2(ii,jj) = max(max(diff2));
        meandiff2(ii,jj) = mean(mean(diff2));
        
    end
end

figure('Position', [0,0,600, 1000])
subplot(2,1,1)
hold on
for i = 1:len_Zp
    plot(Zp_range, maxdiff(i, :), 'LineWidth', 1.5)
end
xx = xlim;
yy = ylim;
text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(a)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('Aberration size (\lambda)')
ylabel('Maximum difference between \pm 1 planes')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
grid on
lgd = legend('Z_{20}, Defocus', 'Z_{22}, Vertical astigmatism', 'Z_{31}, X-Coma',...
    'Z_{33}, Trefoil','Z_{40}, Primary sph. aberration','Z_{42}, Vert. sec. astig.');
set(lgd, 'FontSize', 8, 'Location', 'best')
subplot(2,1,2)
hold on
for i = 1:len_Zp
    plot(Zp_range, maxdiff2(i, :), 'LineWidth', 1.5)
end
xx = xlim;
yy = ylim;
text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(b)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('Aberration size (\lambda)')
ylabel('Maximum difference between \pm 2 planes')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
grid on
lgd = legend('Z_{20}, Defocus', 'Z_{22}, Vertical astigmatism', 'Z_{31}, X-Coma',...
    'Z_{33}, Trefoil','Z_{40}, Primary sph. aberration','Z_{42}, Vert. sec. astig.');
set(lgd, 'FontSize', 8, 'Location', 'best')
figure('Position', [0,0,600, 1000])
subplot(2,1,1)
hold on
for i = 1:len_Zp
    plot(Zp_range, meandiff(i, :), 'LineWidth', 1.5)
end
xx = xlim;
yy = ylim;
text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(a)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('Aberration size (\lambda)')
ylabel('Average difference between \pm 1 planes')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
grid on
lgd = legend('Z_{20}, Defocus', 'Z_{22}, Vertical astigmatism', 'Z_{31}, X-Coma',...
    'Z_{33}, Trefoil','Z_{40}, Primary sph. aberration','Z_{42}, Vert. sec. astig.');
set(lgd, 'FontSize', 8, 'Location', 'best')
subplot(2,1,2)
hold on
for i = 1:len_Zp
    plot(Zp_range, meandiff2(i, :), 'LineWidth', 1.5)
end
xx = xlim;
yy = ylim;
text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(b)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('Aberration size (\lambda)')
ylabel('Average difference between \pm 2 planes')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
grid on
lgd = legend('Z_{20}, Defocus', 'Z_{22}, Vertical astigmatism', 'Z_{31}, X-Coma',...
    'Z_{33}, Trefoil','Z_{40}, Primary sph. aberration','Z_{42}, Vert. sec. astig.');
set(lgd, 'FontSize', 8, 'Location', 'best')