%Script to simulate the images from the 0.92 NA microscope objective used
%in A. Alberti's group
%This script puts one atom in the center of the lattice which makes it easy
%to look at PSFs
%Carrie Weidner
%Aarhus University
%3 May 2020

close all; clc; clear all;
rng('shuffle')

file = 'test_img_proc';

%% Options

export_abs = 1; %exports images
show_locations = 0; % show locations of atoms
plot_z_intensity_shift = 0; %this only works if zlim is lambda (5 total planes)
calc_filt_PSF = 0;
plot_filt = 0;
plot_xcut = 0;

% add phase front (model aberrations)
do_phase_front = 1;

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Cs-133 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 532e-9*2;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 852e-9; % for Cs atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_REAL/20/X_SC; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_REAL/20/X_SC;
% total number of pixels (use 512 for now, not too far off from the 75 �m field of view)
px_x = 1024;
% the total span of each image is about 63 �m, so 32 �m in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
% zlim = 2;
% dz = zlim/2;
zlim = LAMBDA_LATT_SC; % total number of planes
% z-resolution is one plane (need to check if the lattice is cubic)
dz = LAMBDA_LATT_SC/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = -4*zlim:dz:0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.92; %Specified NA of objective (unitless)
% objective radius from surface 4 in suppl. material
a = 1e-2/X_SC; %radius of objective (1 cm into scaled units)
f = 12e-3/X_SC; %working distance (150 �m to scaled units)
zp = 150e-6/X_SC; %distance from the objective

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    % add Zernike polynomials phase front
    % https://en.wikipedia.org/wiki/Zernike_polynomials
    % scale by what I assume is the exit pupil a = zp*tan(asin(NA)))
    % update 200814: this should be f, as per W. Alt's suggestion
    % where zp = 150 �m is the working distance and NA is 0.92
    %     sc1 = f*tan(asin(NA));
    %     sc2 = zp*tan(asin(NA));
    sc = max(max(X2D));
    XZ = X2D/sc;
    YZ = Y2D/sc;
    %     XZ = X2D/sqrt(max(max(X2D))/2);
    %     YZ = Y2D/sqrt(max(max(Y2D))/2);
    %     XZ = X2D;
    %     YZ = Y2D;
    Rmax = max(max(XZ));
    R2D_sq = (XZ).^2 + (YZ).^2; % radius squared
    R2D_sq_minus = (XZ).^2 - (YZ).^2; % this guy pops up a lot too
    Zp20 = sqrt(3)*(2*R2D_sq - 1); %defocus
    Zp2n2 = 2*sqrt(6)*XZ.*YZ; % oblique astigmatism
    Zp22 = sqrt(6)*R2D_sq_minus; % vertical astigmatism
    %     Zp3n1 = sqrt(8)*(3*R2D_sq*YZ-2*YZ); % y-coma
    Zp31 = sqrt(8)*(3*R2D_sq.*XZ-2*XZ); % x-coma
    Zp33 = sqrt(8)*(4*XZ.^3 - 3*R2D_sq.*XZ); % trefoil
    Zp40 = sqrt(5)*(6*R2D_sq.^2 - 6*R2D_sq + 1); % primary spherical aberration
    Zp42 = sqrt(10)*(4*R2D_sq - 3).*R2D_sq_minus; % vertical secondary astigmatism
    Zp = {Zp20, Zp22, Zp31, Zp33, Zp40, Zp42};
    
    phase_factor = zeros(size(X2D));
    %     ratio = 12101.5; % calibration ratio
    
    %     coeffs = [1,1,1,1,1,1];
    coeffs = [0,0,0,0,0,0];
    %     coeffs = [0.07, -0.01, -0.004, -0.001, 0.006, -0.015];
    
    for i = 1:length(Zp)
        Zp_cont = coeffs(i)*Zp{i};
        phase_factor = phase_factor + Zp_cont;
        Zp_cont(R2D_sq > Rmax^2) = NaN;
        delta(i) = max(max(Zp_cont)) - min(min(Zp_cont));
    end
    
    delta(1)
    
    E = exp(1i*2*pi*phase_factor);
    
end

% actually generate PSF
% this PSF is the typical Bessel beam multiplied by a small hyperbolic
% tangent component that mimics the asymmetry shown in Fig. 3 of
% https://arxiv.org/pdf/1611.02159.pdf
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);%.*(2*(tanh(Y2D/4) + 1))/2;
% a = 1;
% PSF = E.*exp(-X2D.^2/a - Y2D.^2/a);
% below is a simple test to get apodization to work, not currently used
% f_eff = 11.96e-3;
% A_sine = 1./sqrt(1 - (X2D.^2 + Y2D.^2)/f_eff^2); % apodization fcn
% pupil_fcn = sqrt(A_sine);
%
% PSF = abs(fftshift(fft2(pupil_fcn.*E))).^2;

%% placing atoms

% where on our pixel grid do we see atoms?
% Because this is adapted from previous code, we call this the DMD
% IT'S REALLY ATOMS DON'T FALL FOR MY LIES
% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!
n_planes = len_z;
spx = 1; %was 7

%% Fresnel propagation

len_sites = 5;

I0 = cell(1,len_z);
for i = 1:len_z
    I0{i} = zeros(size(X2D));
end
    for mm = 1:len_z
        % Do Fresnel propagation
        dz = z(mm);
        % THE LINE BELOW IS RIGHT DON'T MESS WITH IT
        I0{mm} = diffract(dz, PSF, lambda); %from file exchange
    end

%% Add noise
%     rsq = X2D.^2 + Y2D.^2;
Iplanes = zeros(len_x, len_y, len_z);
for i = 1:len_z
    Iplanes(:,:,i) = I0{i};
end
aa = 0;
bb = 0; %set this to one to correctly model shot noise
% add shot noise
Iplanes = Iplanes + bb*sqrt(Iplanes).*randn(size(Iplanes)) + abs(sqrt(aa)*randn(size(Iplanes)));

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if tot = len(z)
nx = 2; %number of plots along the x-direction
ny = 3; %number of plots along y-direction
tot = nx*ny; %total number of plots
if tot > len_z
    % basically, you can't plot any more points than you have calculated
    % this will leave blank plots but it's better than stupid errors
    tot = len_z;
end

% plotting limits
dim_x = xlim_px/16;
dim_y = ylim_px/16;
plotlim_x = dim_x;
plotlim_y = dim_y;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);

image_aspect = plotlim_x/plotlim_y;

% plot the magnitude of the field at all Fresnel points
% and export the images
%     Lmax = 750;
PSFs = cell(1,n_planes);

Lmax = 1;
if export_abs
    for i = 1:n_planes
        img = Iplanes(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i);
        %             img = img/max(max(Iplanes(:,:,ceil(n_planes/2))));
        PSFs{i} = img;
        %             figure(i)
        %             imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(img, 90))
        %             set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        %             title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        %             xlabel('x (�m)')
        %             ylabel('y (�m)')
        %             pbaspect([1 image_aspect 1])
        %             caxis([0 200])
        clim  =[0 1];
        [cmap_nonlin, ticks, labels] = nonlinearCmap(parula(500), 0.1, clim, 2, 0.1);
        %             colormap(cmap_nonlin)
        %             colorbar('Ticks', ticks, 'TickLabels', labels)
        % if you want to invert the colormap uncomment the below line
        % colormap(flipud(hires_color_map(90)))
        if show_locations == 1
            hold on
            for j = 1:n_planes
                atom_loc_plane = atom_locs{j};
                %                     for k = 1:n_atoms_plane(j)
                %                         if j ~= i
                %                             plot(y(site_inds(atom_loc_plane(1,k)))*X_SC*1e6, -x(site_inds(atom_loc_plane(2,k)))*X_SC*1e6, 'ro', 'MarkerSize', 10)
                %                         else
                %                             plot(y(site_inds(atom_loc_plane(1,k)))*X_SC*1e6, -x(site_inds(atom_loc_plane(2,k)))*X_SC*1e6, 'bo', 'MarkerSize', 10)
                %                         end
                %                     end
            end
        end
        % uncomment me to write images
        % imwrite(img, ['Z:\experiment\Images\simulated_QGM_images\', file, '\', file, '_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
        %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
        %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
    end
end

%theoretical
u = 2*pi*z*(NA^2)/(lambda_proj/X_SC);
I_vs_z = ones(size(z));
for i = 1:5
    if i ~= 3
        I_vs_z(i) = sin(u(i)/4)/(u(i)/4);
    end
end

%     figure
%     for i = 1:6
%         subplot(2,3,i)
%         Zp_cont = coeffs(i)*Zp{i};
%         Zp_cont(R2D_sq > Rmax^2) = NaN;
% %         imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, zpi(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind))
%         imagesc(x,y, Zp_cont)
%         colorbar
%     end

diff = PSFs{1} - PSFs{5};
diff2 = PSFs{2} - PSFs{4};
maxdiffs = [max(max(diff)), max(max(diff2))];
I_vs_z
maxes = [max(max(PSFs{1})), max(max(PSFs{2})), max(max(PSFs{3})), max(max(PSFs{4})), max(max(PSFs{5}))]
%     maxes(3)/maxes(2)
%     maxes(1)/maxes(2)
%     maxes(2)/maxes(3)
%     maxes(4)/maxes(3)
%     maxes(3)/maxes(4)
%     maxes(5)/maxes(4)
figure('Position', [0 0 500 1000])
subplot(4,2,1)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{3}, 90))
xlabel('x (�m)')
ylabel('y (�m)')
title('Plane 0 (focus)')
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(a)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colormap(cmap_nonlin)
caxis([0 1])
colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
subplot(4,2,2)
hold on
plot(-2:2, maxes, 'k.', 'MarkerSize', 12)
plot(-2:2, I_vs_z, 'r.', 'MarkerSize', 12)
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(2) - 0.1*(yy(2)-yy(1)),'(b)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('Plane number (n)')
ylabel('Maximum intensity')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
grid on
subplot(4,2,4)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{1}, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(d)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Plane -2')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colormap(cmap_nonlin)
caxis([0 1])
colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
subplot(4,2,6)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{5}, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)), '(f)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Plane +2')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colormap(cmap_nonlin)
caxis([0 1])
colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
subplot(4,2,8)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(diff, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(h)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Difference Planes \pm 2')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colorbar
colormap(gca, parula(90))
subplot(4,2,3)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{2}, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(c)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Plane -1')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colormap(cmap_nonlin)
caxis([0 1])
colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
subplot(4,2,5)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(PSFs{4}, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(e)', 'Color', 'w', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Plane +1')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colormap(cmap_nonlin)
caxis([0 1])
colorbar('Ticks', ticks, 'TickLabels', labels, 'FontSize', 6)
subplot(4,2,7)
imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(diff2, 90))
xx = xlim;
yy = ylim;
text(xx(1) - 0.1*(xx(1)-xx(2)), yy(1) - 0.1*(yy(1)-yy(2)),'(g)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
xlabel('x (�m)')
ylabel('y (�m)')
title('Difference Planes \pm 1')
pbaspect([1 image_aspect 1])
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
colorbar
colormap(gca, parula(90))

if plot_z_intensity_shift
    zp = linspace(min(z), max(z), 100);
    zeta = k*NA^2*zp;
    fz = (sin(zeta/4)./(zeta/4)).^2;
    p = polyfit(zp*X_SC*1e6/0.612, fz, 2);
    yp = polyval(p,zp*X_SC*1e6/0.612);
    max_z = zeros(1, len_z);
    for i = 1:len_z
        %         max_z(i) = abs(Ediff(ind0_x, ind0_y, i)).^2;
        max_z(i) = max(max(Iplanes(:,:, i)))/ max(max(Iplanes(:,:, 3)));
    end
    figure
    hold on
    plot(zp*X_SC*1e6/0.612, fz, 'r', 'LineWidth', 1.5)
    plot(zp*X_SC*1e6/0.612, yp, 'c--', 'LineWidth', 1.5)
    plot(z*X_SC*1e6/0.612, max_z, 'k.', 'MarkerSize', 12)
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('Lattice plane')
    ylabel('Peak intensity in adjacent planes (fraction of focus)')
    grid on
    axis([min(z)*X_SC*1e6/0.532 max(z)*X_SC*1e6/0.532 0.2 1.1])
    legend('Sinc', 'Quad fit', 'Fresnel amp')
end

if plot_xcut
    figure('Position', [0,0,600, 1000])
    subplot(2,1,1)
    hold on
    for i = 1:5
        plot(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6/0.612, Iplanes(ind0_x,xlim_neg_ind:xlim_pos_ind,i), 'LineWidth', 1.5)
        %             plot(x(xlim_neg_ind:xlim_pos_ind), Iplanes(xlim_neg_ind:xlim_pos_ind,ind0_x,i), 'LineWidth', 1.5)
    end
    xx = xlim;
    yy = ylim;
    text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(a)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('Distance (sites)')
    ylabel('Intensity')
    grid on
    lgd = legend('-2', '-1', '0', '1', '2');
    set(lgd, 'FontSize', 8, 'Location', 'best')
    set(gca, 'FontSize', 12, 'FontWeight', 'bold')
    %set(gca, 'YScale', 'log')
    subplot(2,1,2)
    hold on
    for i = 1:5
        %             plot(x(xlim_neg_ind:xlim_pos_ind), Iplanes(ind0_x,xlim_neg_ind:xlim_pos_ind,i), 'LineWidth', 1.5)
        plot(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6/0.612, Iplanes(xlim_neg_ind:xlim_pos_ind,ind0_x,i), 'LineWidth', 1.5)
    end
    xx = xlim;
    yy = ylim;
    text(xx(2) - 0.1*(xx(2)-xx(1)), yy(1) - 0.1*(yy(1)-yy(2)),'(b)', 'Color', 'k', 'FontWeight', 'Bold', 'FontSize', 12)
    xlabel('Distance (sites)')
    ylabel('Intensity')
    grid on
    lgd = legend('-2', '-1', '0', '1', '2');
    set(lgd, 'FontSize', 8, 'Location', 'best')
    set(gca, 'FontSize', 12, 'FontWeight', 'bold')
    %set(gca, 'YScale', 'log')
end



