%test the lucy-richardson deconvolution algorith
%for more info look up deconvlucy
%C Weidner
%17 March 2020

close all
clear all
clc

% load('img_cell_aa0p1_bb0p1_25.mat')
load('img_cell_26_fluc.mat')
load('PSF_avg.mat') %loads the PSFs propagated through all planes from simulate_one_atom_center.m

PSFs = PSF_avg;

% img_cent = img_cell{3}; %right now just focus on the center image

%% This code just deconvolves with respect to the focal PSF using the peakfinder

a = 5;

cents = cell(1,a);
cent_map = cell(1,a);
J = cell(1,a);
J_nothresh = cell(1,a);

%parameters for peakfinder
% thresh = 0.1;
thresh = 1.5;
edge_skip = 1;
min_area = 2;
PSF_prefactor = 1;
doFilt = 0; %filter?

%filter
u = 150; v = 25; % Settings for the kernel, for the peakfinding
kernel = hires_kernel_generator_190702('gauss',u,v,'iXon');

for i = 1:a
    tic
    % [cent,img,img_nothresh] = hires_deconv_peak_find_200318(rawImg,thres,filt,edge_skip,minArea,PSF)
    [cents{i},cent_map{i},J{i},J_nothresh{i}] = hires_deconv_peak_find_200318(img_cell{i},thresh,kernel,edge_skip, min_area,PSF_prefactor*PSFs{5}, doFilt);
    toc
end

%% Tracking atoms

% I am so sorry this code is awful

%first, find any traces with the same atom in all 5 pictures
cent1 = cents{1};
cent2 = cents{2};
cent3 = cents{3};
cent4 = cents{4};
cent5 = cents{5};
dpx = 5;
tmp = 0;
len1 = size(cent1, 2);
len2 = size(cent2, 2);
len3 = size(cent3, 2);
len4 = size(cent4, 2);
len5 = size(cent5, 2);
maxlen = max(len1, max(len2, max(len3, max(len4, len5))));
atoms_total = maxlen;
inds2keep = zeros(5, maxlen);
amps = zeros(5, maxlen);

for i1 = 1:len1
    for i2 = 1:len2
        for i3 = 1:len3
            for i4 = 1:len4
                for i5 = 1:len5
                    %check if centers are less than dpx apart
                    if abs(cent1(1,i1) - cent2(1,i2)) < dpx && abs(cent1(2,i1) - cent2(2,i2)) < dpx ...
                            && abs(cent2(1,i2) - cent3(1,i3)) < dpx && abs(cent2(2,i2) - cent3(2,i3)) < dpx ...
                            && abs(cent3(1,i3) - cent4(1,i4)) < dpx && abs(cent3(2,i3) - cent4(2,i4)) < dpx ...
                            && abs(cent4(1,i4) - cent5(1,i5)) < dpx && abs(cent4(2,i4) - cent5(2,i5)) < dpx
                        tmp = tmp + 1;
                        inds2keep(:,tmp) = [i1, i2, i3, i4, i5];
                        amps(:,tmp) = [cent1(3,i1), cent2(3,i2), cent3(3,i3), cent4(3,i4), cent5(3,i5)];
                    end
                end
            end
        end
    end
end

% do it all again for atoms in 4 consecutive pictures (I hate this code but
% I cannot figure out how to make it suck less)

remaininginds1 = setdiff(1:len1, inds2keep(1,:));
remaininginds2 = setdiff(1:len2, inds2keep(2,:));
remaininginds3 = setdiff(1:len3, inds2keep(3,:));
remaininginds4 = setdiff(1:len4, inds2keep(4,:));
remaininginds5 = setdiff(1:len5, inds2keep(5,:));
len1 = length(remaininginds1);
len2 = length(remaininginds2);
len3 = length(remaininginds3);
len4 = length(remaininginds4);
len5 = length(remaininginds5);
maxlen = max(len1, max(len2, max(len3, len4)));

for i1 = 1:len1
    for i2 = 1:len2
        for i3 = 1:len3
            for i4 = 1:len4
                if abs(cent1(1,remaininginds1(i1)) - cent2(1,remaininginds2(i2))) < dpx && abs(cent1(2,remaininginds1(i1)) - cent2(2,remaininginds2(i2))) < dpx ...
                        && abs(cent2(1,remaininginds2(i2)) - cent3(1,remaininginds3(i3))) < dpx && abs(cent2(2,remaininginds2(i2)) - cent3(2,remaininginds3(i3))) < dpx ...
                        && abs(cent3(1,remaininginds3(i3)) - cent4(1,remaininginds4(i4))) < dpx && abs(cent3(2,remaininginds3(i3)) - cent4(2,remaininginds4(i4))) < dpx
                    tmp = tmp + 1;
                    inds2keep(:,tmp) = [remaininginds1(i1), remaininginds2(i2), remaininginds3(i3), remaininginds4(i4),0];
                    amps(:,tmp) = [cent1(3,remaininginds1(i1)), cent2(3,remaininginds2(i2)), cent3(3,remaininginds3(i3)), cent4(3,remaininginds4(i4)), 0];
                end
            end
        end
    end
end

remaininginds1 = setdiff(remaininginds1, inds2keep(1,:));
remaininginds2 = setdiff(remaininginds2, inds2keep(2,:));
remaininginds3 = setdiff(remaininginds3, inds2keep(3,:));
remaininginds4 = setdiff(remaininginds4, inds2keep(4,:));
len1 = length(remaininginds1);
len2 = length(remaininginds2);
len3 = length(remaininginds3);
len4 = length(remaininginds4);

for i2 = 1:len2
    for i3 = 1:len3
        for i4 = 1:len4
            for i5 = 1:len5
                if abs(cent2(1,remaininginds2(i2)) - cent3(1,remaininginds3(i3))) < dpx && abs(cent2(2,remaininginds2(i2)) - cent3(2,remaininginds3(i3))) < dpx ...
                        && abs(cent3(1,remaininginds3(i3)) - cent4(1,remaininginds4(i4))) < dpx && abs(cent3(2,remaininginds3(i3)) - cent4(2,remaininginds4(i4))) < dpx ...
                        && abs(cent4(1,remaininginds4(i4)) - cent5(1,remaininginds5(i5))) < dpx && abs(cent4(2,remaininginds4(i4)) - cent5(2,remaininginds5(i5))) < dpx
                    tmp = tmp + 1;
                    inds2keep(:,tmp) = [0,remaininginds2(i2),remaininginds3(i3),remaininginds4(i4),remaininginds5(i5)];
                    amps(:,tmp) = [0, cent2(3,remaininginds2(i2)), cent3(3,remaininginds3(i3)), cent4(3,remaininginds4(i4)), cent5(3,remaininginds5(i5))];
                end
            end
        end
    end
end

% Discard any unused columns
amps(:, ~any(amps)) = [];
inds2keep(:, ~any(inds2keep)) = [];

maxes = amps == max(amps);
atoms_per_plane = sum(maxes, 2);
total_atoms = sum(atoms_per_plane); %should be equal to atoms_total if we tracked all the atoms

% Talk to me, baby
disp(['I found ', num2str(atoms_total), ' atoms!'])

if isempty(setdiff(1:len1, inds2keep(1,:))) %do we have any atoms left
    disp('I tracked all of the atoms!')
else
    disp(['You have ', num2str(atoms_total - total_atoms), ' atoms unaccounted for.'])
end

%% Can I make nice plots

plot_amps = 1;
plot_peakfind = 1;
plot_rawdata = 1;

% Plot the amplitude curves
if plot_amps == 1
    figure
    hold on
    plot(-2:2, amps, 'LineWidth', 1.5)
    % plot(-2:2, amps, '-o','LineWidth', 1.5)
    xlabel('Lattice Plane')
    ylabel('Peak Amplitude (arb)')
    set(gca, 'FontSize', 12, 'FontWeight', 'bold')
    xticks(-2:2)
    grid on
end

max_locs = amps == max(amps);
pic_ind = zeros(1, size(max_locs, 2));
for i = 1:size(max_locs,2)
    pic_ind(i) = find(max_locs(:,i) == 1);
end
cent_ind = inds2keep(amps == max(amps))';
cent_pic = [cent_ind; pic_ind];

% Plot the raw data, deconvolved images, and peak-found images for each plane
if plot_peakfind == 1
    for i = 1:5
        centers = cents{i};
        inds = cent_pic(1,cent_pic(2,:) == i);
        figure('Position', [0, 0, 1500, 400])
        subplot(1,3,1)
        imagesc(img_cell{i})
        colormap(hires_color_map(90))
        xlabel('X Pixel')
        ylabel('Y Pixel')
        title('Raw Image')
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        hold on
        scatter(centers(1,:), centers(2,:), 'wo')
        scatter(centers(1,inds), centers(2,inds), 'go')
        colorbar
        caxis([0,200])
        subplot(1,3,2)
        imagesc(J_nothresh{i})
        colormap(hires_color_map(90))
        xlabel('X Pixel')
        ylabel('Y Pixel')
        title('Deconvolved Image')
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        hold on
        scatter(centers(1,:), centers(2,:), 'wo')
        scatter(centers(1,inds), centers(2,inds), 'go')
        colorbar
        subplot(1,3,3)
        imagesc(cent_map{i})
        colormap(hires_color_map(90))
        colorbar
        hold on
        scatter(centers(1,:), centers(2,:), 'wo')
        scatter(centers(1,inds), centers(2,inds), 'go')
        xlabel('X Pixel')
        ylabel('Y Pixel')
        title('Peaks')
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    end
end

% Plot the raw data with peaks in each plane
if plot_rawdata == 1
    figure('Position', [0, 0, 1600, 400])
    for i = 1:5
        centers = cents{i};
        inds = cent_pic(1,cent_pic(2,:) == i);
        subplot(1,5,i)
        imagesc(img_cell{i})
        colormap(hires_color_map(90))
        colorbar
        caxis([0,500])
        xlabel('X Pixel')
        ylabel('Y Pixel')
        title(['Plane ', num2str(i-3)])
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        hold on
        scatter(centers(1,:), centers(2,:), 'wo')
        scatter(centers(1,inds), centers(2,inds), 'go')
    end
end