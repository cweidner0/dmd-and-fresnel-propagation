%test the lucy-richardson deconvolution algorith
%for more info look up deconvlucy
%C Weidner
%17 March 2020

close all
clear all
clc

%% load images, etc

n_imgs = 10;
postfix = ['_nn_f20_nc_', num2str(n_imgs)];
load(['img_cell', postfix, '.mat'])
load(['plane_dist', postfix, '.mat'])
load('PSF_avg_max.mat') %loads the PSFs propagated through all planes from simulate_one_atom_center.m

PSFs = PSF_avg_max;

% do you want plots
do_plot = 0;
do_plot_final = 1;

%% Peakfinder parameters

n_planes = 5;
n_PSFs = 9;

%parameters for peakfinder
% thresh = 0.1;
thresh = 0.5;
edge_skip = 1;
min_area = 1;
PSF_prefactor = 1;
doFilt = 0; %filter?

%filter
u = 150; v = 25; % Settings for the kernel, for the peakfinding
kernel = hires_kernel_generator_190702('gauss',u,v,'iXon');

num_atoms = zeros(1, n_imgs); %detected numbers of atoms
n_atoms = zeros(1, n_imgs); %actual number of atoms
traces = cell(1, n_imgs); %traces
diffs_1 = cell(1, n_imgs);
diffs_2 = cell(1, n_imgs);
diffs_t = cell(1, n_imgs);
atom_locs = zeros(n_imgs, 3);
atom_locs_real = zeros(n_imgs, 3);
pct_detected = zeros(1, n_imgs);
pct_wrong = zeros(1, n_imgs);

for kk = 1:n_imgs
    tic
    % an atom is, on average, 2 planes out of focus, so if we hit the center image with
    % the relevant PSF we should find most atoms
    [cents,cent_map,J,J_nothresh] = hires_deconv_peak_find_200318(img_cell{kk,3},thresh,kernel,edge_skip, min_area,PSF_prefactor*PSFs{3}, doFilt);
    toc

    %% Tracking atoms

    num_atoms(kk) = size(cents, 2);
    vals = zeros(n_PSFs, num_atoms(kk));
    
    for i = 1:n_PSFs
        J = deconvlucy(img_cell{kk,3}, PSFs{i});
        for j = 1:num_atoms(kk)
            vals(i,j) = J(cents(2,j), cents(1,j));
        end
    end

    for i = 1:num_atoms(kk)
        vals(:,i) = vals(:,i)/max(vals(:,i)); %normalize traces
    end
    
    traces{kk} = vals;
    
    diffs_1t = abs(vals(4,:) - vals(3,:));
    diffs_2t = abs(vals(5,:) - vals(3,:));
    diffs_tt = diffs_1t - diffs_2t;
    
    for i = 1:num_atoms(kk)
        if diffs_1t(i) < 0.1 && diffs_2t(i) < 0.1 %plane pm 2
            atom_locs(kk,3) = atom_locs(kk,3) + 1;
        elseif diffs_tt(i) > -0.03 %plane pm 1 (if not plane pm 2)
            atom_locs(kk,2) = atom_locs(kk,2) + 1;
        else %plane 0
            atom_locs(kk,1) = atom_locs(kk,1) + 1;
        end
    end
    
    diffs_1{kk} = diffs_1t;
    diffs_2{kk} = diffs_2t;
    diffs_t{kk} = diffs_tt;
   
    if do_plot == 1
        figure
        hold on
        for i = 1:num_atoms
            plot(-4:4, vals(:,i), 'LineWidth', 1.5)
        end
        xlabel('PSF plane')
        ylabel('Center amplitude')
        title('Traces (normalized)')
        xticks(-4:4)
        % legend('two planes out', 'one plane out', 'focus')
        grid on
        saveas(gcf, ['atoms', postfix, '_traces.png'])
        
        figure
        hold on
        plot(diffs_1t, 'k.', 'MarkerSize', 12)
        plot(diffs_2t, 'ro', 'MarkerSize', 6)
        xlabel('Atom')
        ylabel('Amplitude')
        legend('One plane difference', 'Two plane difference')
        set(gca, 'YScale', 'linear')
        grid on
        saveas(gcf, ['atoms', postfix, '_diffs.png'])
        
        figure
        plot(diffs_tt, 'b*', 'MarkerSize', 6)
        legend('Difference of differences')
        xlabel('Atom')
        ylabel('Amplitude')
        set(gca, 'YScale', 'linear')
        grid on
        saveas(gcf, ['atoms', postfix, '_dod.png'])
    end
    
    n_atoms_plane = plane_dist{kk};
    atom_locs_real(kk,:) = [n_atoms_plane(3), n_atoms_plane(2) + n_atoms_plane(4), n_atoms_plane(1) + n_atoms_plane(5)];
    diffs = atom_locs_real(kk,:) - atom_locs(kk,:);
    n_atoms(kk) = sum(n_atoms_plane);
    pct_detected(kk) = num_atoms(kk)/n_atoms(kk);
    pct_wrong(kk) = sum(diffs)/num_atoms(kk);
end

if do_plot_final == 1
%     figure
%     hold on
%     plot(1:n_imgs, pct_detected, 'r', 'LineWidth', 1.5)
%     plot(1:n_imgs, pct_wrong, 'b', 'LineWidth', 1.5)
%     xlabel('Image number')
%     ylabel('Percentage')
%     legend('Detected', 'Wrongly classified')
%     grid on
%     saveas(gcf, ['atoms', postfix, '_pcts.png'])
    
    figure('Position',[100 100 1200 900])
    hold on
    plot(1:n_imgs, sum(atom_locs_real,2), 'k.', 'MarkerSize', 12)
    plot(1:n_imgs, sum(atom_locs,2), 'ko', 'MarkerSize', 8)
    plot(1:n_imgs, atom_locs_real(:,1), 'r.', 'MarkerSize', 12)
    plot(1:n_imgs, atom_locs(:,1), 'ro', 'MarkerSize', 8)
    plot(1:n_imgs, atom_locs_real(:,2), 'b.', 'MarkerSize', 12)
    plot(1:n_imgs, atom_locs(:,2), 'bo', 'MarkerSize', 8)
    plot(1:n_imgs, atom_locs_real(:,3), 'c.', 'MarkerSize', 12)
    plot(1:n_imgs, atom_locs(:,3), 'co', 'MarkerSize', 8)
    xlabel('Image number')
    ylabel('Number')
    legend('Total real', 'Total det', 'Center plane real', 'Center plane det', 'One plane out real', 'One plane out det', 'Two planes out real', 'Two planes out det')
    grid on
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    saveas(gcf, ['atoms', postfix, '_planes.png'])
end

atom_stats.n_atoms_real = n_atoms; %actual number of atoms
atom_stats.n_atoms_det = num_atoms; %detected number of atoms
atom_stats.atom_locs_real = atom_locs_real; %actual atom location (distance from focus)
atom_stats.atom_locs_det = atom_locs; %detected atom location
atom_stats.pct_detected = pct_detected; %percent of atoms detected
atom_stats.pct_wrong = pct_wrong; %percent of detected atoms assigned to wrong plane

save(['atom_stats', postfix, '.mat'], 'atom_stats')

if do_plot == 1
    atom_stats
end