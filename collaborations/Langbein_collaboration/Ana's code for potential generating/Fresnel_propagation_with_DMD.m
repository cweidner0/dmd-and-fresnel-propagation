function fff = Fresnel_propagation_with_DMD(input_file, output_file)

%Script to generate optical potential from DMD 

close all; clc

%% Options

load_DMD_image = 1; %loads DMD image from bitmap file
plot_DMD = 1; %plots image loaded onto DMD
plot_init = 0;
plot_abs = 1; %plots magnitude of image
plot_phase = 0; %plots phase of image
plot_xcut = 1;
plot_ycut = 1;
plot_lattice = 1; %plot background lattice with images

% ideally, this should work just fine if we define a superpixel and do the
% convolution, but this does not work right now
% so keep this at zero
do_superpixel = 0;
% use the FFT method to do the Fresnel propagation if this is set to 1
% otherwise use the convolutional method
% FFT method is faster and thus recommended
do_FFT = 1;
% use a window (see relevant code below for more details)
do_window = 1;

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V, 
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized 
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 940e-9;
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
xlim = 100;
ylim = 100;
zlim = 5;
% grid resolution
dx = 1/10;
dy = 1/10;
dz = zlim;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = 0:dz:zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% define window function for Fresnel kernel H
% this is important so that we do not have obnoxious repeating patterns in
% our Fresnel propagation/DMD convolution code
% basically, we need to force things to zero at the edge of the window if
% we have small patterns (like a single light pixel in the center of the DMD)
% one has to be very careful with this

if do_window == 1
    %WINDOW (DO THIS IF THINGS START REPEATING)
    divH = 4;
    window = zeros(len_x, len_y);
    winx = ceil(len_x/divH);
    winy = ceil(len_y/divH);
    window(x0_ind-ceil(winx):x0_ind+ceil(winx), y0_ind-ceil(winy):y0_ind+ceil(winy)) = ...
        coswin(length(x0_ind-ceil(winx):x0_ind+ceil(winx)), 11)*...
        coswin(length(y0_ind-ceil(winy):y0_ind+ceil(winy)), 11)';
else
    %NO WINDOW
    window = ones(size(X2D));
end

%% Field parameters

E = 1; %amplitude of field (in lattice recoils)
% w0 = 1e-6/X_SC; %waist of 
% zR = pi*w0^2/lambda;

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.4-0.45
NA = 0.4; %NA of hires objective (unitless)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
PSF = make_bessel(NA, a, zp, f, lambda, 0, 0);

%% DMD parameters

spx = 7; %superpixel size

%regular pixels
px_x = 1920;
px_y = 1080;



%superpixels
spx_x = floor(px_x/spx); %we take the floor because we need integers
spx_y = floor(px_y/spx); %half a superpixel will do us little good, so we take the floor


DMD = [px_x, px_y];

sDMD = [spx_x, spx_y];

%% input grid

%the code below basically finds the center of the DMD
ind_x0_DMD = ceil(px_x/2)+1;
ind_y0_DMD = ceil(px_y/2)+1;
ind_x0_sDMD = ceil(spx_x/2)+1;
ind_y0_sDMD = ceil(spx_y/2)+1;

% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!
for n=1:10
    if load_DMD_image == 1 %load bitmap
    % currently this just reads images from the current working directory
%        DMD = imread('C:\Users\dell\Desktop\Quantum\dmd-and-fresnel-propagation-master\patterns\pattern60.bmp');
    DMD = imread(input_file);
    %DMD = imread('pattern9.bmp');
    else %write code to generate the DMD image you want
    % DMD(ind_x0_DMD, ind_y0_DMD) = 1;
    DMD(ind_x0_DMD:ind_x0_DMD+(spx-1), ind_y0_DMD:ind_y0_DMD+(spx-1)) = 1;
    end
end
    

% REMEMBER THAT THIS DOES NOT WORK
% hopefully I can fix it at some point
if do_superpixel == 1
    %make superpixels
    for i = 1:spx_x
        ind1i = spx*i-(spx-1);
        ind2i = spx*i;
        for j = 1:spx_y
            ind1j = spx*j-(spx-1);
            ind2j = spx*j;
            sDMD(i,j) = sum(sum(DMD(ind1i:ind2i, ind1j:ind2j)))/(spx^2);
        end
    end
end

%% Do convolutions

if do_superpixel == 1
    E0 = conv2(PSF, sDMD, 'same');
else
    E0 = conv2(PSF, DMD/(spx^2), 'same');
end

%normalize
E0 = E0/max(max(abs(E0)));

FE0 = fftshift(fft2(E0));

Ediff = E0.*ones(size(X)); %empty vector to store E field at different points on grid

rsq = X.^2 + Y.^2;
H = window.*(-1i*k/(2*pi*Z)).*exp(1i*(k./(2*Z)).*rsq + 1i*k*Z);
FH = fftshift(fft2(H(:,:,len_z)));

%% Lattice parameters

if plot_lattice == 1
    V0 = 1; %amplitude of lattice (in recoils)
    Vlatt_2D = V0*(cos(K_LATT*X2D).^2 + cos(K_LATT*Y2D).^2 - 1); %only do 2D lattice
else
    Vlatt_2D = zeros(size(X2D));
end

%% Do Fresnel propagation

% Wikipedia to the rescue
% see https://en.wikipedia.org/wiki/Fresnel_diffraction#Alternative_forms

tic %start timer
for i = 2:len_z
    if z(i) ~= 0
        if do_FFT == 1 % do FFT method (fastest)
            FH = fftshift(fft2(H(:,:,i)))*(dx*dy);
            Ediff(:,:,i) = ifftshift(ifft2(ifftshift(FE0.*FH)));
        else % use convolutional method
            Ediff(:,:,i) = dx*dy*conv2(window.*E0,H(:,:,i),'same');
        end
    end
end
toc %stop timer

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if you 
nx = 1; %number of plots along the x-direction
ny = 2; %number of plots along y-direction
tot = nx*ny; %total number of plots
if tot > len_z
    % basically, you can't plot any more points than you have calculated
    % this will leave blank plots but it's better than stupid errors
    tot = len_z;
end

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% colors!
colors = {'r', 'b', 'k', 'g', 'm', 'r--', 'b--', 'k--', 'g--', 'm--', 'r:', ...
    'b:', 'k:', 'g:', 'm:'};

% plotting limits
[~, xd_ind] = min(abs(x + xd));
[~, yd_ind] = min(abs(y + yd));
dim_x = xlim/2;
dim_y = ylim/2;
plotlim_x = dim_x;
plotlim_y = dim_y;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));

% plot DMD image and the projected image at z = 0
if plot_DMD == 1
    figure('Position', [100 100 800 400])
    %subplot(1,2,1)
    if do_superpixel == 1
        imagesc(sDMD)
    else
        imagesc(DMD)
    end
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (px)')
    ylabel('y (px)')
    title('DMD Pattern')
    figure('Position', [90 90 278 174])
    %plot()
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    F = getframe(gca);
    imwrite(F.cdata, output_file);
%     print(gcf, '-dpng', 'C:\Users\dell\Desktop\Quantum\dmd-and-fresnel-propagation-master\outputs\output60')
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Field at focus')
end

% plot the initial field and it's FFT
if plot_init == 1
    figure('Position', [100 100 800 400])
    %subplot(1,2,1)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Initial field')
    %subplot(1,2,2)
    imagesc(fx(xlim_neg_ind:xlim_pos_ind), fy(ylim_neg_ind:ylim_pos_ind), ...
        abs(FE0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)))
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    title('Initial field FFT')
end

% plot the magnitude of the field at all Fresnel points
if plot_abs == 1
    figure('Position', [0,0,800,300])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            abs(Ediff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,ceil(((i/tot)*len_z)))).^2)
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(ceil((i/tot)*len_z*X_SC*1e-6))), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
    end
end
% plot the phase of the field at all Fresnel points
if plot_phase == 1
    figure('Position', [0,0,800,300])
    for i = 1:tot
        %subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            angle(Ediff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,ceil(((i/tot)*len_z)))))
        colorbar
        caxis([-pi pi])
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['FFT, z = ', num2str(z(ceil((i/tot)*len_z*X_SC*1e-6))), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
    end
end

% plot y-cut at position yd
if plot_ycut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('Y-cut')
    for i = 1:tot
        plot(y(ylim_neg_ind:ylim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(yd_ind,ylim_neg_ind:ylim_pos_ind,ceil(((i/tot)*len_z)))).^2 -...
            Vlatt_2D(yd_ind, ylim_neg_ind:ylim_pos_ind), colors{i})
        leg{i} = ['z = ', num2str(z(ceil((i/tot)*len_z*X_SC*1e-6))), ' �m'];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('y (�m)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_x*X_SC*1e6, plotlim_x*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% plot x-cut at position xd
if plot_xcut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('X-cut')
    for i = 1:tot
        plot(x(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(xlim_neg_ind:xlim_pos_ind,xd_ind,ceil(((i/tot)*len_z)))).^2 - ...
            Vlatt_2D(xlim_neg_ind:xlim_pos_ind, xd_ind), colors{i})
        leg{i} = ['z = ', num2str(z(ceil((i/tot)*len_z)))];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end