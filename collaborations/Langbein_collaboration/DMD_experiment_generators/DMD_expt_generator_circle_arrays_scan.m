%% Generates the DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%half-tiling
px_x_2 = px_x/2;
px_y_2 = px_y/2;

%range
rngx = 920;
rngy = 500;

%% Save folder
folderstr = 'DMD_circle_array_scan';
savefile = ['./', folderstr, '/'];

%% Merge circles!

% R7

% Image size is 1080 px

circle_number = 5;
circle_rad = 10;
circle_dist = 70;
rot = 0;
mkline = 0;

% image positions
circ_size = px_y;
circ_half = ceil(circ_size/2);

len = 5;
len2 = 3;

pos_x = ones(1,len) - circ_half;
pos_y = ones(1,len2) - circ_half;

for i = 1:len
    pos_x(i) = px_x_4  + (i-(len-1)/2 + 1)*ceil(rngx/(len-1)) - circ_half - 200;
end
for i = 1:len2
    pos_y(i) = px_y_4 + (i-(len2-1)/2 + 1)*ceil(rngy/(len2-1)) - circ_half - 200;
end

% index for images
idx = 0;

imgs = {1};

imgs{1} = make_circles(circle_number, circle_rad, circle_dist, rot, mkline);
for j = 1:len
    for k = 1:len2
        file_preamble = ['R', num2str(circle_rad), ...
            'x', num2str(j-1), 'y', num2str(k-1), '_1000000_False_1'];
        make_DMD_image(imgs, {[pos_x(j), pos_y(k)]}, circ_size, file_preamble, idx, savefile);
        idx = idx + 1;
    end
end