%% Generates the DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%% Save folder
folderstr = 'DMD_donuts';
savefile = ['./', folderstr, '/'];

%% Merge circles!

% R7

% Image size is 1080 px

circle_number = 1;
len_i1 = length(circle_number);
circle_rad = 16;
len_i2 = length(circle_rad);
circle_rad2 = 8;
len_i5 = length(circle_rad2);
circle_dist = 1;
len_i3 = length(circle_dist);
rot = 0;
len_i4 = length(rot);
mkline = 0;

% image positions
circ_size = px_y;
circ_half = ceil(circ_size/2);

pos_x = 1 - circ_half;
pos_y = 1 - circ_half;

for i = 3
    pos_x(i-2) = pos_x(i-2) + (i-1)*px_y/4 + ceil((px_x-px_y)/2);
    pos_y(i-2) = pos_y(i-2) + (i-1)*px_y/4;
end

% index for images
idx = 0;

imgs = {1};

for i1 = 1:len_i1
    for i2 = 1:len_i2
        for i3 = 1:len_i3
            for i4 = 1:len_i4
                for i5 = 1:len_i5
                    imgs{1} = make_donuts(circle_number(i1), circle_rad(i2), circle_rad2(i5), circle_dist(i3), rot(i4), mkline);
                    file_preamble = ['R', num2str(circle_rad(i2)), 'r', num2str(circle_rad2), ...
                        's', num2str(circle_dist(i3)), 'n', num2str(circle_number(i1)), ...
                        'r', num2str(rot(i4)), '_1000000_False_1'];
                    make_DMD_image(imgs, {[pos_x, pos_y]}, circ_size, file_preamble, idx, savefile);
                    idx = idx + 1;
                end
            end
        end
    end
end