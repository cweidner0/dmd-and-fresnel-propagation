%% Generates the DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%half-tiling
px_x_2 = px_x/2;
px_y_2 = px_y/2;

%range
rng = 245;

%% Save folder
folderstr = 'DMD_merge_3_circles';
savefile = ['./', folderstr, '/'];

%% Merge circles!

% R7

% Image size is 1080 px

circle_number = 3;
circle_rad = 7;
circle_dist = linspace(0,72,13);
rot = 0;
mkline = 1;

%create Hilbert images
n_imgs = length(circle_dist);

% image positions
circ_size = px_y;
circ_half = ceil(circ_size/2);

pos_x = ones(1,3) - circ_half;
pos_y = ones(1,3) - circ_half;

for i = 1:3
    pos_x(i) = px_x_2  + (i-2)*ceil(rng/4) - circ_half - 100;
    pos_y(i) = px_y_2 + (i-2)*ceil(rng/4) - circ_half - 200;
end

% index for images
idx = 0;

imgs = {1};

for i = 1:n_imgs
    imgs{1} = make_circles(circle_number, circle_rad, circle_dist(i), rot, mkline);
    for j = 1:3
        for k = 1:3
            file_preamble = ['R', num2str(circle_rad), 'sepn', num2str(circle_dist(i)), ...
                'x', num2str(j-1), 'y', num2str(k-1), '_1000000_False_1'];
            make_DMD_image(imgs, {[pos_x(j), pos_y(k)]}, circ_size, file_preamble, idx, savefile);
            idx = idx + 1;
        end
    end
end