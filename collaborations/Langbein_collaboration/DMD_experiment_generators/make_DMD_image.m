function DMD_imgs = make_DMD_image(imgs, posflag, px, file_preamble, idx, varargin)

% Makes images DMD ready
% --assumes the imgs_in argument is a cell of image arrays
% NOT GUARANTEED TO WORK IF THE IMAGE IS NOT A SQUARE
% --posflag is a cell that flags where the image should be placed, a zero
% here places the image in the center, a two-element array specifies the
% position of the image on the DMD array. This does not throw an error if
% the image will be placed outside of the allowed array but rather cuts
% off the image.
% NB: THE POSITION IN THE TWO-ELEMENT ARRAY DENOTES THE POSITION OF THE
% TOP-LEFT-MOST POINT OF THE IMAGE, NOT ITS CENTER!
% --px defines a scaling amount: a zero here will default to the DMD array
% size along the y-direction (1080 px)
% --idx is the index at which to start numbering images

% makes the subdirectory in the parent folder if it does not already exist
% can specify filename as third argument, otherwise a folder named "DMD_images"
% is created and the images are saved there

%% Some initial checking

len_imgs = length(imgs);

if len_imgs ~= length(posflag)
    error('Number of images is not the same as the length of the position flagging cell.')
end
if mod(idx,1) ~= 0 || idx < 0
    error('Image numbering index must be a positive integer.')
end

if nargin == 6
    if ischar(varargin{1})
        % turn annoying warning off
        savefile = varargin{1};
        if ~isfolder(savefile)
            mkdir(savefile);
        end
    else
        error('Save file path must be a string.')
    end
elseif nargin > 6
    error('Too many arguments specified in function call. Specify filename as second argument to function.')
else
    folderstr = 'DMD_expt_images';
    % turn annoying warning off
    warning('off', 'MATLAB:MKDIR:DirectoryExists');
    mkdir(folderstr);
    savefile = ['./', folderstr, '/'];
end

%% DMD parameters

%regular pixels
px_x = 1920;
px_y = 1080;

%% The fun bit

DMD_imgs = cell(1, len_imgs);
% rotate final images (only by +/- 90 degrees, please)
DMD_rot = 90;

for i = 1:len_imgs
    if px(i) == 0
        px(i) = px_y;
    elseif px(i) > px_y
        error(['Scaling value too large. Max scaling value is ', num2str(px_y)])
    end
    img_for_DMD = imgs{i};
    %check if image is binary
    bwflag = isempty(find(img_for_DMD~= 0 & img_for_DMD ~= 1,1));
    % initialize the DMD image
    DMD_img = zeros(px_x, px_y);
    % find correct scale factor
    [~, size_y] = size(img_for_DMD);
    sc = px(i)/size_y;
    if bwflag % image is binary
        %scale image using nearest neighbor interpolation
        img_for_DMD = imresize(img_for_DMD, sc, 'nearest');
    else % image is not binary
        %scale image using bilinear interpolation
        img_for_DMD = imresize(img_for_DMD, sc, 'bilinear');
        %dither image using Floyd-Steinberg algorithm
        img_for_DMD = Floyd_Steinberg(1,img_for_DMD);
    end
    [size_x, size_y] = size(img_for_DMD);
    % NB: POSITION OF IMAGE MARKS TOP LEFT CORNER OF IMAGE
    % make a real DMD img
    pos = posflag{i};
    if pos == 0
        pos = floor(abs([px_x - size_x, px_y - size_y]/2))+1;
    elseif length(pos) ~= 2
        error('Position flag should be a vector specifying two pixel values.')
    elseif abs(pos(1)) > px_x || abs(pos(2)) > px_y || pos(1) + size_x < 0 || pos(2) + size_y < 0
        error('Position flag out of range.')
    end
    %perform huge round of checks
    pos = round(pos); % convert non-integers to integers
    flag_x = 0;
    flag_y = 0;
    % checks if position is less than zero
    if pos(1) < 1
        %cut image off in x
        start_x = 1;
        imgst_x = -pos(1)+1;
        flag_x = 1;
    else
        start_x = pos(1);
        imgst_x = 1;
    end
    if pos(2) < 1
        %cut image off in x
        start_y = 1;
        imgst_y = -pos(2)+1;
        flag_y = 1;
    else
        start_y = pos(2);
        imgst_y = 1;
    end
    if pos(1) + size_x > px_x
        %cut image off in x
        end_x = px_x-1;
        imgend_x = px_x - pos(1);
    else
        end_x = pos(1) + size_x-1;
        imgend_x = size_x;
    end
    if pos(2) + size_y > px_y
        %cut image off in x
        end_y = px_y - 1;
        imgend_y = px_y - pos(2);
    else
        end_y = pos(2) + size_y - 1;
        imgend_y = size_y;
    end
    DMD_img(start_x:end_x+flag_x, start_y:end_y+flag_y) = img_for_DMD(imgst_x:imgend_x, imgst_y:imgend_y);
    %make the image logical (saves space on disk)
    DMD_img = logical(DMD_img == 1);
    DMD_imgs{i} = imrotate(DMD_img, DMD_rot);
    imwrite(DMD_imgs{i},[savefile, file_preamble, '_', num2str(idx + i - 1), '.bmp']);
end

