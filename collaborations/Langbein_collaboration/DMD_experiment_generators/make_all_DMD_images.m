%% runs all of the current DMD image producing scripts and times them

%% HILBERT

close all; clear all; clc;
disp  DMD_expt_generator_Hilbert
tic
run DMD_expt_generator_Hilbert
toc


%% ZERNIKE

% close all; clear all; clc;
% tic
% run DMD_expt_generator_Zernike
% toc


%% MERGING CIRCLE ARRAYS
% 
% disp DMD_expt_generator_merge_2_circles
% tic
% run DMD_expt_generator_merge_2_circles
% toc

disp DMD_expt_generator_merge_3_circles
tic
run DMD_expt_generator_merge_3_circles
toc
% 
% disp DMD_expt_generator_merge_2by2_array
% tic
% run DMD_expt_generator_merge_2by2_array
% toc

%% MORE CIRCLE ARRAYS
% 
% disp DMD_expt_generator_circle_lines
% tic
% run DMD_expt_generator_circle_lines
% toc
% 
% disp DMD_expt_generator_circle_arrays
% tic
% run DMD_expt_generator_circle_arrays
% toc
% 
% disp DMD_expt_generator_circle_arrays_2
% tic
% run DMD_expt_generator_circle_arrays_2
% toc
% 
% disp DMD_expt_generator_circles
% tic
% run DMD_expt_generator_circles
% toc