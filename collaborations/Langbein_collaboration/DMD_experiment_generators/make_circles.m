function img_out = make_circles(circle_number, circle_rad, circle_dist, rot, make_line)

%% image parameters

px = 1080; % max DMD size
img_out = zeros(px);

%% set up the grid

% set up grid (scaled for easy rotation)
% grid centered on zero, total span of one
x = linspace(-0.5, 0.5, px);
y = linspace(-0.5, 0.5, px);
% scale parameters properly
rsq = (circle_rad/px)^2;
sc_dist = circle_dist/px;
% build a meshgrid
[X,Y] = meshgrid(x,y);

%% rotation, a celebration

% rotation angle, now in radians!
rot_rad = rot*pi/180;
% rotation matrix
rot_mat = [[cos(rot_rad), sin(rot_rad)];[-sin(rot_rad), cos(rot_rad)]];

%% make circles

% find centers of circles
center_x = zeros(circle_number, 1);
center_y = zeros(circle_number, 1);
if mod(circle_number, 2) == 0
    for i = 1:circle_number
        center_x(i) = (i - (circle_number + 1)/2)*sc_dist;
    end
else
    for i = 1:circle_number
        center_x(i) = (i - floor(circle_number/2) - 1)*sc_dist;
    end
end

if make_line == 1
    % make a linear array of circles
    % I'm weird and prefer to work with rows, sorry
    center = cat(2, center_x, center_y);
    % rotate and re-arrange
    center = rot_mat*center';
    center = center';
    center_x = center(:,1);
    center_y = center(:,2);
    for i = 1:circle_number
        img_out((X - center_x(i)).^2 + (Y - center_y(i)).^2 <= rsq) = 1;
    end
else
    % make a grid array of circles
    centers_x = center_x*ones(1,circle_number);
    centers_y = centers_x;
    for i = 1:circle_number
        for j = 1:circle_number
            % rotate and re-arrange
            vec = [centers_x(i), centers_y(j)];
            vec = rot_mat*vec';
            img_out((X - vec(1)).^2 + (Y - vec(2)).^2 < rsq) = 1;
        end
    end
end