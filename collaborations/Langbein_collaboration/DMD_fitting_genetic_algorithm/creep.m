function child = creep(A1, creeprate, ind)

temp = randperm(ind, 1);
ind2 = temp(1);

r = rand(1);

child = A1;
child(ind2) = child(ind2) + (0.5 - r)*creeprate;

if child(ind2) > 1
    child(ind2) = 1;
elseif child(ind2) < 0
    child(ind2) = 0;
end