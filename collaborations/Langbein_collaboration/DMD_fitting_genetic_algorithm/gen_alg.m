function children = gen_alg(param, fit)

%a lot of this is just ripped from the code I wrote for my PhD

children = zeros(size(param));
N = length(fit);
N_live = 2;
N_die = 2;
inddie = zeros(1, N_die);

ind = length(param(:,1)) - 1;

%ga_array = 1:N;
fitsortlive = sort(fit, 'ascend');
fitsortdie = sort(fit, 'descend');

%keep the 2 fittest
for i = 1:N_live
    indlive = find(fit == fitsortlive(i), 1, 'first');
    children(:,i) = param(:, indlive);
end

%kill the 2 least fit
for i = 1:N_die
    inddie(i) = find(fit == fitsortdie(i), 1, 'first');
end

param(:, inddie) = [];

for k = 1:N-N_live
    x1 = 50;
    temp = randperm(x1, N-N_live);
    if mod(temp(k), 3) == 0
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = one_pt_swap(param(:, temp2(1)), param(:, temp2(2)), ind);
    elseif mod(temp(k), 2) == 0
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = mutate(param(:, temp2(1)), ind+1);
    else
        x2 = N-N_die;
        temp2 = randperm(x2);
        children(:,k + N_live) = two_pt_swap(param(:, temp2(1)), param(:, temp2(2)), ind);
    end
end