function fit = get_fitness(c0, c1)

alpha_0 = -1;
alpha_1 = 0.5;

fit = alpha_0*c0 + alpha_1*c1;