function [omega_pulse, Omega_pulse, t] = make_HS1_GA(t_size, tau_pulse, Omega_max, omega_0, omega_sweep)

beta = 5.3; %this is standard

t = linspace(0, tau_pulse, t_size);

ampl = omega_sweep/tau_pulse;
tau_sc = 2*t/tau_pulse - 1;

Omega_pulse = Omega_max*sech(beta*tau_sc);
omega_pulse = omega_0 + ampl*tanh(beta*tau_sc);