function [omega_pulse, Omega_pulse] = make_HS1(t, tau_pulse, Omega_max, omega_0, omega_sweep)

beta = 5.3; %this is standard

ampl = omega_sweep/tau_pulse;
tau_sc = 2*t/tau_pulse - 1;

Omega_pulse = Omega_max*sech(beta*tau_sc);
omega_pulse = omega_0 + ampl*tanh(beta*tau_sc);
