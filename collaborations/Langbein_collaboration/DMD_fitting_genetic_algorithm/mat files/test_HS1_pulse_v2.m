close all
clc

tic
%this does the HS1 pulse, right now just trying to replicate Mathematica
%results

%set ODE options
Jpattern = [0 1;1 0];
Jpattern_sparse = sparse(Jpattern);
% opts = odeset('Reltol',1e-6,'AbsTol',1e-6,'Stats','on');
% opts = odeset('Reltol',1e-4,'AbsTol',1e-4,'Stats','on');
opts = odeset('Reltol',1e-3,'AbsTol',1e-3, 'JPattern', Jpattern_sparse,'Stats','on');

%HS1 parameters
tau_pulse = 10e-3;
Omega_max = 2*pi*3e3;
omega_sweep = 2*pi*0.01e3;

Omega_0 = 2*pi*6e3; %Capital letters for Rabi frequency
omega_0 = 2*pi*6.8e6; %Lowercase for atomic transition frequencies

Tau = 2*pi/Omega_0; %the timescales on which things happen, same conventions as above
tau = 2*pi/omega_0;

tsize = 1e3;
t_sample = linspace(0, tau_pulse, tsize);
tspan = [0, tau_pulse];

[omega_pulse, Omega_pulse] = make_HS1(t_sample, tau_pulse, Omega_max, omega_0, omega_sweep);

% opts = odeset('Reltol',1e-4,'AbsTol',1e-5, 'Jacobian', @(t,~)system_jacobian(t, tau_pulse, Omega_max, omega_0, omega_sweep), 'Stats','on');

c0 = [1 0];

[t_res,c_res] = ode15s(@(t_res,c_res) two_level_sol(t_res, c_res, omega_0, omega_pulse, Omega_pulse, t_sample), tspan, c0, opts);

%off resonant
d1plane = 0.5e-4; %distance of 1 plane in cm
gradB = 50; %gradient in G/cm
B = gradB*d1plane;
delta = 2*0.7e6*B;

[t_nonres,c_nonres] = ode15s(@(t_nonres,c_nonres) two_level_sol(t_nonres, c_nonres, omega_0-delta, omega_pulse, Omega_pulse, t_sample), tspan, c0, opts);

figure
hold on
plot(t_res,abs(c_res(:,1)).^2,'r-',t_res,abs(c_res(:,2)).^2,'r-.')
plot(t_nonres,abs(c_nonres(:,1)).^2,'k-',t_nonres,abs(c_nonres(:,2)).^2,'k-.')

tlen_res = length(t_res);
tlen_nonres = length(t_nonres);

abs(c_res(tlen_res, 2))^2
abs(c_nonres(tlen_nonres,2))^2
toc
