i = 1
tau_pulse = param(1,i);
Omega_max = param(2,i);
omega_sweep = param(3,i);
[omega_pulse, Omega_pulse, t_sample] = make_HS1_GA(t_sample_size, tau_pulse, Omega_max, omega_0, omega_sweep);
tspan = [0, tau_pulse];
[t_res,c_res] = ode113(@(t_res,c_res) two_level_sol(t_res, c_res, omega_0, omega_pulse, Omega_pulse, t_sample), tspan, c_init, opts);
[t_nonres,c_nonres] = ode113(@(t_nonres,c_nonres) two_level_sol(t_nonres, c_nonres, omega_0-delta, omega_pulse, Omega_pulse, t_sample), tspan, c_init, opts);
c0(i) = abs(c_res(length(t_res),2))^2;
c1(i) = abs(c_nonres(length(t_nonres),2))^2;

figure
hold on
plot(t_res,abs(c_res(:,1)).^2,'r-',t_res,abs(c_res(:,2)).^2,'r-.')
plot(t_nonres,abs(c_nonres(:,1)).^2,'k-',t_nonres,abs(c_nonres(:,2)).^2,'k-.')