%trying out some pulse engineering like a baus

Omega_0 = 2*pi*6e3; %Capital letters for Rabi frequency
omega_0 = 2*pi*6.8e6; %Lowercase for atomic transition frequencies

Tau = 2*pi/Omega_0; %the timescales on which things happen, same conventions as above
tau = 2*pi/omega_0;

tsize = 1e3;
tlim = 12*Tau;
t_sample = linspace(0,tlim,tsize);
tspan = [0, tlim];

Omega_t = Omega_0*ones(1, tsize);
omega_t = omega_0 - Omega_0*ones(1, tsize);

c0 = [1 0];

% opts = odeset('Reltol',1e-6,'AbsTol',1e-6,'Stats','on');
opts = odeset('Reltol',1e-4,'AbsTol',1e-4, 'Stats','on');
[t,c] = ode113(@(t,c) two_level_sol(t, c, omega_0, omega_t, Omega_t, t_sample), tspan, c0, opts);

plot(t,abs(c(:,1)).^2,'-o',t,abs(c(:,2)).^2,'-.')