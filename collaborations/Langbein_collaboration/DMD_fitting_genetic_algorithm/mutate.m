function child = mutate(A1, ind)

temp = randperm(ind);
ind2 = temp(1);

child = A1;
if child(ind2) == 0
    child(ind2) = 1;
else
    child(ind2) = 0;
end