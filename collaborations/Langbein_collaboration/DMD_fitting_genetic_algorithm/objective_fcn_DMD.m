function out = objective_fcn_DMD(x, xc)

global X2D X_SC LAMBDA

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.43 for
%787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
%is why we see single atoms)
NA = 0.68; %Specified NA of hires objective (unitless)
% NA = 0.51; %Measured effective NA of the hires objective (787 system)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
E = ones(size(X2D)); %amplitude of field (in lattice recoils)
PSF = E.*make_bessel(NA, a, zp, f, LAMBDA, 0, 0);

%% input grid

dim = xc{2};
xround = round(x*(2^(dim^2)));
num = zeros(1, dim^2);
bin = dec2bin(xround, dim^2);
for i = 1:dim^2
    num(i) = str2double(bin(i));
end
DMD_center = reshape(num, dim, dim);

px_x = 1920;
px_y = 1080;
ind_x0_DMD = ceil(px_x/2)+1;
ind_y0_DMD = ceil(px_y/2)+1;
DMD = zeros(px_x, px_y);

DMD(ceil(ind_x0_DMD-max(size(DMD_center))/2):ceil(ind_x0_DMD+(max(size(DMD_center))-2)/2), ceil(ind_y0_DMD-min(size(DMD_center))/2):ceil(ind_y0_DMD+(min(size(DMD_center))-2)/2)) = DMD_center;

%% Do convolutions

spx = 7;

E0 = conv2(PSF, DMD/(spx^2), 'same');

%normalize
E0 = E0/max(max(abs(E0)));

diff = abs(E0).^2 - xc{1};

out = sum(sum(abs(diff))); % minimize the residual