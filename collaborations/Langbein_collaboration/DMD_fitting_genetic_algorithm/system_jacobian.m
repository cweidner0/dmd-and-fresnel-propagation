function J_out = system_jacobian(t, tau_pulse, Omega_max, omega_0, omega_sweep)

[omega, Omega] = make_HS1(t, tau_pulse, Omega_max, omega_0, omega_sweep);

J_out = zeros(2,2);

J_out(1,2) = (1i*Omega/2)*(exp(1i*(omega - omega_0)*t)+exp(-1i*(omega + omega_0)*t));
J_out(2,1) = (1i*Omega/2)*(exp(1i*(omega + omega_0)*t)+exp(-1i*(omega - omega_0)*t));