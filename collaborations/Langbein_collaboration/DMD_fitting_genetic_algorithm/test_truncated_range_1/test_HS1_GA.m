close all
clc

rng('shuffle')

%ODE solver options
opts = odeset('Reltol',1e-4,'AbsTol',1e-4);
% opts = odeset('Reltol',1e-3,'AbsTol',1e-3, 'Stats','on');
% opts = odeset('Stats','on');

%physics
omega_0 = 2*pi*6.8e6; %Lowercase for atomic transition frequencies
%off resonant magnetic field shit
d1plane = 0.5e-4; %distance of 1 plane in cm
gradB = 50; %gradient in G/cm
B = gradB*d1plane;
delta = 2*0.7e6*B;

c_init = [1 0]; %atoms start in ground state

%limits to parameters
tau_lo = 100e-6;
tau_hi = 10e-3;
Omega_lo = 5e2;
Omega_hi = 5e3;
omega_lo = 1e1;
omega_hi = 1e4;

N_child = 20;
N_iter = 100;

t_sample_size = 1e3;

param = rand(3, N_child);

c0 = zeros(1,N_child);
c1 = zeros(1,N_child);

avg = zeros(1, N_iter);
bestfit = zeros(1, N_iter);

mut_bound = 0.3;
creeprate = 0.3;

for k = 1:N_iter
    k
    for i = 1:N_child
        tau_pulse = (tau_hi-tau_lo)*param(1,i) + tau_lo;
        Omega_max = (Omega_hi-Omega_lo)*param(2,i) + Omega_lo;
        omega_sweep = (omega_hi-omega_lo)*param(3,i) + omega_lo;
        [omega_pulse, Omega_pulse, t_sample] = make_HS1_GA(t_sample_size, tau_pulse, Omega_max, omega_0, omega_sweep);
        tspan = [0, tau_pulse];
        [t_res,c_res] = ode113(@(t_res,c_res) two_level_sol(t_res, c_res, omega_0, omega_pulse, Omega_pulse, t_sample), tspan, c_init, opts);
        [t_nonres,c_nonres] = ode113(@(t_nonres,c_nonres) two_level_sol(t_nonres, c_nonres, omega_0-delta, omega_pulse, Omega_pulse, t_sample), tspan, c_init, opts);
        c0(i) = abs(c_res(length(t_res),2))^2;
        c1(i) = abs(c_nonres(length(t_nonres),2))^2;
    end
    fit = get_fitness(c0, c1);
    avg(k) = mean(fit);
    bestfit(k) = min(fit);
    if k < N_iter
        param = gen_alg(param, fit, creeprate);
    end
end

param_best = param(:,1);

csvwrite('avg.csv', avg)
csvwrite('bestfit.csv', bestfit)
csvwrite('param_best.csv', param_best)
csvwrite('param.csv', param)
csvwrite('c0.csv', c0)
csvwrite('c1.csv', c1)

save('GA_out.mat')