function cdot = two_level_sol(t, c, omega_0, omega_t, Omega_t, t_sample)

Omega_t_interp = interp1(t_sample, Omega_t, t, 'spline');
omega_t_interp = interp1(t_sample, omega_t, t, 'spline');

cdot = zeros(2,1);

cdot(1) = (1i/2)*c(2).*Omega_t_interp.*(exp(1i*t.*(omega_t_interp-omega_0)) + exp(-1i*t.*(omega_t_interp+omega_0)));
cdot(2) = (1i/2)*c(1).*Omega_t_interp.*(exp(1i*t.*(omega_t_interp+omega_0)) + exp(-1i*t.*(omega_t_interp-omega_0)));