function img_disc_final = Floyd_Steinberg(disc, img_in)

% Function that takes an input image (img_in) and applies the Floyd-Steinberg
% dithering algorithm to the image to produce a dithered image with a pixel
% resolution described by the variable disc

% if disc = 1, then the image will be binary (as it should be for our DMDs)

% see here: https://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering
% and: https://se.mathworks.com/matlabcentral/fileexchange/33342-floyd-steinberg-dithering-algorithm

% This appears to behave better than the MATLAB dither() command in light
% and dark areas!
% It is about a factor of 2 slower, however. For the fifth square Zernike
% polynomial (see square_Zernike.m), this code takes 0.15 s to run vs. 0.08
% for the MATLAB version.

[px_x, px_y] = size(img_in);

% map between zero and one
max_img = max(max(img_in));
min_img = min(min(img_in));
img_in = (img_in - min_img)/(max_img - min_img);

img_disc = zeros(size(img_in));

for i = 1:px_x
    for j = 1:px_y
        img_disc(i,j) = round(disc*img_in(i,j));
        err = img_in(i,j) - img_disc(i,j);
        if i < px_x %not bottom row
            if j == 1 %top row
                img_in(i,j+1) = img_in(i, j+1) + err*(7/13);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/13);
                img_in(i+1,j+1) = img_in(i+1,j+1) + err*(1/13);
            elseif j > 1 && j < px_y
                img_in(i,j+1) = img_in(i, j+1) + err*(7/16);
                img_in(i+1,j-1) = img_in(i+1,j-1) + err*(3/16);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/16);
                img_in(i+1,j+1) = img_in(i+1,j+1) + err*(1/16);
            else
                img_in(i+1,j-1) = img_in(i+1,j-1) + err*(3/8);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/8);
            end
        else %bottom row
            if j > px_y
                img_in(i, j+1) = img_in(i, j+1) + err;
            end
        end
    end
end

img_disc_final = round(disc*img_in);