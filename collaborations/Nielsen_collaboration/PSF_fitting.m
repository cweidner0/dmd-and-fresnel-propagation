%Script to simulate the images from the 0.92 NA microscope objective used
%in A. Alberti's group
%This script puts one atom in the center of the lattice which makes it easy
%to look at PSFs
%Carrie Weidner
%Aarhus University
%3 May 2020

close all; clc; clear all;

%% Options

export_abs = 1; %exports images
plot_xcut = 1;
plot_ycut = 1;

n_imgs = 1; % number of images to generate

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm
V0 = 20; %lattice depth in recoils

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 532e-9;
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_SC/20; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_SC/20;
% total number of pixels
n_sites = 27; %this must be odd(and a multiple of 9 if you want to contain only unit cells)
px_x = n_sites*10;
% the total span of each image is about 63 �m, so 32 �m in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
zlim = 0; % total number of planes (here only care about one)

%set up the convolution
DMD = zeros(px_x, px_x);

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = 0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.69; %Specified NA of hires objective (unitless)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective

%% Field parameters

% actually generate PSF
% this PSF is the typical Bessel beam multiplied by a small hyperbolic
% tangent component that mimics the asymmetry shown in Fig. 3 of
% https://arxiv.org/pdf/1611.02159.pdf
PSF = abs(make_bessel(NA, a, zp, f, lambda, 0, 0)).^2;
abs_PSF = abs(PSF).^2;

x_Composer = 4*x/LAMBDA_LATT_SC;
PSF_Composer = abs_PSF(:, x0_ind);

f = fit(x_Composer', PSF_Composer, 'gauss1');
cfit = f.c1

f2 = fit(x', PSF_Composer, 'gauss1')
cfit2 = f2.c1

PSF_Composer_Gauss = exp(-(x_Composer/cfit).^2);

figure
hold on
plot(x_Composer, -cos(K_LATT*x).^2 + 1, 'k', 'LineWidth', 1.5)
plot(x_Composer, PSF_Composer, 'r--', 'LineWidth', 1.5)
plot(x_Composer, PSF_Composer_Gauss, 'b:', 'LineWidth', 1.5)
xlabel('Distance (sites)')
ylabel('Amplitude')
grid on
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
legend('lattice', 'PSF', 'Gauss fit')

depths = [2.5, 5, 10, 20, 30, 40];
holes = [5, 2.5, 0.6, 0.1, 0.02, 0.006];
ratio = holes./depths;
fe1 = fit(depths', holes', 'exp1')
fe2 = fit(depths', holes', 'exp2')
aefit1 = fe1.a;
befit1 = fe1.b;
aefit2 = fe2.a;
befit2 = fe2.b;
cefit2 = fe2.c;
defit2 = fe2.d;
xefit = linspace(2.5, 40, 100);
efit1 = aefit1*exp(befit1*xefit);
efit2 = aefit2*exp(befit2*xefit) + cefit2*exp(defit2*xefit);
figure
hold on
plot(depths, holes, 'r.', 'MarkerSize', 12)
plot(xefit, efit1, 'b-.', 'LineWidth', 1.5)
plot(xefit, efit2, 'k:', 'LineWidth', 1.5)
plot(depths, holes, 'r.', 'MarkerSize', 12)
set(gca, 'YScale', 'log')
xlabel('Lattice depth (recoils)')
ylabel('Dimple depth (recoils)')
grid on
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
legend('Composer data, 1% level', 'Single exp fit', 'Double exp fit')

fe1 = fit(depths', ratio', 'exp1')
fe2 = fit(depths', ratio', 'exp2')
aefit1 = fe1.a;
befit1 = fe1.b;
aefit2 = fe2.a;
befit2 = fe2.b;
cefit2 = fe2.c;
defit2 = fe2.d;
xefit = linspace(2.5, 40, 100);
efit1 = aefit1*exp(befit1*xefit);
efit2 = aefit2*exp(befit2*xefit) + cefit2*exp(defit2*xefit);
figure
hold on
plot(depths, ratio, 'r.', 'MarkerSize', 12)
plot(xefit, efit1, 'b-.', 'LineWidth', 1.5)
plot(xefit, efit2, 'k:', 'LineWidth', 1.5)
plot(depths, ratio, 'r.', 'MarkerSize', 12)
set(gca, 'YScale', 'log')
xlabel('V_0 (recoils)')
ylabel('v/V_0')
grid on
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
legend('Composer data', 'Single exp fit', 'Double exp fit')