%PSF Leakages onto neighboring sites
%Carrie Weidner
%Aarhus University
%13 April 2019

close all; clc

%% Options

load_DMD_image = 0; %loads DMD image from bitmap file
plot_xcut = 1; % plots an x-cut along the position xd
plot_ycut = 0; % plots a y-cut along the position yd
plot_lattice = 1; %plot background lattice with images

% ideally, this should work just fine if we define a superpixel and do the
% convolution, but this does not work right now
% so keep this at zero
do_superpixel = 0;
% use the FFT method to do the Fresnel propagation if this is set to 1
% otherwise use the convolutional method
% FFT method is faster and thus recommended
do_FFT = 1;

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
% lambda_proj = 780e-9; % atoms
% lambda_proj = 787e-9; % spin-addressing light
% lambda_proj = 940e-9; % potential-changing light
lambda_proj = 1064e-9; % lattice wavelength
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
% a = 2;
% xlim = a*LAMBDA_LATT_SC;
% ylim = a*LAMBDA_LATT_SC;
% grid resolution (this sets the spacing of the dots because of the
% intricacies of the convolution operation, I am looking into how to fix
% this)
% when dealing with DMD images:
% the spacing as a function of resolution is (roughly) given by
% spacing(m) = X_SC*dx/1e-2;
% This is based on numerical scaling
% and the experimental magnification of 1/100
exp_spacing = 7.56e-6;
% zT is for imaging Talbot planes
% NB: There is a bug somewhere, and the Talbot planes show up a factor of 2
% sooner than they should (that is, the doubled plane shows up at zT/2)
% see https://en.wikipedia.org/wiki/Talbot_effect
% I suspect the issue is somewhere in the scaling between experimental
% spacing (exp_spacing) and spacing in the program
% zT = 2*exp_spacing^2/lambda_proj;
zT = (lambda_proj/(1-sqrt(1-(lambda_proj/exp_spacing)^2)));
zlim = LAMBDA_LATT_SC; %1 in scaled units is about 239 nm
% zlim = zT/X_SC;
% dx = exp_spacing*1e-2/X_SC;
dx = 105e-9/X_SC/20; % each hires pixel is 105 nm
dy = 105e-9/X_SC/20;
% the total span of each image is about 20 �m, so 10�m in each direction
% xlim = 238*dx/2;
% ylim = xlim;
px = 1021;
% px = 21;
xlim = floor(px/2)*dx;
ylim = floor(px/2)*dy;
dz = LAMBDA_LATT_SC/2; %z-resolution is one plane

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y X2D Y2D

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim).^2 + (Y2D/ylim).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y] = meshgrid(x,y);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% Field parameters

E = ones(size(X2D)); %amplitude of field (in lattice recoils)

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.43 for
%787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
%is why we see single atoms)
NA = 0.69; %Specified NA of hires objective (unitless)
% NA = 0.51; %Measured effective NA of the hires objective (787 system)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

%% DMD parameters

spx = 7; %superpixel size

%regular pixels
px_x = 1920;
px_y = 1080;

%superpixels
spx_x = floor(px_x/spx); %we take the floor because we need integers
spx_y = floor(px_y/spx); %half a superpixel will do us little good, so we take the floor

DMD = zeros(px_x, px_y);
sDMD = zeros(spx_x, spx_y);

%% input grid

%the code below basically finds the center of the DMD
ind_x0_DMD = ceil(px_x/2)+1;
ind_y0_DMD = ceil(px_y/2)+1;
ind_x0_sDMD = ceil(spx_x/2)+1;
ind_y0_sDMD = ceil(spx_y/2)+1;

% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!

if load_DMD_image == 1 %load bitmap
    % currently this reads images from the "DMD_images" subfolder
    filename = 'R10_10by10_array.bmp';
    foldername = 'DMD_images';
    if ispc
        % Windows
        DMD = imread(['.\', foldername, '\', filename]);
    else
        % Unix-based system
        DMD = imread(['./',foldername, '/', filename]);
    end
    %     DMD = imread('single_R7_dimple.bmp');
else %write code to generate the DMD image you want
    % single spot
    DMD(ind_x0_DMD, ind_y0_DMD) = 1;
end

%% Do convolutions

if do_superpixel == 1
    E0 = conv2(PSF, sDMD, 'same');
else
    E0 = conv2(PSF, DMD/(spx^2), 'same');
end

%normalize
E0 = sqrt(5)*E0/max(max(abs(E0)));

I0 = abs(E0).^2;

%% Lattice parameters

if plot_lattice == 1
    V0 = 10000; %amplitude of lattice (in recoils)
    Vlatt_2D = V0*(cos(K_LATT*X2D).^2 + cos(K_LATT*Y2D).^2 - 1); %only do 2D lattice
else
    Vlatt_2D = zeros(size(X2D));
end

%% Plotting

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% colors!
colors = {'r', 'b', 'k', 'g', 'm', 'r--', 'b--', 'k--', 'g--', 'm--', 'r:', ...
    'b:', 'k:', 'g:', 'm:'};

% plotting limits
[~, xd_ind] = min(abs(x + xd));
[~, yd_ind] = min(abs(y + yd));
dim_x = xlim;
dim_y = ylim;
% dim_x = 19;
% dim_y = 19;
plotlim_x = 19;
plotlim_y = 19;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);

% aspect ratios
% DMD plots
if do_superpixel == 1
    DMD_aspect = spx_y/spx_x;
else
    DMD_aspect = px_y/px_x;
end
% other 2D plots
image_aspect = plotlim_x/plotlim_y;

% plot y-cut at position yd
if plot_ycut == 1
    figure
    hold on
    title('Y-cut')
    plot(y(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
        -Vlatt_2D(yd_ind, ylim_neg_ind:ylim_pos_ind), colors{2})
    plot(y(ylim_neg_ind:ylim_pos_ind)'*X_SC*1e6,...
        -abs(E0(yd_ind,ylim_neg_ind:ylim_pos_ind)).^2 -...
        Vlatt_2D(yd_ind, ylim_neg_ind:ylim_pos_ind), colors{1})
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('y (�m)')
    ylabel('Amplitude (arb)')
    axis([-plotlim_x*X_SC*1e6, plotlim_x*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% plot x-cut at position xd
if plot_xcut == 1
    figure
    hold on
    title('X-cut')
    plot(x(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
        -Vlatt_2D(xlim_neg_ind:xlim_pos_ind, xd_ind), colors{2})
    plot(x(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
        -abs(E0(xlim_neg_ind:xlim_pos_ind,xd_ind)).^2 - ...
        Vlatt_2D(xlim_neg_ind:xlim_pos_ind, xd_ind), colors{1})
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('Amplitude (arb)')
    axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end
