close all;

NA = 0.9;
lambda = 532e-9;

lambda_latt = 1064e-9;
d_site = lambda_latt/2;
k = 2*pi/lambda_latt;

x = -5*d_site:d_site/100:5*d_site;
y = -5*d_site:d_site/100:5*d_site;

x0_ind = find(x==0);
y0_ind = find(y==0);

global X2D Y2D
[X2D, Y2D] = meshgrid(x,y);

I0 = abs(make_bessel(NA, 1, 1, 1, lambda, 0,0)).^2;
I1 = abs(make_bessel(NA, 1, 1, 1, lambda, 0, d_site)).^2;

I0_cut = I0(:,y0_ind);
I1_cut = I1(:,y0_ind);

figure
hold on
plot(x, I0_cut)
plot(x, I1_cut)
plot(x, cos(2*k*x), 'k')

% overlap = 