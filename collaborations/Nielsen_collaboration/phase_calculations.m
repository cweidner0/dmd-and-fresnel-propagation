close all; clear all; clc;

%load data
phase_data = table2array(readtable('lattice_phase_parameters.csv'));
n_sites = 9; % fractal lattice size
% note we only have 8 hopping parameters

abs_phase_data = abs(phase_data.^2);
angle_phase_data = angle(phase_data);

% prep for plotting
phase_abs_plot = abs_phase_data;
phase_abs_plot(phase_abs_plot == 0) = NaN;
phase_angle_plot = angle_phase_data;
phase_angle_plot(phase_angle_plot == 0) = NaN;

%pcolor is stupid
sites = 1:n_sites;
sites2 = 1:n_sites + 1;
phase_abs_plot = [phase_abs_plot; phase_abs_plot(n_sites, :)];
phase_abs_plot = [phase_abs_plot, phase_abs_plot(:, n_sites-1)];
phase_angle_plot = [phase_angle_plot; phase_angle_plot(n_sites, :)];
phase_angle_plot = [phase_angle_plot, phase_angle_plot(:, n_sites-1)];

% figure('Position', [100, 100, 1000, 400])
% subplot(1,2,1)
% pcolor(sites2, sites, phase_abs_plot')
% shading flat
% colorbar
% colormap jet
% yticks(sites+1/2)
% yticklabels(strtrim(cellstr(num2str(sites'))'))
% xticks(sites+1/2)
% xticklabels(strtrim(cellstr(num2str(sites'))'))
% title('Tunneling magnitude along rows')
% ylabel('J_{j, j+1}')
% xlabel('Row i')
% set(gca, 'FontSize', 12, 'FontWeight', 'bold')
% set(gca, 'Color', 'k')
% subplot(1,2,2)
% pcolor(sites, sites2, phase_abs_plot)
% shading flat
% colorbar
% colormap jet
% yticks(sites+1/2)
% yticklabels(strtrim(cellstr(num2str(sites'))'))
% xticks(sites+1/2)
% xticklabels(strtrim(cellstr(num2str(sites'))'))
% title('Tunneling magnitude along columns')
% xlabel('J_{i,i+1}')
% ylabel('Column j')
% set(gca, 'FontSize', 12, 'FontWeight', 'bold')
% set(gca, 'Color', 'k')

figure('Position', [100, 100, 1000, 400])
subplot(1,2,1)
pcolor(sites, sites2, phase_angle_plot)
shading flat
h = colorbar;
h.Label.String = '\phi_{j,j+1}';
pos = get(h,'Position');
h.Label.Position = [pos(1)+2.5 pos(2) + 0.1]; % to change its position
h.Label.Rotation = 0; % to rotate the text
colormap jet
yticks(sites+1/2)
yticklabels(strtrim(cellstr(num2str(sites'))'))
xticks(sites+1/2)
xticklabels(strtrim(cellstr(num2str(sites'))'))
title('Tunneling phase along columns')
xlabel('Site j')
ylabel('Row i')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
set(gca, 'Color', 'k')
caxis([-pi, pi])
subplot(1,2,2)
pcolor(sites2, sites, phase_angle_plot')
shading flat
h = colorbar;
xlabel(h, '\phi_{i,i+1}');
pos = get(h,'Position');
h.Label.Position = [pos(1)+2 pos(2) + 0.1]; % to change its position
h.Label.Rotation = 0; % to rotate the text
colormap jet
yticks(sites+1/2)
yticklabels(strtrim(cellstr(num2str(sites'))'))
xticks(sites+1/2)
xticklabels(strtrim(cellstr(num2str(sites'))'))
title('Tunneling phase along rows')
ylabel('Site i')
xlabel('Column j')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
set(gca, 'Color', 'k')
caxis([-pi, pi])

% figure
% imagesc(angle_phase_data)
% colormap jet
% colorbar
% 
% phase_angle_plot = angle_phase_data;
% phase_angle_plot(phase_angle_plot == 0) = NaN;
% figure('Position', [0, 0, 1000, 1000])
% for i = 1:n_sites
%     subplot(3,3,i)
%     plot(1:n_sites-1, phase_angle_plot(i,:), '.', 'MarkerSize', 12)
%     xlabel('Row')
%     ylabel('\phi_{i, i+1} (rad)')
%     title(['Column ', num2str(i)])
%     xlim([1,8])
%     ylim([-pi, pi])
%     grid on
% end
% for i = 1:4
%     subplot(2,2,i)
%     plot(1:n_sites-1, phase_angle_plot(i,:), '.', 'MarkerSize', 12)
%     xlabel('Row')
%     ylabel('\phi_{i, i+1} (rad)')
%     title(['Column ', num2str(i)])
%     xlim([1,8])
%     ylim([0, pi])
%     grid on
% end