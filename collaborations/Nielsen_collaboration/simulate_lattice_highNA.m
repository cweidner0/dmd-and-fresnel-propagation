%Script to simulate the images from the 0.92 NA microscope objective used
%in A. Alberti's group
%This script puts one atom in the center of the lattice which makes it easy
%to look at PSFs
%Carrie Weidner
%Aarhus University
%3 May 2020

close all; clc; clear all;

%% Options

export_abs = 1; %exports images
plot_xcut = 1;
plot_ycut = 1;

n_imgs = 1; % number of images to generate

% all numbers from https://arxiv.org/pdf/1611.02159.pdf

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 275.5 nm
V0 = 20; %lattice depth in recoils

% the bare variable "lambda" is the (scaled) wavelength of the imaging
% light ("lambda_proj", so-called because this was originally a script
% for calculating how light could be projected up through an objective)
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 532e-9;
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
dx = LAMBDA_LATT_SC/20; % let's just say that 1 pixel is 1/10 lattice spacing
dy = LAMBDA_LATT_SC/20;
% total number of pixels
n_sites = 27; %this must be odd(and a multiple of 9 if you want to contain only unit cells)
px_x = n_sites*10;
% the total span of each image is about 63 �m, so 32 �m in each direction
xlim_px = px_x*dx/2;
ylim_px = xlim_px;
zlim = 0; % total number of planes (here only care about one)

%set up the convolution
DMD = zeros(px_x, px_x);

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim_px+dx:dx:xlim_px;
len_x = length(x);
y = -ylim_px+dy:dy:ylim_px;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = 0;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim_px).^2 + (Y2D/ylim_px).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
NA = 0.69; %Specified NA of hires objective (unitless)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective

%% Field parameters

% actually generate PSF
% this PSF is the typical Bessel beam multiplied by a small hyperbolic
% tangent component that mimics the asymmetry shown in Fig. 3 of
% https://arxiv.org/pdf/1611.02159.pdf
PSF = abs(make_bessel(NA, a, zp, f, lambda, 0, 0)).^2;
% below is a simple test to get apodization to work, not currently used
% f_eff = 11.96e-3;
% A_sine = 1./sqrt(1 - (X2D.^2 + Y2D.^2)/f_eff^2); % apodization fcn
% pupil_fcn = sqrt(A_sine);
%
% PSF = abs(fftshift(fft2(pupil_fcn.*E))).^2;

%% placing atoms

% where on our pixel grid do we see atoms?
% Because this is adapted from previous code, we call this the DMD
% IT'S REALLY ATOMS DON'T FALL FOR MY LIES
% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!
n_planes = len_z;
spx = 1; %was 7
%place the atoms only on valid lattice sites
n_px_grid = 5; % how many pixels in one lattice site?
atom_pix = 400; %where we load atoms, total extent px_x pixels
lim = atom_pix*dx/2;
xlim_atoms = lim;
x_atoms = -xlim_atoms+dx:dx:xlim_atoms;
x_latt = LAMBDA_LATT_SC/2; %distance between lattice sites
site_inds = 5:10:px_x-5;
site_mesh = meshgrid(site_inds, site_inds); % make a mesh of all available sites

%% Fresnel propagation
% run this multiple times per image

n_atoms_plane = zeros(1,n_planes);
n_atoms_plane(ceil(n_planes/2)) = 1;
%     n_atoms_plane(1) = 1;
n_atoms_total = sum(n_atoms_plane);

atom_locs = cell(1,len_z);

d_dimple = 4; %dimple depth
d_adj = 0; %adjusted depth

DMD_inds = site_inds + 1;

a = 1;
I0 =  -V0*(cos(K_LATT*X2D).^2 + cos(K_LATT*Y2D).^2-1);
% build in holes
hole_rows = 2:3:n_sites;
big_plug_rows = 5:9:n_sites;
adj_rows = 3:3:n_sites;
adj_rows_2 = 1:3:n_sites;
d_small_plug = -3.75;
d_off_rows = -1.5;
d_big_plug = -16;
d_intermediate = -2.5*.5 - d_off_rows ;
DMD_off_inds = 1:10:length(DMD);
% the off rows (to make everything flat)
for i = 1:length(DMD_inds)
    for j = 1:length(DMD_inds)
        DMD(DMD_off_inds(i), DMD_off_inds(j)) = d_off_rows;
    end
end
% the small holes
for i = 1:length(hole_rows)
    DMD(DMD_inds(hole_rows(i)), 1) = d_intermediate;
    DMD(1, DMD_inds(hole_rows(i))) = d_intermediate;
    for j = 1:length(hole_rows)
        DMD(DMD_inds(hole_rows(i)), DMD_inds(hole_rows(j))) = d_small_plug;
        DMD(DMD_inds(hole_rows(i)), DMD_inds(adj_rows(j)) + 5) = d_intermediate;
        DMD(DMD_inds(adj_rows(j)) + 5, DMD_inds(hole_rows(i))) = d_intermediate;
    end
end
% the big plugs
for i = 1:length(big_plug_rows)
    for j = 1:length(big_plug_rows)
        DMD(DMD_inds(big_plug_rows(i))-5, DMD_inds(big_plug_rows(j))-5) = d_big_plug;
        DMD(DMD_inds(big_plug_rows(i))+5, DMD_inds(big_plug_rows(j))-5) = d_big_plug;
        DMD(DMD_inds(big_plug_rows(i))-5, DMD_inds(big_plug_rows(j))+5) = d_big_plug;
        DMD(DMD_inds(big_plug_rows(i))+5, DMD_inds(big_plug_rows(j))+5) = d_big_plug;
%         DMD(DMD_inds(big_plug_rows(i)), DMD_inds(big_plug_rows(j))) = d_big_plug -2;
    end
end
DMD_tot = conv2(PSF, DMD, 'same');
Iadj = I0 + DMD_tot;

figure
imagesc(DMD)
colorbar

%% Plotting

% plotting limits
dim_x = xlim_px;
dim_y = ylim_px;
plotlim_x = dim_x;
plotlim_y = dim_y;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);

image_aspect = plotlim_x/plotlim_y;

% plot the magnitude of the field at all Fresnel points
% and export the images
%     Lmax = 750;

% [cmap_nonlin, ticks, labels] = nonlinearCmap(parula(500), 0.1, clim, 2, 0.1);

Lmax = 1;
if export_abs
    for i = 1:n_planes
        %         imagesc(x*X_SC*1e6, y*X_SC*1e6, I0)
        %         imagesc(x, y, I0)
        figure
        imagesc(Iadj)
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
        %         caxis([0 V0])
        %         colormap(hires_color_map(90))
        %         colormap(cmap_nonlin)
        colorbar
        % uncomment me to write images
        % imwrite(img, ['Z:\experiment\Images\simulated_QGM_images\', file, '\', file, '_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
        %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
        %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
    end
end

if plot_xcut
    xplane = [1,2,3,4,5,6,7,8,9];
    figure('Position', [100, 100, 1200, 900])
    hold on
    for i = 1:9
        subplot(3,3,i)
        hold on
        plot(x, I0(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        plot(x, DMD_tot(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        plot(x, Iadj(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        xlabel('x (px)')
        ylabel('intensity')
        if i == 9
            legend('lattice', 'DMD', 'total')
        end
        set(gca, 'FontSize', 12, 'FontWeight', 'bold')
        grid on
        title(['Lattice along x-row ', num2str(xplane(i))])
        ylim([-40, 0])
        xlim([min(x), max(x)])
    end
    figure('Position', [100, 100, 1200, 900])
    hold on
    for i = 1:9
        subplot(3,3,i)
        hold on
        plot(x, I0(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        plot(x, DMD_tot(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        plot(x, Iadj(site_inds(xplane(i)), :), 'LineWidth', 1.5)
        xlabel('x (px)')
        ylabel('intensity')
        if i == 9
            legend('lattice', 'DMD', 'total')
        end
        set(gca, 'FontSize', 12, 'FontWeight', 'bold')
        grid on
        title(['Lattice along x-row ', num2str(xplane(i))])
        ylim([-40, 0])
        xlim([min(x), max(x)])
    end
end

if plot_ycut
    yplane = [1,2,3,4,5,6,7,8,9];
    figure('Position', [100, 100, 1200, 900])
    for i = 1:9
        subplot(3,3,i)
        hold on
        plot(x, I0(:, site_inds(yplane(i))), 'LineWidth', 1.5)
        plot(x, DMD_tot(:, site_inds(yplane(i))), 'LineWidth', 1.5)
        plot(x, Iadj(:, site_inds(yplane(i))), 'LineWidth', 1.5)
        xlabel('x (px)')
        ylabel('intensity')
        if i == 9
            legend('lattice', 'DMD', 'total')
        end
        set(gca, 'FontSize', 12, 'FontWeight', 'bold')
        grid on
        title(['Lattice along y-row ', num2str(xplane(i))])
        ylim([-21, -20])
        xlim([min(x), max(x)])
    end
end
