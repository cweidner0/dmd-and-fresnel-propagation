% Script to generate optical potential from DMD
% Includes lattice with a given phase, calculates \Delta values
% Carrie Weidner
% University of Bristol
% 13 February 2024

close all; clc

%% Options

plot_DMD = 0; %plots image loaded onto DMD
plot_init = 0;
plot_abs = 0; %plots magnitude of image
plot_phase = 0; %plots phase of image
plot_xcut = 1; % plots cuts about centre of image in x-, y-, or z-
plot_ycut = 0;
plot_zcut = 0;
plot_lattice = 1; %plot background lattice with images

% add phase front (model aberrations)
do_phase_front = 0;
% calibrate your scaling factor (see scf)
do_calibrate = 1;
% get delta values
get_deltas = 1;

% phase of underlying lattice
% phase currently only added along x
lattice_phase = 0;

% overall field amplitude (this will scale the power in the DMD laser)
I_sc = 0.1;
E_sc = sqrt(I_sc);

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 940e-9; % potential-changing light
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;

% set up spatial grid (scaled units)
exp_spacing = 5.71e-6;
zlim = 2*532e-9/X_SC; % plus or minus 2 lattice sites
xlim = 25;
ylim = xlim;
dz = zlim/2;

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

len_x = 512;
len_y = 512;
x = linspace(-xlim, xlim, len_x);
y = linspace(-ylim, ylim, len_y);
dx = x(2) - x(1);
dy = y(2) - y(1);

% x = -xlim:dx:xlim;
n_test = -4:4;
% len_x = length(x);
% y = -ylim:dy:ylim;
% len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = 0:dz:zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% figure out our indices where we expect lattice minima to be
Deltas_positions = (n_test*pi + lattice_phase)/K_LATT; % where we will calculate the \Deltas
Deltas_inds = zeros(size(Deltas_positions));
for i = 1:length(Deltas_positions)
    [~,Deltas_inds(i)] = min(abs(x-Deltas_positions(i)));
end

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim).^2 + (Y2D/ylim).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    % add Zernike polynomials phase front
    % https://en.wikipedia.org/wiki/Zernike_polynomials
    % scale by a factor sc
    % Note that for best results, this should be compared to real data
    % or theoretical simulations as getting this scale factor right is
    % super tricky
    % the user can add whatever polynomials they like, just make sure
    % they are both in Zp and coeffs
    sc = max(max(X2D));
    XZ = X2D/sc;
    YZ = Y2D/sc;
    Rmax = max(max(XZ));
    R2D_sq = (XZ).^2 + (YZ).^2; % radius squared
    R2D_sq_minus = (XZ).^2 - (YZ).^2; % this guy pops up a lot too
    Zp20 = sqrt(3)*(2*R2D_sq - 1); %defocus
%     Zp2n2 = 2*sqrt(6)*XZ.*YZ; % oblique astigmatism
    Zp22 = sqrt(6)*R2D_sq_minus; % vertical astigmatism
%     Zp3n1 = sqrt(8)*(3*R2D_sq*YZ-2*YZ); % y-coma
    Zp31 = sqrt(8)*(3*R2D_sq.*XZ-2*XZ); % x-coma
    Zp33 = sqrt(8)*(4*XZ.^3 - 3*R2D_sq.*XZ); % trefoil
    Zp40 = sqrt(5)*(6*R2D_sq.^2 - 6*R2D_sq + 1); % primary spherical aberration
    Zp42 = sqrt(10)*(4*R2D_sq - 3).*R2D_sq_minus; % vertical secondary astigmatism
    Zp = {Zp20, Zp22, Zp31, Zp33, Zp40, Zp42}; %0.05
    
    % vector preallocation
    phase_factor = zeros(size(X2D));
    delta = zeros(length(coeffs));
    
    % Zernike coefficients (in units of wavelength, but remember
    % that the scaling factor is tricky)
    coeffs = [0,0,0,0,0,0];
    
    % build the phase factor
    for i = 1:length(Zp)
        Zp_cont = coeffs(i)*Zp{i};
        phase_factor = phase_factor + Zp_cont;
        Zp_cont(R2D_sq > Rmax^2) = NaN;
        delta(i) = max(max(Zp_cont)) - min(min(Zp_cont));
    end
    
    % and apply it
    E = exp(1i*2*pi*phase_factor);

end

%% PSF parameters (Bessel beam)

% Bessel beam
% making the NA higher will make the beam itself smaller
NA = 0.69; % Specified NA of hires objective (unitless)
a = 2.54e-2/2/X_SC; % radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; % working focal distance (12.95 mm to scaled units)
zp = f; % distance from the objective
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

%% DMD parameters

% regular pixels
px_x = 1920;
px_y = 1080;

DMD = zeros(px_x, px_y);

%% input grid

%the code below basically finds the center of the DMD
ind_x0_DMD = ceil(px_x/2)+1;
ind_y0_DMD = ceil(px_y/2)+1;

% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!

% what's loaded on the DMD?
% single spot
DMD(ind_x0_DMD, ind_y0_DMD) = 1;

%% Do convolutions with the DMD pattern

E0 = conv2(PSF, DMD, 'same');

%normalize and scale by the field amplitude
E0 = E_sc*E0/max(max(abs(E0)));
FE0 = fftshift(fft2(E0));

Ediff = E0.*ones(size(x)); %empty vector to store E field at different points on grid

%% Lattice parameters

if plot_lattice == 1
    % only add phase along x
    V0 = 1; %amplitude of lattice (in recoils)
    [X2D_ph, Y2D_ph] = meshgrid(x, y-lattice_phase/K_LATT);
    Vlatt_2D = V0*(cos(K_LATT*X2D_ph).^2 + cos(K_LATT*Y2D_ph).^2 - 1 ); %only do 2D lattice
else
    Vlatt_2D = zeros(size(X2D));
end

%% Do Fresnel propagation

% Wikipedia to the rescue
% see https://en.wikipedia.org/wiki/Fresnel_diffraction#Alternative_forms

tic %start timer
for i = 1:len_z
    if z(i) ~= 0
        % note that the factor 5 in front of z has been set so the code
        % agrees with the experimental data we have!
        % see Fresnel_propagation_with_DMD_with_PTR_data.m to plot the data
        % propTF is from the file exchange
        % you know scf is set right when you load a single pixel on the DMD
        % and you get 0.6895 at /pm 2 lattice planes
        % it depends on dx, dy, and spx
        % turn on do_calibrate option and set scf until the cal_out =
        % 0.6895
        scf = 128;
        [Ediff(:,:,i)] = propTF(E0, len_x, lambda, scf*z(i));
    else
        Ediff(:,:,i) = E0;
    end
end

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if tot = len(z)
nx = 2; %number of plots along the x-direction
ny = 3; %number of plots along y-direction
tot = nx*ny; %total number of plots
if tot > len_z
    % basically, you can't plot any more points than you have calculated
    % this will leave blank plots but it's better than stupid errors
    tot = len_z;
end

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% colors and linestyles!
colors = {'r', 'b', 'k', 'g', 'm', 'r--', 'b--', 'k--', 'g--', 'm--', 'r:', ...
    'b:', 'k:', 'g:', 'm:'};

% plotting limits
[~, xd_ind] = min(abs(x + xd));
[~, yd_ind] = min(abs(y + yd));
dim_x = xlim;
dim_y = ylim;
% dim_x = 3;
% dim_y = 3;
plotlim_x = 11;
plotlim_y = 11;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);
ind0_z = ceil(len_z/2);

% aspect ratios
% DMD plots
DMD_aspect = px_x/px_y;
% other 2D plots
image_aspect = plotlim_x/plotlim_y;


% plot DMD image and the projected image at z = 0
if plot_DMD == 1
    figure('Position', [100 100 800 400])
    subplot(1,2,1)
    imagesc(DMD)
    pbaspect([1 DMD_aspect 1])
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (px)')
    ylabel('y (px)')
    title('DMD Pattern')
    subplot(1,2,2)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (µm)')
    ylabel('y (µm)')
    title('Field at focus')
    pbaspect([1 image_aspect 1])
end

% plot the initial field and it's FFT
if plot_init == 1
    figure('Position', [100 100 600 500])
    subplot(1,2,1)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (µm)')
    ylabel('y (µm)')
    title('Initial field')
    pbaspect([1 image_aspect 1])
    subplot(1,2,2)
    imagesc(fx(xlim_neg_ind:xlim_pos_ind), fy(ylim_neg_ind:ylim_pos_ind), ...
        abs(FE0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)))
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    title('Initial field FFT')
    pbaspect([1 image_aspect 1])
end

% plot the magnitude of the field at all Fresnel points
if plot_abs == 1
    figure('Position', [0,0,400*ny,300*nx])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            Idiff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i))
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(i)*X_SC*1e6), ' µm'])
        xlabel('x (µm)')
        ylabel('y (µm)')
        pbaspect([1 image_aspect 1])
        max(max(Idiff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i)))
        caxis([0 1])
    end
end

% plot the phase of the field at all Fresnel points
if plot_phase == 1
    figure('Position', [0,0,600,500])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            angle(Ediff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i)))
        colorbar
        caxis([-pi pi])
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['FFT, z = ', num2str(z(i)*X_SC*1e6), ' µm'])
        xlabel('x (µm)')
        ylabel('y (µm)')
        pbaspect([1 image_aspect 1])
    end
end

% plot y-cut at position yd
if plot_ycut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('Y-cut')
    for i = 1:tot
        plot(y(ylim_neg_ind:ylim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(yd_ind,ylim_neg_ind:ylim_pos_ind,i)).^2 -...
            Vlatt_2D(yd_ind, ylim_neg_ind:ylim_pos_ind), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6), ' µm'];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('y (µm)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_x*X_SC*1e6, plotlim_x*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% plot x-cut at position xd
if plot_xcut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('X-cut')
    for i = 1:tot
        plot(x(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(xlim_neg_ind:xlim_pos_ind,xd_ind,i)).^2 - ...
            Vlatt_2D(xlim_neg_ind:xlim_pos_ind, xd_ind), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6)];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (µm)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% for calibration, adjust scf until cal_out = 0.6895 (or so)
if do_calibrate
    a = reshape(abs(Ediff(yd_ind,xd_ind,:)).^2, 1, len_z);
    cal_out = a(1)/I_sc
end

% plot z-cut at position xd, yd
if plot_zcut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('Z-cut')
    for i = 1:tot
        plot(z*X_SC*1e6,...
            reshape(abs(Ediff(yd_ind,xd_ind,:)).^2 - ...
            Vlatt_2D(yd_ind, xd_ind), 1, len_z), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6)];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (µm)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    %     axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, 0, max(max(abs(E0).^2 + Vlatt_2D))])
    grid on
end

if get_deltas
    potential_at_deltas = -abs(Ediff(Deltas_inds, xd_ind,ind0_z)).^2 - ...
                Vlatt_2D(Deltas_inds, xd_ind);
    for i = 1:length(potential_at_deltas)-1
        Deltas(i) = potential_at_deltas(i+1) - potential_at_deltas(i);
    end

    Deltas
end