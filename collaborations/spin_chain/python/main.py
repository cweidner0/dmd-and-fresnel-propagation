# -*- coding: utf-8 -*-
"""
Python port of dmd-and-fresnel-propagation/collaborations/spinchain

This script generates the optical potential from a DMD pattern assuming a set 
of experimental parameters (projection wavelength, NA, DMD pixel size, etc).

The generated pattern can also be propagated along the projection axis, but
note that it can be somewhat tricky to properly calibrate this propagation.
More information on this can be found in the relevant section of code.

One can also define an optical lattice with a given wavelength and depth and
calculate the \Delta values that define the biases between the different 
lattice sites. 

There is also an option to set the lattice phase.

Note that \Delta will not depend on the value you set for the
depth. Adjustments to the depth that arise due to the DMD potential have not
been coded into this script, and in the case that \Delta << depth, this is a
reasonable thing to neglect.

This script is somewhat less capable than the MATLAB script but it contains all
of the relevant aspects.

NOTE THAT Z-PROPAGATION IS NOT FUNCTIONAL HERE THIS CODE BUT THE SKELETON OF 
WHAT IS NEEDED IS PRESENT

Author: Carrie Weidner @cweidner0
University of Bristol
Date: 29.02.2024

% SPDX-FileCopyrightText: Copyright (C) 2024 Carrie A Weidner <c.weidner@bristol.ac.uk>, University of Bristol
% SPDX-License-Identifier: AGPL-3.0-or-later
"""

### import relevant aspects ###

import numpy as np
from matplotlib import pyplot as plt
import scipy as sp
import spinchain_funcs as sf
from PIL import Image

### options ###

do_load_bmp = False # load bitmap for DMD pattern
filename = 'test.bmp'
do_calibration = False # calibrate the propagation
scf = 2.03 # adjust scf until the calibration factor is 0.6895 (or so)
do_plot = False # plot stuff

field_scaling= 0.1 # scales the input field (in units of the lattice recoil)

lattice_depth = 1 # lattice depth (in recoils)
lattice_phase = 0 # lattice phase (zero sets antinode at x = 0)
n_sites = 9 # number of lattice sites to consider in the chain
lambda_latt_real = 1064e-9 # lattice wavelength in real units
lambda_proj_real = 940e-9 # projection wavelength
## the below use Aarhus values ##
NA_proj = 0.7 # projection numerical aperture
a = 2.54e-2/2 # objective radius (m)
f = 12.95e-3 # working focal distance of objective (m)

### constants ###
# constants are given capital letters
# even if that's not particularly pythonic

ER = 1 # lattice recoil
HBAR = 1 # reduced Planck's constant (1.055e-34 Js)
M = 1 # mass of atom (Rb-87 m = 1.44e-25 kg)
K_LATT = np.sqrt(2*M*ER/HBAR*HBAR) # lattice wavenumber, sqrt(2) in our scaled units
LAMBDA_LATT_SC = 2*np.pi/K_LATT # scaled lattice wavelength
X_SC = lambda_latt_real/LAMBDA_LATT_SC # spatial scaling in our scaled units
LAMBDA_PROJ_SC = lambda_proj_real/X_SC # scaled projection wavelength
K_PROJ = 2*np.pi/LAMBDA_PROJ_SC
A_SC = a/X_SC
F_SC = f/X_SC

# the below code determines if the potential is attractive or repulsive
# set for Rb-87 D2 line
# this code works if we are far detuned from D1 and D2 lines (D1 line at 795 nm)
# otherwise it is not reasonable
lambda_atom = 780e-9; 

if lambda_proj_real > lambda_atom:
    trap_sc = -1 # attractive potential (red-detuned)
else:
    trap_sc = 1 # repulsive potential (blue-detuned)

### spatial grid definition ###
# dz is usually one vertical lattice spacing for historical reasons
# as usually this sets a reasonable uncertainty (on the order of vertical 
# lattice planes)

dx = 0.1
dy = dx
dz = LAMBDA_LATT_SC/2

xlim = 25
ylim = 25
zlim = 2*dz

x = np.linspace(-xlim, xlim, 512)
y = np.linspace(-ylim, ylim, 512)
z = np.arange(-zlim, zlim+dz, dz)
dx = x[1] - x[0]
dy = y[1] - y[0]

len_x = len(x)
len_z = len(z)

# find the index at zero
cent= (0,0)
x0_ind = np.argmin(np.abs(x-cent[0]))
y0_ind = np.argmin(np.abs(y-cent[1]))
z0_ind = np.argmin(np.abs(z))

### figure out the indices where we expect our lattice minima to be ###
sites = np.arange(-np.floor((n_sites-1)/2), np.floor((n_sites+2)/2), 1)
lattice_minima = (sites*np.pi + lattice_phase)/K_LATT;
lattice_minima_inds = np.argmin(np.abs(x[:,np.newaxis]-lattice_minima),0)

### make PSF ###

E = sf.make_bessel(x, y, cent, NA_proj, A_SC, F_SC, F_SC, LAMBDA_PROJ_SC)

### Note that if we were to include aberrations, we'd do it here ###
# see MATLAB code for more details

### make DMD image ###
# currently very simple, binary, no dithering or anything
# can add a simple Floyd-Steinberg algorithm if we want to translate
# greyscale into binary

# DMD size
# currently at Aarhus values but these are super common for commercial DMDs

px_x = 1920
px_y = 1080

# find DMD center
ind_x0_DMD = int(np.ceil(px_x/2))-1
ind_y0_DMD = int(np.ceil(px_y/2))-1

# DMD pattern
if do_load_bmp:
    img = Image.open(filename)
    img.load()
    DMD = np.asarray( img, dtype = bool)
    DMD = DMD.astype(int) # gets around boolean weirdness
else:
    DMD = np.zeros((px_x, px_y))
    DMD[ind_x0_DMD, ind_y0_DMD] = 1

# convolve DMD pattern with E
E0 = sp.signal.fftconvolve(E, DMD,  mode='same')
E0 = np.sqrt(field_scaling)*E0/np.max(abs(E0))

### make lattice potential ###
# just applies the lattice phase along x (the spin chain)
Vlatt = -lattice_depth*(np.power(np.cos(K_LATT*x - lattice_phase), 2) + 
    np.power(np.cos(K_LATT*y[:, np.newaxis]), 2) - 1)

# Propagate field
Ez = sf.prop_TF(E0, len_x, LAMBDA_PROJ_SC, z, scf)
if do_calibration:
    cal_out = abs(Ez[0, x0_ind, y0_ind])**2/field_scaling
    print('The calibration factor is', str(cal_out))
    
# Get total potential (with lattice)

Utot = trap_sc*abs(Ez)**2 + Vlatt;

### find our Deltas ###
Deltas = np.zeros((n_sites-1))
for s in range(n_sites-1):
    Deltas[s] = Utot[z0_ind, lattice_minima_inds[s+1], y0_ind]-Utot[z0_ind, lattice_minima_inds[s], y0_ind]
    
print(Deltas)

if do_plot:
    ### plotting right now just for testing
    fig, axs = plt.subplots(figsize=(8,8), ncols = 2, nrows = 2)
    ax1 = axs.flat[0]
    ax2 = axs.flat[1]
    ax3 = axs.flat[2]
    ax4 = axs.flat[3]
    pos1 = ax1.pcolor(np.abs(E)**2)
    fig.colorbar(pos1, ax=ax1, location='right', anchor=(0, 0.3), shrink=0.7)
    ax1.set_title('PSF')
    pos2 = ax2.pcolor(DMD)
    fig.colorbar(pos2, ax=ax2, location='right', anchor=(0, 0.3), shrink=0.7)
    ax2.set_title('DMD image')
    pos3 = ax3.pcolor(np.abs(E0)**2)
    fig.colorbar(pos3, ax=ax3, location='right', anchor=(0, 0.3), shrink=0.7)
    ax3.set_title('DMD projection at focus')
    pos4 = ax4.pcolor(np.angle(E0))
    fig.colorbar(pos4, ax=ax4, location='right', anchor=(0, 0.3), shrink=0.7)
    ax4.set_title('image phase')
    
    figa, axa = plt.subplots(figsize=(3, 3), ncols = 1)
    posa = axa.pcolor(np.abs(Ez[0,:,:])**2, vmin = 0, vmax = field_scaling)
    figa.colorbar(posa, ax=axa, location='right', anchor=(0, 0.3), shrink=0.7)
    axa.set_title('z = -2dz')
    
    # plot cuts in x,y
    figb, axb = plt.subplots()
    axb.plot(y*X_SC*1e-6, Utot[z0_ind, x0_ind, :], 'r')
    axb.plot(x*X_SC*1e-6, Utot[z0_ind, :, y0_ind], 'b')
