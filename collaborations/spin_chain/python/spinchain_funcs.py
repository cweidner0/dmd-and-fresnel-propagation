# -*- coding: utf-8 -*-
"""
Python port of dmd-and-fresnel-propagation/collaborations/spinchain

This script includes the separate functions that are used in the generation
of the optical potential from a DMD pattern assuming a set of experimental 
parameters (projection wavelength, NA, DMD pixel size, etc) contained within 
main.py

Author: Carrie Weidner @cweidner0
University of Bristol
Date: 30.1.2024

% SPDX-FileCopyrightText: Copyright (C) 2024 Carrie A Weidner <c.weidner@bristol.ac.uk>, University of Bristol
% SPDX-License-Identifier: AGPL-3.0-or-later
"""

import numpy as np
import scipy as sp
from matplotlib import pyplot as plt


def make_bessel(x, y, cent, NA, a, zp, f, lb):
    
    '''
    Defines Bessel beam (DMD PSF) on a 2D plane, includes all phase factors
    
    This must be scaled by the bare electric field after generation.
    
    see M. Gu, Advanced Optical Imaging Theory, Springer Berlin, 2000
    for a reference on why this is reasonable for the PSF of the DMD
    through the projection system
    
    inputs:
        x: array of x-values
        y: array of y-values
        cent: 2-element array with center of Bessel beam
        NA: numerical aperture
        a: radius of objective
        zp: distance from the objective
        f: focal length of objective
        lb: projection wavelength
        
    outputs:
        E: electric field on a plane corresponding to DMD PSF and projection 
        system defined by the objective parameters as inputs
    '''
    
    k = 2*np.pi/lb # redundant with K_PROJ
    N = np.pi*(a**2)/(lb*zp)
    pref = 2*np.pi*NA/lb
    rho = pref*np.sqrt(np.square(x-cent[0]) + np.square(y[:, np.newaxis]-cent[1])) # define radial grid
    
    E = 1j*np.exp(-1j*k*f)*np.multiply(np.exp(1j*np.multiply(rho,rho)/(4*N)),
                                       np.divide(2*sp.special.jv(1, rho), rho))
    
    return E

def prop_TF(E0, L, lb, z, sc = 1):
    '''
    Fresnel propagation using the Transfer function
    Based on Computational Fourier Optics by Voelz 

    Python port of MATLAB file exchange code from:
    Manuel Ferrer (2024). Fresnel Propagation using the Transfer function 
    (https://www.mathworks.com/matlabcentral/fileexchange/72389-fresnel-propagation-using-the-transfer-function), 
    MATLAB Central File Exchange. Retrieved February 3, 2024. 
    
    inputs:
        E0: input complex field at source plane
        L: sidelength of the simulation window of the source plane
        lb: wavelength of light
        z: vector of propagation distances (right now jsut a scalar)
        sc: optional scaling factor for calibration
        
    outputs:
        EF: complex amplitude of beam at output plane
        
        
    '''
    
    M = np.shape(E0) # input array size
    dx = L/M[0]
    
    z = z*sc
    
    dim = len(z)
    
    EF = np.zeros((dim, M[0], M[1]),dtype=complex)
    
    fx = np.arange(-1/(2*dx), 1/(2*dx), 1/L) # frequency array
    
    #transfer function
    # H = np.exp(-1j*np.pi*lb*np.multiply(z[:, np.newaxis, np.newaxis],(np.power(fx,2) + np.power(fx[:, np.newaxis], 2))))
    i = 0
    for zt in z: # yes I know there are smarter ways to do this, that's later Carrie's problem
        H = np.exp(-1j*np.pi*lb*zt*(np.power(fx,2) + np.power(fx[:, np.newaxis], 2)))
    
        # Fourier transform input field
        E1 = sp.fft.fft2(sp.fft.fftshift(E0))
        #E1 = sp.fft.fft2(E0)
        # perform convolution (multiplication in Fourier space)
        E2 = np.multiply(H, E1)
        # ifft to get final field
        E3 = sp.fft.ifft2(E2)
        #EF = E3
        EF[i,:,:] = sp.fft.ifftshift(E3)
        i+=1
    
    return EF
    
    