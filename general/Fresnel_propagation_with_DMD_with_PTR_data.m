%Script to generate optical potential from DMD
%OG DMD script, current modification to play with atoms
%(includes PTR calculations, etc)
%Carrie Weidner
%Aarhus University
%13 April 2019

close all; clc

%% Options

load_DMD_image = 0; %loads DMD image from bitmap file
plot_DMD = 1; %plots image loaded onto DMD
plot_init = 0;
plot_abs = 1; %plots magnitude of image
plot_noise = 0;
plot_phase = 0; %plots phase of image
plot_xcut = 0;
plot_ycut = 0;
plot_zcut = 0;
plot_lattice = 1; %plot background lattice with images
plot_carpet = 0; %talbot carpet plot
plot_x_intensity_shift = 1;
plot_z_intensity_shift = 0;
plot_PTR = 1;
plot_PTR_pk_fnd = 0;

% ideally, this should work just fine if we define a superpixel and do the
% convolution, but this does not work right now
% so keep this at zero
do_superpixel = 1;
% use the FFT method to do the Fresnel propagation if this is set to 1
% otherwise use the convolutional method
% FFT method is faster and thus recommended
do_FFT = 1;
% use a window (see relevant code below for more details)
do_window = 0;
% add phase front (model aberrations)
do_phase_front = 0;

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 780e-9; % atoms
% lambda_proj = 787e-9; % spin-addressing light
% lambda_proj = 940e-9; % potential-changing light
% lambda_proj = 1064e-9; % lattice wavelength
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
% a = 2;
% xlim = a*LAMBDA_LATT_SC;
% ylim = a*LAMBDA_LATT_SC;
% grid resolution (this sets the spacing of the dots because of the
% intricacies of the convolution operation, I am looking into how to fix
% this)
% when dealing with DMD images:
% the spacing as a function of resolution is (roughly) given by
% spacing(m) = X_SC*dx/1e-2;
% This is based on numerical scaling
% and the experimental magnification of 1/100
exp_spacing = 7.56e-6;
% zT is for imaging Talbot planes
% NB: There is a bug somewhere, and the Talbot planes show up a factor of 2
% sooner than they should (that is, the doubled plane shows up at zT/2)
% see https://en.wikipedia.org/wiki/Talbot_effect
% I suspect the issue is somewhere in the scaling between experimental
% spacing (exp_spacing) and spacing in the program
% zT = 2*exp_spacing^2/lambda_proj;
zT = (lambda_proj/(1-sqrt(1-(lambda_proj/exp_spacing)^2)));
zlim = LAMBDA_LATT_SC; %1 in scaled units is about 239 nm
% zlim = zT/X_SC;
% dx = exp_spacing*1e-2/X_SC;
dx = 105e-9/X_SC; % each hires pixel is 105 nm
dy = 105e-9/X_SC;
% the total span of each image is about 20 �m, so 10�m in each direction
% xlim = 238*dx/2;
% ylim = xlim;
px = 119;
% px = 21;
xlim = floor(px/2)*dx;
ylim = floor(px/2)*dy;
dz = LAMBDA_LATT_SC/2; %z-resolution is one plane

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = 0:dz:zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim).^2 + (Y2D/ylim).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% define window function for Fresnel kernel H
% this is important so that we do not have obnoxious repeating patterns in
% our Fresnel propagation/DMD convolution code
% basically, we need to force things to zero at the edge of the window if
% we have small patterns (like a single light pixel in the center of the DMD)
% one has to be very careful with this

if do_window == 1
    %WINDOW (DO THIS IF THINGS START REPEATING)
    divH = 6;
    window = zeros(len_x, len_y);
    winx = ceil(len_x/divH);
    winy = ceil(len_y/divH);
    window(x0_ind-ceil(winx):x0_ind+ceil(winx), y0_ind-ceil(winy):y0_ind+ceil(winy)) = ...
        coswin(length(x0_ind-ceil(winx):x0_ind+ceil(winx)), 11)*...
        coswin(length(y0_ind-ceil(winy):y0_ind+ceil(winy)), 11)';
else
    %NO WINDOW
    window = ones(size(X2D));
end

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    %right now this is just a tilted phase front, but I plan to do some
    %sort of generalization
    %nn_x and nn_y are related to how much the phase front is tilted
    %(total tilt in radians is 2*pi*nn_i in the i-th direction)
    nn_x = 0;
    nn_y = 0;
    np = 0; %parabolic tilt
    nc_x = 0; %x-coma
    nc_y = 0; %y-coma
    E = exp(nn_x*2*pi*1i*X2D/xlim).*exp(nn_y*2*pi*1i*Y2D/ylim).*exp(np*2*pi*1i*(2*R2D_sq-1)).*exp(nc_x*2*pi*1i*(3*R2D_sq-2).*X2D/xlim).*exp(nc_y*2*pi*1i*(3*R2D_sq-2).*Y2D/ylim);
end
% w0 = 1e-6/X_SC; %waist of beam
% zR = pi*w0^2/lambda;

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.43 for
%787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
%is why we see single atoms)
NA = 0.69; %Specified NA of hires objective (unitless)
% NA = 0.51; %Measured effective NA of the hires objective (787 system)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

%% DMD parameters

spx = 7; %superpixel size

%regular pixels
px_x = 1920;
px_y = 1080;

%superpixels
spx_x = floor(px_x/spx); %we take the floor because we need integers
spx_y = floor(px_y/spx); %half a superpixel will do us little good, so we take the floor

DMD = zeros(px_x, px_y);
sDMD = zeros(spx_x, spx_y);

%% input grid

%the code below basically finds the center of the DMD
ind_x0_DMD = ceil(px_x/2)+1;
ind_y0_DMD = ceil(px_y/2)+1;
ind_x0_sDMD = ceil(spx_x/2)+1;
ind_y0_sDMD = ceil(spx_y/2)+1;

% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!

if load_DMD_image == 1 %load bitmap
    % currently this reads images from the "DMD_images" subfolder
    filename = 'test_1.bmp';
    foldername = 'DMD_images';
    if ispc
        % Windows
        DMD = imread(['.\', foldername, '\', filename]);
    else
        % Unix-based system
        DMD = imread(['./',foldername, '/', filename]);
    end
    %     DMD = imread('single_R7_dimple.bmp');
else %write code to generate the DMD image you want
    % single spot
    DMD(ind_x0_DMD, ind_y0_DMD) = 1;
end

% REMEMBER THAT THIS DOES NOT WORK
% hopefully I can fix it at some point
if do_superpixel == 1
    %make superpixels
    for i = 1:spx_x
        ind1i = spx*i-(spx-1);
        ind2i = spx*i;
        for j = 1:spx_y
            ind1j = spx*j-(spx-1);
            ind2j = spx*j;
            sDMD(i,j) = sum(sum(DMD(ind1i:ind2i, ind1j:ind2j)))/(spx^2);
        end
    end
end

%% Do convolutions

if do_superpixel == 1
    E0 = conv2(PSF, sDMD, 'same');
else
    E0 = conv2(PSF, DMD/(spx^2), 'full');
end

% % load('imgT.mat');
% load imgdiff.mat
% imgT = imgtosave(:,:,9);
% 
% %normalize
% % this assumes that the image is square
% floorpx1 = floor(len_x/2);
% floorpx2 = floor(max(size(imgT))/2);
% E0((floorpx1 - floorpx2+1):(floorpx1 + floorpx2+1),(floorpx1 - floorpx2+1):(floorpx1 + floorpx2+1)) = imgT/max(max(imgT));

%normalize
E0 = E0/max(max(abs(E0)));

FE0 = fftshift(fft2(E0));

Ediff = E0.*ones(size(X2D)); %empty vector to store E field at different points on grid

rsq = X.^2 + Y.^2;
% Fresnel kernel from Wiki
H = window.*(-1i*k/(2*pi*Z)).*exp(1i*(k./(2*Z)).*rsq + 1i*k*Z);
% The below kernel has a factor of 2 hacked into it, because for some
% strange reason the beam was diffracting too quickly. This kernel produces
% results that agree with both Talbot diffraction theory and what we see in
% the experiment.
% H = window.*(-1i*k/(2*pi*(Z/2))).*exp(1i*(k./(2*(Z/2))).*rsq + 1i*k*(Z/2));

%% Lattice parameters

if plot_lattice == 1
    V0 = 1; %amplitude of lattice (in recoils)
    Vlatt_2D = V0*(cos(K_LATT*X2D).^2 + cos(K_LATT*Y2D).^2 - 1); %only do 2D lattice
else
    Vlatt_2D = zeros(size(X2D));
end

%% Do Fresnel propagation

% Wikipedia to the rescue
% see https://en.wikipedia.org/wiki/Fresnel_diffraction#Alternative_forms

tic %start timer
for i = 1:len_z
    if z(i) ~= 0
        if do_FFT == 1 % do FFT method (fastest)
%             FH = fftshift(fft2(H(:,:,i)))*(dx*dy);
%             Ediff(:,:,i) = ifftshift(ifft2(ifftshift(FE0.*FH)));
%             [Ediff(:,:,i)] = propTF(E0, xlim, lambda, z(i)/4.15);
            [Ediff(:,:,i)] = propTF(E0, len_x, lambda, 5*z(i));
        else % use convolutional method
            Ediff(:,:,i) = dx*dy*conv2(window.*E0,H(:,:,i),'same');
        end
    end
end
% add noise

aa = 0;
bb = 0;

Idiff = abs(Ediff).^2;

Idiff = Idiff + bb*sqrt(Idiff).*randn(size(Idiff)) + abs(sqrt(aa)*randn(size(Idiff)));

Idiff = Idiff.*(1 + bb*sqrt(Idiff).*randn(size(Idiff)));
% for kk = 1:len_z
%     for i = 1:len_x
%         for j = 1:len_y
%             Ediff(i,j,kk) = Ediff(i,j,kk) + bb*abs(Ediff(i,j,kk))*randn(1,1);
%         end
%     end
% end
Idiff = Idiff + abs(sqrt(aa)*randn(size(Idiff)));
toc %stop timer

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if tot = len(z)
nx = 2; %number of plots along the x-direction
ny = 3; %number of plots along y-direction
tot = nx*ny; %total number of plots
if tot > len_z
    % basically, you can't plot any more points than you have calculated
    % this will leave blank plots but it's better than stupid errors
    tot = len_z;
end

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% colors!
colors = {'r', 'b', 'k', 'g', 'm', 'r--', 'b--', 'k--', 'g--', 'm--', 'r:', ...
    'b:', 'k:', 'g:', 'm:'};

% plotting limits
[~, xd_ind] = min(abs(x + xd));
[~, yd_ind] = min(abs(y + yd));
dim_x = xlim;
dim_y = ylim;
% dim_x = 3;
% dim_y = 3;
plotlim_x = xlim;
plotlim_y = ylim;
[~, xlim_neg_ind] = min(abs(x + dim_x));
[~, xlim_pos_ind] = min(abs(x - dim_x));
[~, ylim_neg_ind] = min(abs(y + dim_y));
[~, ylim_pos_ind] = min(abs(y - dim_y));
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);

% aspect ratios
% DMD plots
if do_superpixel == 1
    DMD_aspect = spx_y/spx_x;
else
    DMD_aspect = px_y/px_x;
end
% other 2D plots
image_aspect = plotlim_x/plotlim_y;


% plot DMD image and the projected image at z = 0
if plot_DMD == 1
    figure('Position', [100 100 800 400])
    subplot(1,2,1)
    if do_superpixel == 1
        imagesc(sDMD)
    else
        imagesc(DMD)
    end
    pbaspect([1 DMD_aspect 1])
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (px)')
    ylabel('y (px)')
    title('DMD Pattern')
    subplot(1,2,2)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Field at focus')
    pbaspect([1 image_aspect 1])
end

% plot the initial field and it's FFT
if plot_init == 1
    figure('Position', [100 100 600 500])
    subplot(1,2,1)
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6, y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
        abs(E0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)).^2)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('y (�m)')
    title('Initial field')
    pbaspect([1 image_aspect 1])
    subplot(1,2,2)
    imagesc(fx(xlim_neg_ind:xlim_pos_ind), fy(ylim_neg_ind:ylim_pos_ind), ...
        abs(FE0(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind)))
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    title('Initial field FFT')
    pbaspect([1 image_aspect 1])
end

% plot the magnitude of the field at all Fresnel points
if plot_abs == 1
    figure('Position', [0,0,400*ny,300*nx])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            Idiff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i))
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
        max(max(Idiff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i)))
        caxis([0 1])
    end
end
 
%plot noise
if plot_noise == 1 
    figure('Position', [0,0,400*ny,300*nx])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            noise(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i))
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
        caxis([0 1])
    end
end

% plot the phase of the field at all Fresnel points
if plot_phase == 1
    figure('Position', [0,0,600,500])
    for i = 1:tot
        subplot(nx,ny,i)
        imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6,...
            angle(Ediff(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i)))
        colorbar
        caxis([-pi pi])
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['FFT, z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
    end
end

% plot y-cut at position yd
if plot_ycut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('Y-cut')
    for i = 1:tot
        plot(y(ylim_neg_ind:ylim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(yd_ind,ylim_neg_ind:ylim_pos_ind,i)).^2 -...
            Vlatt_2D(yd_ind, ylim_neg_ind:ylim_pos_ind), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6), ' �m'];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('y (�m)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_x*X_SC*1e6, plotlim_x*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% plot x-cut at position xd
if plot_xcut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('X-cut')
    for i = 1:tot
        plot(x(xlim_neg_ind:xlim_pos_ind)'*X_SC*1e6,...
            -abs(Ediff(xlim_neg_ind:xlim_pos_ind,xd_ind,i)).^2 - ...
            Vlatt_2D(xlim_neg_ind:xlim_pos_ind, xd_ind), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6)];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, -max(max(abs(E0).^2 + Vlatt_2D)), 0])
    grid on
end

% plot z-cut at position xd, yd
if plot_zcut == 1
    figure
    hold on
    leg = strings(1, tot);
    title('Z-cut')
    for i = 1:tot
        plot(z*X_SC*1e6,...
            reshape(abs(Ediff(yd_ind,xd_ind,:)).^2 - ...
            Vlatt_2D(yd_ind, xd_ind), 1, len_z), colors{i})
        leg{i} = ['z = ', num2str(z(i)*X_SC*1e6)];
    end
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('Amplitude (arb)')
    legend(leg,'Location','best','Fontsize',10)
    %     axis([-plotlim_y*X_SC*1e6, plotlim_y*X_SC*1e6, 0, max(max(abs(E0).^2 + Vlatt_2D))])
    grid on
end

% make a Talbot-carpet-like plot
if plot_carpet == 1
    figure('Position', [0,0,600,500])
    imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,z*X_SC*1e6,...
        reshape(abs(Ediff(xlim_neg_ind:xlim_pos_ind,xd_ind,:)).^2, ...
        length(xlim_neg_ind:xlim_pos_ind), len_z)')
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (�m)')
    ylabel('z (�m)')
    pbaspect([1 image_aspect 1])
end

% how much does the peak intensity shift (relative to the max at the focus)
% as you move through different planes in the vertical lattice?

if plot_z_intensity_shift == 1 || plot_PTR == 1
    zp = linspace(min(z), max(z), 100);
    zeta = k*NA^2*zp;
    fz = (sin(zeta/4)./(zeta/4)).^2;
    p = polyfit(zp*X_SC*1e6/0.532, fz, 2);
    yp = polyval(p,zp*X_SC*1e6/0.532);
end

if plot_z_intensity_shift == 1
    data_z = [40, 55, 60, 50]/60;
    data_z_2 = [60, 70, 80, 75]/80;
    err_z = 5*ones(size(data_z))/60;
    err_z_2 = 5*ones(size(data_z))/80;
    max_z = zeros(1, len_z);
    for i = 1:len_z
%         max_z(i) = abs(Ediff(ind0_x, ind0_y, i)).^2;
        max_z(i) = max(max(abs(Ediff(:,:, i)).^2));
    end
    figure
    hold on
    plot(zp*X_SC*1e6/0.532, fz, 'r', 'LineWidth', 1.5)
    plot(zp*X_SC*1e6/0.532, yp, 'c--', 'LineWidth', 1.5)
    plot(z*X_SC*1e6/0.532, max_z, 'k.', 'MarkerSize', 12)
    errorbar(z(1:4)*X_SC*1e6/0.532, data_z, err_z, 'b*', 'MarkerSize', 12)
    errorbar(z(1:4)*X_SC*1e6/0.532, data_z_2, err_z_2, 'mo', 'MarkerSize', 12)
    plot(z(1:4)*X_SC*1e6/0.532, [0.53, 0.84, 1, 0.88], 'ko', 'MarkerSize', 12)
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('Lattice plane')
    ylabel('Peak intensity in adjacent planes (fraction of focus)')
    grid on
    axis([min(z)*X_SC*1e6/0.532 max(z)*X_SC*1e6/0.532 0.5 1.1])
    legend('Sinc', 'Quad fit', 'Fresnel amp', 'Data 1', 'Data 2', 'Data 3')
    % data estimated from
    % https://labbook.au.dk/display/HLSG/Single+atoms+3D+tomography?preview=/96211059/96211068/fluorAnalysis_PiScan_poster_atom_traces_180810.png
end
if plot_x_intensity_shift == 1
    n_planes_x = 2;
    max_x = zeros(1, n_planes_x);
    x_zero_ind = find(x == min(abs(x)));
    planes = 0:n_planes_x-1;
    x_cut_inds = x_zero_ind + planes*2;
    x_field = -abs(Ediff(x_cut_inds,xd_ind,1)).^2/(-abs(Ediff(x_zero_ind,xd_ind,1)).^2);
    
    figure
    hold on
    plot(x(x_zero_ind:max(x_cut_inds))'*X_SC*1e6,...
        -abs(Ediff(x_zero_ind:max(x_cut_inds),x_zero_ind,i)).^2./...
        -abs(Ediff(x_zero_ind:max(x_zero_ind),x_zero_ind,i)).^2, ...
        'r', 'LineWidth', 1.5)
    plot(x(x_cut_inds)'*X_SC*1e6, x_field, 'k.', 'MarkerSize', 12)
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    xlabel('Distance (�m)')
    ylabel('Peak intensity in adjacent planes (fraction of focus)')
    grid on
    legend('Transverse beam profile', 'Location of lattice planes')
    axis([0 x(max(x_cut_inds))*X_SC*1e6 0 1])
    
    % This data outputs the position (relative to position of the peak beam
    % intensity) in column 1 and the field amplitude (relative to the peak
    % intensity) in column 2
    %     raw_data = [x(x_cut_inds)'*X_SC*1e6, x_field]
    
end

if plot_PTR == 1
    a = 1;
    b = 3;
    N_3by3 = zeros(len_z, 1);
    N_7by7 = zeros(len_z, 1);
    for i = 1:len_z
        [ind_max_row, ind_max_col] = find(abs(Ediff(:,:, i)).^2 == max(max(abs(Ediff(:,:, i)).^2)));
        N_3by3(i) = sum(sum(abs(Ediff(ind0_x-a:ind0_x+a, ind0_y-a:ind0_y+a, i)).^2));
        N_7by7(i) = sum(sum(abs(Ediff(ind0_x-b:ind0_x+b, ind0_y-b:ind0_y+b, i)).^2));
    end
    PTR = N_3by3./N_7by7;
    x1 = linspace(-zlim*X_SC*1e6/0.532, zlim*X_SC*1e6/0.532);
    p2 = polyfit(z*X_SC*1e6/0.532, PTR', 2)
    y1 = polyval(p2,x1);
    figure
    hold on
    plot(x1,y1, 'r','LineWidth', 1.5)
    plot(z*X_SC*1e6/0.532, PTR', 'k.', 'MarkerSize', 12)
    xlabel('Distance (lattice planes)')
    ylabel('PTR (3x3/7x7)')
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    legend('Quad fit', 'PTR')
    
%     p./p2
end

if plot_PTR_pk_fnd == 1
    N_3by3 = zeros(len_z, 3, 3);
    N_7by7 = zeros(len_z, 3, 3);
    a = 1;
    b = 3;
    for i = 1:len_z
        for j = 0:2
            for k = 0:2
                N_3by3(i, j+1,k+1) = sum(sum(abs(Ediff(ind0_x-a+j:ind0_x+a+j, ind0_y-a+k:ind0_y+a+k, i)).^2));
                N_7by7(i, j+1,k+1) = sum(sum(abs(Ediff(ind0_x-b+j:ind0_x+b+j, ind0_y-b+k:ind0_y+b+k, i)).^2));
                %         N_3by3(i) = mean(mean(abs(Ediff(ind0_x-1:ind0_x+1, ind0_y-1:ind0_y+1, i)).^2));
                %         N_7by7(i) = mean(mean(abs(Ediff(ind0_x-1:ind0_x+3, ind0_y-1:ind0_y+3, i)).^2));
            end
        end
    end
    PTR = N_3by3./N_7by7;
    x1 = linspace(-zlim*X_SC*1e6/0.532, zlim*X_SC*1e6/0.532);
    y1 = zeros(length(x1), 3, 3);
    for i = 1:3
        for j = 1:3
            p = polyfit(z*X_SC*1e6/0.532, reshape(PTR(:,i,j), 1, len_z), 2)
            y1(:,i,j) = polyval(p,x1);
        end
    end
        
    leg = cell(1,18);
    figure
    hold on
    grid on
    c = cell(1, 9);
    
    for i = 1:3
        for j = 1:3
            c = rand(1,3);
            plot(x1,reshape(y1(:,i,j), 1, length(x1)), 'LineWidth', 1.5)
            leg{(i-1)*3 + j} =  ['Fit: pk finder err x: ', num2str(i-1), 'Pk finder err y: ', num2str(i-1)];
            plot(z*X_SC*1e6/0.532, reshape(PTR(:,i,j), 1, len_z), '.', 'MarkerSize', 12)
            leg{(i-1)*3 + j + 9} =  ['Data: Pk finder err x: ', num2str(i-1), 'Pk finder err y: ', num2str(i-1)];
        end
    end
    xlabel('Distance (lattice planes)')
    ylabel('PTR (3x3/7x7)')
    set(gca, 'FontSize', 10, 'FontWeight', 'bold')
    legend(leg)
    
    meanPTR = mean(mean(PTR ,2), 3);
    stdPTR = std(std(PTR, [], 2), [], 3);
    p = polyfit(z*X_SC*1e6/0.532, meanPTR', 2);
    ymean = polyval(p,x1);
    figure
    hold on
    plot(x1, ymean, 'r', 'LineWidth', 1.5)
    errorbar(z*X_SC*1e6/0.532, meanPTR, stdPTR, 'k.', 'MarkerSize', 12)
    xlabel('Distance (lattice planes)')
    ylabel('PTR (3x3/7x7)')
end
