function w = coswin(n,window)

% COSWIN Symmetric minimum sidelobe cosine window
%    COSWIN(N,WIN) returns a symmetric N point minimum sidelobe
%    cosine window by evaluating the first half and then flipping the
%    same samples over the other half.
%
%    WIN specifies the order of the window:
%       0 : Rectangular (Dirichlet), 13.3 dB
%       1 : Hanning (default), 31.5 dB
%       2 : 2-Term (Hamming), 43.187 dB
%       3 : 3-Term (Blackman-Nutall), 71.482 dB
%       4 : 4-Term (Blackman-Nutall), 98.173 dB
%       5 : 5-Term, 125.427 dB
%       6 : 6-Term, 153.566 dB
%       7 : 7-Term, 180.468 dB
%       8 : 8-Term, 207.512 dB
%       9 : 9-Term, 234.734 dB
%      10 : 10-Term, 262.871 dB
%      11 : 11-Term, 289.635 dB
%     102 : Exact Hamming, 41.7 dB
%     103 : Exact Blackman, 68.2 dB
%     104 : 4-Term Blackman-Harris, 92 dB
%
%    See also WINDOW.
%
%    Ref:
%    A Family of Cosine-Sum Windows for High-Resolution Measurements
%    Hans-Helge Albrecht
%    Physikalisch-Technische Bendesanstalt
%    Acoustics, Speech, and Signal Processing, 2001. Proceedings. (ICASSP '01).
%    2001 IEEE International Conference on   (Volume:5 )
%    pgs. 3081-3084
 
%  Joe Henning - 6 May 2013
 
if nargin < 2
   window = 1;
end

if (n == 1)
   w = 1;
   return
end
 
if ~rem(n,2)
   % Even length window
   half = n/2;
   w = calc_window(half,n,window);
   w = [w; w(end:-1:1)];
else
   % Odd length window
   half = (n+1)/2;
   w = calc_window(half,n,window);
   w = [w; w(end-1:-1:1)];
end
 
 
function w = calc_window(m,n,window)
% CALC_WINDOW Calculate the window samples
%    CALC_WINDOW calculates and returns the first M points of an
%    N point window determined by the window number
 
x = (0:m-1)'/(n-1);
 
switch window
   case 0
      % rectangular (Dirichlet), 13.3 dB, NBW 1 bin, first zero at +/-1 bin
      w = ones(size(x));
   case 1
      % Hanning, differentiable, 31.5 dB, NBW 1.5 bins, first zero at +/- 2 bins
      w = 0.5 - 0.5*cos(2*pi*x);
   case 2
      % 2 Term Cosine (Hamming), 43.187 dB, NBW 1.36766 bins, 5.37862 dB gain
      a0 = 5.383553946707251e-001;
      a1 = 4.616446053292749e-001;
      w = a0 - a1*cos(2*pi*x);
   case 3
      % 3 Term Cosine (Blackman-Nutall), 71.482 dB, NBW 1.70371 bins, 7.44490 dB gain
      a0 = 4.243800934609435e-001;
      a1 = 4.973406350967378e-001;
      a2 = 7.827927144231873e-002;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x);
   case 4
      % 4 Term Cosine (Blackman-Nutall), 98.173 dB, NBW 1.97611 bins, 8.78795 dB gain
      a0 = 3.635819267707608e-001;
      a1 = 4.891774371450171e-001;
      a2 = 1.365995139786921e-001;
      a3 = 1.064112210553003e-002;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x);
   case 5
      % 5 Term Cosine, 125.427 dB, NBW 2.21535 bins, 9.81016 dB gain
      a0 = 3.232153788877343e-001;
      a1 = 4.714921439576260e-001;
      a2 = 1.755341299601972e-001;
      a3 = 2.849699010614994e-002;
      a4 = 1.261357088292677e-003;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x);
   case 6
      % 6 Term Cosine, 153.566 dB, NBW 2.43390 bins, 10.64612 dB gain
      a0 = 2.935578950102797e-001;
      a1 = 4.519357723474506e-001;
      a2 = 2.014164714263962e-001;
      a3 = 4.792610922105837e-002;
      a4 = 5.026196426859393e-003;
      a5 = 1.375555679558877e-004;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x);
   case 7
      % 7 Term Cosine, 180.468 dB, NBW 2.63025 bins, 11.33355 dB gain
      a0 = 2.712203605850388e-001;
      a1 = 4.334446123274422e-001;
      a2 = 2.180041228929303e-001;
      a3 = 6.578534329560609e-002;
      a4 = 1.076186730534183e-002;
      a5 = 7.700127105808265e-004;
      a6 = 1.368088305992921e-005;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x) + a6*cos(12*pi*x);
   case 8
      % 8 Term Cosine, 207.512 dB, NBW 2.81292 bins, 11.92669 dB gain
      a0 = 2.533176817029088e-001;
      a1 = 4.163269305810218e-001;
      a2 = 2.288396213719708e-001;
      a3 = 8.157508425925879e-002;
      a4 = 1.773592450349622e-002;
      a5 = 2.096702749032688e-003;
      a6 = 1.067741302205525e-004;
      a7 = 1.280702090361482e-006;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x) + a6*cos(12*pi*x) - a7*cos(14*pi*x);
   case 9
      % 9 Term Cosine, 234.734 dB, NBW 2.98588 bins, 12.45267 dB gain
      a0 = 2.384331152777942e-001;
      a1 = 4.005545348643820e-001;
      a2 = 2.358242530472107e-001;
      a3 = 9.527918858383112e-002;
      a4 = 2.537395516617152e-002;
      a5 = 4.152432907505835e-003;
      a6 = 3.685604163298180e-004;
      a7 = 1.384355593917030e-005;
      a8 = 1.161808358932861e-007;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x) + a6*cos(12*pi*x) - a7*cos(14*pi*x) + a8*cos(16*pi*x);
   case 10
      % 10 Term Cosine, 262.871 dB, NBW 3.15168 bins, 12.92804 dB gain
      a0 = 2.257345387130214e-001;
      a1 = 3.860122949150963e-001;
      a2 = 2.401294214106057e-001;
      a3 = 1.070542338664613e-001;
      a4 = 3.325916184016952e-002;
      a5 = 6.873374952321475e-003;
      a6 = 8.751673238035159e-004;
      a7 = 6.008598932721187e-005;
      a8 = 1.710716472110202e-006;
      a9 = 1.027272130265191e-008;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x) + a6*cos(12*pi*x) - a7*cos(14*pi*x) + a8*cos(16*pi*x) - a9*cos(18*pi*x);
   case 11
      % 11 Term Cosine, 289.635 dB, NBW 3.30480 bins, 13.34506 dB gain
      a0 = 2.151527506679809e-001;
      a1 = 3.731348357785249e-001;
      a2 = 2.424243358446660e-001;
      a3 = 1.166907592689211e-001;
      a4 = 4.077422105878731e-002;
      a5 = 1.000904500852923e-002;
      a6 = 1.639806917362033e-003;
      a7 = 1.651660820997142e-004;
      a8 = 8.884663168541479e-006;
      a9 = 1.938617116029048e-007;
      a10 = 8.482485599330470e-010;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x) + a4*cos(8*pi*x) - a5*cos(10*pi*x) + a6*cos(12*pi*x) - a7*cos(14*pi*x) + a8*cos(16*pi*x) - a9*cos(18*pi*x) + a10*cos(20*pi*x);
   case 102
      % 2 Term Exact Hamming, 41.7 dB
      a0 = 25/46.;
      a1 = 21/46.;
      w = a0 - a1*cos(2*pi*x);
   case 103
      % 3 Term Exact Blackman, 68.2 dB
      a0 = 7938/18608.;
      a1 = 9240/18608.;
      a2 = 1430/18608.;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x);
   case 104
      % 4 Term Blackman-Harris window, 92 dB
      a0 = 3.58750287312166e-001;
      a1 = 4.88290107472600e-001;
      a2 = 1.41279712970519e-001;
      a3 = 1.16798922447150e-002;
      w = a0 - a1*cos(2*pi*x) + a2*cos(4*pi*x) - a3*cos(6*pi*x);
   otherwise
      error('Unknown WIN type');
end