% Takes an input image (img_in) and applies the Floyd-Steinberg
% dithering algorithm to the image to produce a dithered image with a pixel
% resolution described by the variable disc

% Compares with the MATLAB dither() command--we see that this code performs
% better in the light and dark regions!

% if disc = 1, then the image will be binary (as it should be for our DMDs)

% see here: https://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering
% and: https://se.mathworks.com/matlabcentral/fileexchange/33342-floyd-steinberg-dithering-algorithm

% assumes the values of the pixels in the input image img_in are mapped
% between 0 and 1

disc = 1;
img_in = zp{5}; %run square_Zernike first to get this

img_init = img_in;

[px_x, px_y] = size(img_in);

img_disc = zeros(size(img_in));

img_disc_init = round(disc*img_in);

for i = 1:px_x
    for j = 1:px_y
        img_disc(i,j) = round(disc*img_in(i,j));
        err = img_in(i,j) - img_disc(i,j);
        if i < px_x %not bottom row
            if j == 1 %top row
                img_in(i,j+1) = img_in(i, j+1) + err*(7/13);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/13);
                img_in(i+1,j+1) = img_in(i+1,j+1) + err*(1/13);
            elseif j > 1 && j < px_y
                img_in(i,j+1) = img_in(i, j+1) + err*(7/16);
                img_in(i+1,j-1) = img_in(i+1,j-1) + err*(3/16);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/16);
                img_in(i+1,j+1) = img_in(i+1,j+1) + err*(1/16);
            else
                img_in(i+1,j-1) = img_in(i+1,j-1) + err*(3/8);
                img_in(i+1,j) = img_in(i+1,j) + err*(5/8);
            end
        else %bottom row
            if j > px_y
                img_in(i, j+1) = img_in(i, j+1) + err;
            end
        end
    end
end

% for i = 1:px_x
%     for j = 1:px_y
%         img_disc(i,j) = round(disc*img_in(i,j));
%         err = img_in(i,j) - img_disc(i,j);
%         if j < px_y %top row
%             img_in(i,j+1) = img_in(i, j+1) + err*(7/16);
%         end
%         if j > 1 && i < px_x
%             img_in(i+1,j-1) = img_in(i+1,j-1) + err*(3/16);
%             img_in(i+1,j) = img_in(i+1,j) + err*(5/16);
%         end
%         if j < px_y && i < px_x
%             
%             img_in(i+1,j+1) = img_in(i+1,j+1) + err*(1/16);
%         end
%     end
% end

img_disc_final = round(disc*img_in);

figure
subplot(2,2,1)
imagesc(img_init)
colorbar
colormap gray
subplot(2,2,2)
imagesc(img_disc_init)
colorbar
colormap gray
subplot(2,2,3)
imagesc(img_disc_final)
colorbar
colormap gray
subplot(2,2,4)
imagesc(dither(img_in))
colorbar
colormap gray

figure
imagesc(img_disc_final-dither(img_in))
colorbar
colormap gray

%this version is better than the MATLAB version! yay!
