%Script to generate optical potential from DMD
%made to simulate a single atom's propagation
%Carrie Weidner
%Aarhus University
%13 April 2019

close all; clc

%% Options

plot_DMD = 0; %plots image loaded onto DMD
plot_init = 0;
plot_abs = 1; %plots magnitude of image
plot_phase = 0; %plots phase of image

% use a window (see relevant code below for more details)
do_window = 0;
% add phase front (model aberrations)
do_phase_front = 0;

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 780e-9; % atoms
% lambda_proj = 787e-9; % spin-addressing light
% lambda_proj = 940e-9; % potential-changing light
% lambda_proj = 1064e-9; % lattice wavelength
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
a = 2;
xlim = 2.1e-6/X_SC/2;
ylim = 2.1e-6/X_SC/2;
% grid resolution (this sets the spacing of the dots because of the
% intricacies of the convolution operation, I am looking into how to fix
% this)
% when dealing with DMD images:
% the spacing as a function of resolution is (roughly) given by
% spacing(m) = X_SC*dx/1e-2;
% This is based on numerical scaling
% and the experimental magnification of 1/100
exp_spacing = 7.56e-6;
% zT is for imaging Talbot planes
% NB: There is a bug somewhere, and the Talbot planes show up a factor of 2
% sooner than they should (that is, the doubled plane shows up at zT/2)
% see https://en.wikipedia.org/wiki/Talbot_effect
% I suspect the issue is somewhere in the scaling between experimental
% spacing (exp_spacing) and spacing in the program
% zT = 2*exp_spacing^2/lambda_proj;
zT = (lambda_proj/(1-sqrt(1-(lambda_proj/exp_spacing)^2)));
zlim = 0; %1 in scaled units is about 239 nm
% zlim = zT/X_SC;
% dx = exp_spacing*1e-2/X_SC;
dx = 105e-9/X_SC; % each hires pixel is 105 nm
dy = 105e-9/X_SC;
% the total span of each image is about 20 �m, so 10�m in each direction
% xlim = 238*dx/2;
% ylim = xlim;
dz = LAMBDA_LATT_SC/2; %z-resolution is one plane

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = 3*LAMBDA_LATT_SC/2:dz:LAMBDA_LATT_SC/2;
% z = 0:dz:zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim).^2 + (Y2D/ylim).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% define window function for Fresnel kernel H
% this is important so that we do not have obnoxious repeating patterns in
% our Fresnel propagation/DMD convolution code
% basically, we need to force things to zero at the edge of the window if
% we have small patterns (like a single light pixel in the center of the DMD)
% one has to be very careful with this
% 
% if do_window == 1
%     %WINDOW (DO THIS IF THINGS START REPEATING)
%     divH = 6;
%     window = zeros(len_x, len_y);
%     winx = ceil(len_x/divH);
%     winy = ceil(len_y/divH);
%     window(x0_ind-ceil(winx):x0_ind+ceil(winx), y0_ind-ceil(winy):y0_ind+ceil(winy)) = ...
%         coswin(length(x0_ind-ceil(winx):x0_ind+ceil(winx)), 11)*...
%         coswin(length(y0_ind-ceil(winy):y0_ind+ceil(winy)), 11)';
% else
%     %NO WINDOW
%     window = ones(size(X2D));
% end

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    %right now this is just a tilted phase front, but I plan to do some
    %sort of generalization
    %nn_x and nn_y are related to how much the phase front is tilted
    %(total tilt in radians is 2*pi*nn_i in the i-th direction)
    nn_x = 0;
    nn_y = 0;
    np = 0; %parabolic tilt
    nc_x = 0; %x-coma
    nc_y = 0; %y-coma
    E = exp(nn_x*2*pi*1i*X2D/xlim).*exp(nn_y*2*pi*1i*Y2D/ylim).*exp(np*2*pi*1i*(2*R2D_sq-1)).*exp(nc_x*2*pi*1i*(3*R2D_sq-2).*X2D/xlim).*exp(nc_y*2*pi*1i*(3*R2D_sq-2).*Y2D/ylim);
end
% w0 = 1e-6/X_SC; %waist of beam
% zR = pi*w0^2/lambda;

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.43 for
%787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
%is why we see single atoms)
NA = 0.68; %Specified NA of hires objective (unitless)
% NA = 0.51; %Measured effective NA of the hires objective (787 system)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

%% DMD parameters

spx = 7; %superpixel size

%regular pixels
px_x = 1920;
px_y = 1080;

DMD = zeros(px_x, px_y);

%% input grid

% %the code below basically finds the center of the DMD
% aa = 10;
% ind_x0_DMD = ceil(px_x/2)+1;
% ind_y0_DMD = ceil(px_y/2)+1;
% 
% DMD(ind_x0_DMD, ind_y0_DMD) = 1;

%% Do convolutions

load('imgT.mat');

%normalize
E0 = imgT/max(max(imgT));

% FE0 = fftshift(fft2(E0));
%
% Ediff = E0.*ones(size(X)); %empty vector to store E field at different points on grid
%
% rsq = X.^2 + Y.^2;
% % Fresnel kernel from Wiki
% H = window.*(-1i*k./(2*pi*Z)).*exp(1i*(k./(2*Z)).*rsq + 1i*k*Z);
% The below kernel has a factor of 2 hacked into it, because for some
% strange reason the beam was diffracting too quickly. This kernel produces
% results that agree with both Talbot diffraction theory and what we see in
% the experiment.
% H = window.*(-1i*k/(2*pi*(Z/2))).*exp(1i*(k./(2*(Z/2))).*rsq + 1i*k*(Z/2));

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if tot = len(z)
nx = 1; %number of plots along the x-direction
ny = 1; %number of plots along y-direction

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% aspect ratios
image_aspect = xlim/ylim;

figure('Position', [0,0,400*ny*3,300*nx])
subplot(1,3,1)
imagesc(x*X_SC*1e6,y*X_SC*1e6,...
    abs(E0).^2)
colorbar
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
title('Sim')
xlabel('x (�m)')
ylabel('y (�m)')
pbaspect([1 image_aspect 1])
%     caxis([0 1])
subplot(1,3,2)
imagesc(linspace(0,spotCut-1)*pixSize./mag,linspace(0,spotCut-1)*pixSize./mag,xcon)
colorbar
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
title('Data')
xlabel('x (�m)')
ylabel('y (�m)')
pbaspect([1 image_aspect 1])
caxis([0 1])
subplot(1,3,3)
imagesc(x*X_SC*1e6,y*X_SC*1e6,...
    abs(E0).^2 - xcon)
colorbar
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
title(['Data'])
xlabel('x (�m)')
ylabel('y (�m)')
pbaspect([1 image_aspect 1])
%     caxis([0 1])

sum(sum(abs(abs(E0).^2 - xcon)))