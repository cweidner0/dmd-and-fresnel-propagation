% close all

load('imgT.mat')

pixSize = 16;
mag = 152.4;
spotCut = 21;

figure('Position', [0,0,400*ny,300*nx])
imagesc(linspace(0,spotCut-1)*pixSize./mag,linspace(0,spotCut-1)*pixSize./mag,imgT)