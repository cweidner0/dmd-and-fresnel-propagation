posflag = {0,[-500,-50],0,0,[900,900],[1000, 0]};
px_y = 1080;
px = [0,1000,0,500,0,200];

len = length(bwflag);

imgs = cell(1,len);

im = Hilbert_curve(2,0);
im2 = square_Zernike(5);
for i = 1:len
    if i < 4
        imgs{i} = im;
    else
        imgs{i} = im2;
    end
end

file_preamble = 'test_1000000_False_1';
idx = 1;

DMD_imgs = make_DMD_image(imgs, posflag, px, file_preamble, idx);