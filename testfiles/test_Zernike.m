a = 1e-9;

out = Fresnel_prop_Zernike([0.00,0,0,0,0,0,0,0], E0);
a0 = sum(abs(out{2}));

out = Fresnel_prop_Zernike([0,0,0,0,0,0,0,-a], E0);
sum(abs(out{2})) - a0

out = Fresnel_prop_Zernike([0,0,0,0,0,0,0,a], E0);
sum(abs(out{2})) - a0

out = Fresnel_prop_Zernike([-1e-2,-2e-2,4e-3,0], E0);
sum(abs(out{2})) - a0