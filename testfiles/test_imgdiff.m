load imgdiff.mat

figure('Position', [0,0,400*ny,300*nx])
for i = 1:5
subplot(2,3,i)
imagesc(imgtosave(:,:,i)./max(max(imgtosave(:,:,4))))
colorbar
set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
max(max(imgtosave(:,:,i)./max(max(imgtosave(:,:,4)))))
end

% figure('Position', [0,0,400*ny,300*nx])
% for i = 1:5
% subplot(2,3,i)
% imagesc(imgtosave(:,:,5+i)./max(max(imgtosave(:,:,9))))
% colorbar
% set(gca, 'FontSize', 8, 'FontWeight', 'bold')
%         title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
%         xlabel('x (�m)')
%         ylabel('y (�m)')
%         pbaspect([1 image_aspect 1])
% max(max(imgtosave(:,:,5+i)./max(max(imgtosave(:,:,9)))))
% end
% 
% 
% 
