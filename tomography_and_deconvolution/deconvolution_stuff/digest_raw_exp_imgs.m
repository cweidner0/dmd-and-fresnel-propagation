%% test script for filtered experimental image deconvolution
% C Weidner
% 1 april 2020

tic
close all
clear all
clc

%% parameters

% image parameters

filepath = 'Z:\experiment\Images\2018\06\2018-06-21\oneR7_5exp_rep_scan_v1_180621\';

run_num = 1:100;
img_num = 2:6;

prefix = 'oneR7_5exp_rep_scan_v1_180621_run_';

% filtering parameters

%initial filter (low-pass)
u_lo = 100; %hard cutoff
v_lo = 100; %std of gaussian
filt_lo = hires_kernel_generator_200402('gauss',u_lo,v_lo,'iXon');

%second filter (high_pass)
u_hi = 100; %unused
v_hi = 150; %std of gaussian
filt_hi = hires_kernel_generator_200402('gausshi',u_hi,v_hi,'iXon');

% PSFs

%filtered PSF
load('PSF_filt_u100_v50.mat');
kk1 = ceil(length(PSFs)/2); %choose center PSF
PSF_deconv_1 = PSFs{kk1};

%unfiltered PSF
load('PSF_nofilt.mat'); %load range of PSFs
kk2 = ceil(length(PSFs)/2); %choose center PSF
PSF_deconv_2 = PSFs{kk2};

% thresholding parameters

init_thresh = 0;
peak_thresh = 5e-5;

% edge skipping
edge_skip = 10; % must be at least 1

%% load images

n_runs = length(run_num);
n_imgs = length(img_num);

% make some empty arrays that we will fill with our values
% these will be the columns in our dataTable
% naming convention follows what was used previously
% see Z:\experiment\DATA_analysis_ALICE\2018\2018-06-21\03_oneR7_5exp_rep_scan_v1_180621
% and oneR7_5exp_rep_scan_v5_180621_table_high_tresh.mat
run = [];               % run number
img = [];               % image number
atomNo = [];            % atom number
center = [];            % centers
peakCnt = [];           % raw image peak count
peakCnt3x3 = [];        % raw image peak count 3x3
peakCnt5x5 = [];        % raw image peak count 5x5
peakCnt7x7 = [];        % raw image peak count 7x7
filtPeakCnt = [];       % processed image peak count
filtPeakCnt3x3 = [];    % processed image peak count 3x3
filtPeakCnt5x5 = [];    % processed image peak count 5x5
filtPeakCnt7x7 = [];    % processed image peak count 7x7

% number of pixels beyond center for 3x3, 5x5, and 7x7
n3 = 1;
n5 = 2;
n7 = 3;

% list of how many atoms there are in an image (can tell us if we have
% thresholding problems)
num_atoms = zeros(n_runs, n_imgs);

for i = 1:n_runs
    disp(i)
    for j = 1:n_imgs
        total_path = [filepath, prefix, num2str(run_num(i)), '_', num2str(img_num(j)), '.tif'];
        raw_img = double(imread(total_path)); %load image
        [cent,~,proc_img] = hires_deconv_peak_find_exp_data_200401(raw_img, filt_lo, filt_hi, init_thresh, peak_thresh, PSF_deconv_1, PSF_deconv_2, edge_skip);
        num_atoms(i,j) = size(cent,2);
        for k = 1:size(cent,2)
            % populate our table entries
            % it's all orange because we're making arrays grow without
            % preallocation, because it's the easiest way to do it
            run = [run; run_num(i)];
            img = [img; img_num(j)];
            atomNo = [atomNo; k];
            center = [center; cent(1:2,k)'];
            peakCnt = [peakCnt; raw_img(cent(2,k), cent(1,k))];
            peakCnt3x3 = [peakCnt3x3; sum(sum(raw_img(cent(2,k)-n3:cent(2,k)+n3, cent(1,k)-n3:cent(1,k)+n3)))];
            peakCnt5x5 = [peakCnt5x5; sum(sum(raw_img(cent(2,k)-n5:cent(2,k)+n5, cent(1,k)-n3:cent(1,k)+n5)))];
            peakCnt7x7 = [peakCnt7x7; sum(sum(raw_img(cent(2,k)-n5:cent(2,k)+n7, cent(1,k)-n3:cent(1,k)+n7)))];
            filtpeakCnt = [filtPeakCnt; proc_img(cent(2,k), cent(1,k))];
            filtPeakCnt3x3 = [filtPeakCnt3x3; sum(sum(proc_img(cent(2,k)-n3:cent(2,k)+n3, cent(1,k)-n3:cent(1,k)+n3)))];
            filtPeakCnt5x5 = [filtPeakCnt5x5; sum(sum(proc_img(cent(2,k)-n5:cent(2,k)+n5, cent(1,k)-n3:cent(1,k)+n5)))];
            filtPeakCnt7x7 = [filtPeakCnt7x7; sum(sum(proc_img(cent(2,k)-n5:cent(2,k)+n7, cent(1,k)-n3:cent(1,k)+n7)))];
        end
    end
end

dataTable = table(run, img, atomNo, center, peakCnt3x3, peakCnt5x5, peakCnt7x7, filtPeakCnt3x3, filtPeakCnt5x5, filtPeakCnt7x7);

figure
imagesc(num_atoms)
xlabel('Run number')
ylabel('Image number')
colorbar
caxis([0, 50])
toc

save('fluor_analysis_PiScan_200402_deconv.mat', 'dataTable')