%% Identify pointers which occur multiple times
% Same atom signals in the following image might be detected to correspond
% to multiple different atom signals in the previous image. This occurs if
% atoms hop and get lost and the filtering tolerance for the acceptable
% hopping distance is set too high. This script identifies if same
% IDs/pointers are used multiple times and if yes only keeps the one with
% the smallest distance.
% 
% Latest change: 17.08.2018 Robert Heck

% this has been edited by C Weidner 200403
% to fix an error caused by the following line:
% for ii= 1:length(duplicateNextImgId)

function  [dataTable, numNonUniqueMapping] = filter_non_unique_pointers(dataTable)

Imgs = unique(dataTable.img);
Runs = unique(dataTable.run);

% find indices where there is a non unique mapping occuring
numNonUniqueMapping = 0;
for i = 1:length(Runs)    
    for j = 1:length(Imgs)-1
        % extract certain image of certain run
        dataFiltered = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(j),:);
        [~, uniqueIdx] = unique(dataFiltered.minIdNextImg);
        % check if same pointers of atom signals to next image occur
        % multiple times
        nonUnique = length(uniqueIdx) ~= height(dataFiltered); 
        if nonUnique
            % find IDs which occur multiple times
            duplicateNextImgId = dataFiltered.minIdNextImg(setdiff(1:numel(dataFiltered.minIdNextImg), uniqueIdx));
            %only keep the minimal distance duplicate, discard all others
            for ii= 1:length(duplicateNextImgId)
                % find indices in original array which are minimal
                IduplicateNextImgId = find(dataTable.run==Runs(i) & dataTable.img==Imgs(j) & dataTable.minIdNextImg == duplicateNextImgId(ii));
                [minDist, IminDist] = min(dataTable.minDistNextImg(IduplicateNextImgId));
                % identify the non-minimal indices and erase pointer
                IErase = setdiff(IduplicateNextImgId, IduplicateNextImgId(IminDist));
                dataTable{IErase,{'minIdNextImg'}} = NaN;
                numNonUniqueMapping = numNonUniqueMapping + 1;
            end
        end
    end
end
% count how many incidences of non unique atom ID assigments
% numNonUniqueMapping
