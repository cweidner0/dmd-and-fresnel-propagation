%% Identify atoms which are in the following image
% there is an ismembertol function with comparison method "by row", I will
% for now calculate for each signal in one image the eucildean distances squared to
% each signal in the next image and find the minimal one, save ID of the
% minimal atom in the following image. Corresponds to a list with pointer
% to next element
% 
% Latest change: 17.08.2018 Robert Heck


function  [dataTable] = find_eucledian_dist(dataTable)


dataTable.minDistNextImg = nan(height(dataTable),1);
dataTable.minIdNextImg = nan(height(dataTable),1);
Imgs = unique(dataTable.img);
Runs = unique(dataTable.run);

for i = 1:length(Runs)  
    for j = 1:length(Imgs)-1
        dataFirstImage = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(j),:);
        dataSecondImage = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(j+1),:);
        for k = 1:height(dataFirstImage)
            % calculate eucl distance to each atom in the second image and
            % find minimum
            euclDist = (dataFirstImage.center(k,1)-dataSecondImage.center(:,1)).^2 + (dataFirstImage.center(k,2)-dataSecondImage.center(:,2)).^2;
            [minDist, IminDist]= min(euclDist);
            dataFirstImage.minDistNextImg(k) = minDist;
            dataFirstImage.minIdNextImg(k) = dataSecondImage.atomNo(IminDist);
        end
        % add values to initial table
        dataTable.minDistNextImg(dataTable.run==Runs(i) & dataTable.img==Imgs(j)) = dataFirstImage.minDistNextImg;
        dataTable.minIdNextImg(dataTable.run==Runs(i) & dataTable.img==Imgs(j)) = dataFirstImage.minIdNextImg;
    end
end