% This function builds on the FastPeakFind.m (from Matlab repo)
% That was not fully suitable, especially not for the low SNR signals. That
% function also did some flattening of the images before it started to
% filter. Anyways, this is simpler and works better.
%
% Inputs:
%   img:            The raw image where we want to find peaks.
%   filt_lo:        Kernel for first low-pass filter
%   filt_hi:        Kernel for second high-pass filter
%   init_thresh:    Thresholding after initial filtering
%   peak_thresh:    Thresholding after final deconvolution
%   PSF_deconv_1:   PSF for initial (wide) deconvolution
%   PSF_deconv_2:   PSF for final (narrow) deconvolution
%   NB: set any of these inputs to NaN to skip the corresponding action
%
% 180717 - Ott� El�asson - Add the other option of simple peak finding, case control with variable
%   alg. Clean aslo a bit the thresholding part use now the fourier filter
%   function and the filt variable is the convolution kernel.
% 180731 - Robert Heck - bug (variable name clash df & edg in edge removal) corrected
%  bug (if none of detected areas matches criteria output was all detected
%  peaks) corrected. Now output is a zero array in all these cases.
% 180810 - edit output such that also std of peak count is given out
% 200318 - CW edit for use with deconvolution method, also huge cleanup, made
% 200401/2 - CW edit for use with experimental data
%
% Latest update: 200318
% Carrie Weidner

function  [cent,cent_map,img] = hires_deconv_peak_find_exp_data_200401(raw_img, filt_lo, filt_hi, init_thresh, peak_thresh, PSF_deconv_1, PSF_deconv_2, edge_skip)

% plotting is for testing
plot_stuff = 0;

%% filtering and thresholding and deconvolution, oh my!

% see https://labbook.au.dk/display/HLSG/Deconvolution+and+Linear+Algebra#DeconvolutionandLinearAlgebra-Workingonexperimentaldata
% for a detailed description, but I will try to make this as
% self-explanatory as possible

% set our processed image to the initial image
img = raw_img;

if plot_stuff == 1
    figure
    imagesc(img)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (px)')
    ylabel('y (px)')
    title('Raw image')
    pbaspect([1 1 1])
    colormap(hires_color_map(90))
end

% first, low-pass filtering
if ~isnan(filt_lo)
    [img, ~] = hires_fourier_filter_180716(img, filt_lo);
    if plot_stuff == 1
        figure
        imagesc(img)
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        xlabel('x (px)')
        ylabel('y (px)')
        title('Filtered image')
        pbaspect([1 1 1])
        colormap(hires_color_map(90))
    end
end

% now thresholding
img = img.*(img>init_thresh);

% first deconvolution (with filtered PSF)
if ~isnan(PSF_deconv_1)
    img = deconvlucy(img, PSF_deconv_1);
    if plot_stuff == 1
        figure
        imagesc(img)
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        xlabel('x (px)')
        ylabel('y (px)')
        title('Deconvolved image')
        pbaspect([1 1 1])
        colormap(hires_color_map(90))
    end
end

% second high-pass filtering
if ~isnan(filt_hi)
    [img, ~] = hires_fourier_filter_180716(img, filt_hi);
    if plot_stuff == 1
        figure
        imagesc(img)
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        xlabel('x (px)')
        ylabel('y (px)')
        title('Filtered image 2')
        pbaspect([1 1 1])
        colormap(hires_color_map(90))
    end
end

% second deconvolution (with unfiltered PSF)
if ~isnan(PSF_deconv_2)
    img = deconvlucy(img, PSF_deconv_2);
end

% second thresholding
img = img.*(img>peak_thresh);

%% And the peak finder itself. Borrowed from FastPeakFind.m (from Matlab repo)

% peak find - using the local maxima approach - 1 pixel resolution

% d will be noisy on the edges, and also local maxima looks
% for nearest neighbors so edge must be at least 1. We'll skip 'edge' pixels.
% i.e. skip "edge_skip" edge pixels

img_size = size(img); %image size
[x, y]=find(img(edge_skip:img_size(1)-edge_skip,edge_skip:img_size(2)-edge_skip));

% initialize outputs
test_size = 1000; % if we detect 1000 items we deserve a damn Nobel
cent = zeros(3,test_size); % array of centers (to be filled)
num_cents = 0; % number of centers found
cent_map=zeros(img_size); % amplitude matrix with ones where the center is

x = x + edge_skip-1;
y = y + edge_skip-1;
for j=1:length(y)
    if (img(x(j),y(j)) >= img(x(j)-1,y(j)-1 )) &&...
            (img(x(j),y(j)) > img(x(j)-1,y(j))) &&...
            (img(x(j),y(j)) >= img(x(j)-1,y(j)+1)) &&...
            (img(x(j),y(j)) > img(x(j),y(j)-1)) && ...
            (img(x(j),y(j)) > img(x(j),y(j)+1)) && ...
            (img(x(j),y(j)) >= img(x(j)+1,y(j)-1)) && ...
            (img(x(j),y(j)) > img(x(j)+1,y(j))) && ...
            (img(x(j),y(j)) >= img(x(j)+1,y(j)+1))
        
        
        num_cents = num_cents + 1;
        cent(:,num_cents) = [y(j); x(j); img(x(j), y(j))]; %row with y value, x value, amplitude
        cent_map(x(j),y(j))= cent_map(x(j),y(j)) + img(x(j), y(j)); % if matrix output is desired
        
    end
end

%% Remove zero entries in center matrix

cent(:,num_cents + 1:test_size) = [];

if plot_stuff == 1
    figure
    imagesc(img)
    colorbar
    set(gca, 'FontSize', 8, 'FontWeight', 'bold')
    xlabel('x (px)')
    ylabel('y (px)')
    title('Deconv image 2')
    pbaspect([1 1 1])
    colormap(hires_color_map(90))
    hold on
    scatter(cent(1,:), cent(2,:), 'wo')
end

