%test the lucy-richardson deconvolution algorithm
%for more info look up deconvlucy
%C Weidner
%17 March 2020

close all
clear all
clc

load('img_cell_center_sn_tn0p1.mat') %loads the test image made with simulate_many_lattice_images_grid.m
load('PSF_avg_max.mat') %loads the PSFs propagated through all planes from simulate_one_atom_center.m

PSFs = PSF_avg_max;

img_cent = img_cell; %right now just focus on the center image

%% this code does the deconvolution for all PSFs
% need to look into it more

J = cell(1,5);
ii = 2;

figure('Position', [100, 100, 1000, 600])
subplot(2,3,1)
imagesc(img_cent{ii})
colormap(hires_color_map(90))
colorbar

for i = 1:5
    J{i} = deconvlucy(img_cent{ii}, PSFs{i});
    subplot(2,3,i+1)
    imagesc(J{i})
    colormap(hires_color_map(90))
    colorbar
    title(['Deconv with PSF from plane ', num2str(i-5)])
end

centval = zeros(3,5);
ind0 = ceil(size(J{i}, 2)/2);
for i = 1:3
    for j = 1:9
        JJ = deconvlucy(img_cent{i}, PSFs{j});
        centval(i,j) = JJ(ind0, ind0);
    end
end

figure
hold on
for i = 1:3
    plot(-4:4, centval(i,:), 'LineWidth', 1.5)
end
xlabel('PSF plane')
ylabel('center amplitude')
xticks(-4:4)
legend('two planes out', 'one plane out', 'focus')
grid on


%% This code just deconvolves with respect to the focal PSF
% 
% tic
% J1 = deconvlucy(img_cent{3}, PSFs{3});
% toc
% 
% figure('Position', [100, 100, 1500, 600])
% subplot(1,2,1)
% imagesc(img_cent{3})
% colormap(hires_color_map(90))
% colorbar
% subplot(1,2,2)
% imagesc(J1)
% colormap(hires_color_map(90))
% colorbar
