% script to test data sorting
close all
clear all
clc

%load dataTable
load('fluor_analysis_PiScan_200402_deconv.mat')

%make Id row
dataTable.Id = [1: height(dataTable)]';
%first find eucl dist
[dataTable] = find_eucledian_dist(dataTable);
%filter nonunique pointers
[dataTable, numNonUniqueMapping] = filter_non_unique_pointers(dataTable);
%trace atoms
[IdxSeries] = traceAtoms(dataTable);

% filter for events that are on ALL images
IIdxSeries = cellfun(@length,IdxSeriesRaw)==5;
IdxSeries = IdxSeriesRaw(IIdxSeries);
IIdxSeries = find(cellfun(@length,IdxSeriesRaw)>=5);
IdxSeriesL3To5 = IdxSeriesRaw(IIdxSeries);