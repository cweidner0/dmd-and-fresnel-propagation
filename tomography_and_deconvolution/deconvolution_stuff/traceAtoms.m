%% create series of indices to track same atoms throughout the scan
% start in image one and go for each atom through following images of the same run,
% iterate over run. Finally go to image 2 and 3 to catch also series that
% start later. Discard doublets 
% 
% Latest change: 17.08.2018 Robert Heck



function  [IdxSeries] = traceAtoms(dataTable)


j=1; % start only with very first image for now
IdxSeries = {};
ii = 0;

Imgs = unique(dataTable.img);
Runs = unique(dataTable.run);

for i = 1:length(Runs)
    
    % load first image
    dataFirstImage = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(j),:);
    
    for k=1:height(dataFirstImage)
        
        %find first atom ID, use .Id to have also the absolute position in
        %initial data
        
        jj = j+1; % image counter
        dataSecondImage = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(jj),:);
        minIdNextImg = dataFirstImage.minIdNextImg(k);
        
        % we want at least a series of two (i.e. where there were atoms on three succeeding images)
        % the last one is filtered initially in the non-hopping sorting
        if ismember(minIdNextImg,dataSecondImage.atomNo)
            ii = ii + 1;
            IdxSeries{ii} = [dataFirstImage.Id(k)]; % build for each atom an array of indices
        end
        
        while ismember(minIdNextImg,dataSecondImage.atomNo) % test if atom still is in next image
            
            % if yes write absolute index to list
            IdxSeries{ii} = [IdxSeries{ii} dataSecondImage.Id(minIdNextImg == dataSecondImage.atomNo)];
            
            % update counters, Ids and data sets
            minIdNextImg = dataSecondImage.minIdNextImg(minIdNextImg == dataSecondImage.atomNo);
            jj = jj + 1;
            
            if jj<=length(Imgs)
            dataSecondImage = dataTable(dataTable.run==Runs(i) & dataTable.img==Imgs(jj),:);
            else
                break
            end
            
        end
     
    end
end
IdxSeries = IdxSeries'; % transpose