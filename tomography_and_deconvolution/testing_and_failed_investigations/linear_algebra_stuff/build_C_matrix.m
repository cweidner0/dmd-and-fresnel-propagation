%% Building the matrix of coefficients
% apply this to a vector of length N_x*N_x*N_z (row, col, plane)
% inputs: N_x, N_z, number of transverse and longitudinal sites,
% respectively
% assumes image is square transversally
% outputs: C, matrix of coupling coefficients

function C = build_C_matrix(N_x, N_z)

%% Parameters
%N_x = 5; %number of transverse sites
center_trans_1 = N_x-1;
center_trans_2 = N_x+1;
displace_one_plane = N_x^2;
%N_z = 9; %number of longitudinal sites

%% The following numbers obtained by simulate_one_atom_center.m

%NO ABERRATIONS

%focal plane
c000 = 1;
c010 = 0.1516;
c100 = c010;
c110 = 0.0266;

%one plane off
c001 = 0.9147;
c011 = 0.1511;
c101 = c011;
c111 = 0.0376;

%two planes
c002 = 0.6933;
c012 = 0.1474;
c102 = c012;
c112 = 0.0639;

%three planes
c003 = 0.4202;
c013 = 0.1361;
c103 = c013;
c113 = 0.0894;

%four planes
c004 = 0.1867;
c014 = 0.1144;
c104 = c014;
c114 = 0.0992;

% more planes are easily added because it's just a damn pattern
% just change the z-extent you work with in simulate_one_atom_center.m
% you can even put in aberrations, I don't care

% now build a matrix
dim = N_z*N_x^2;
C = c000*diag(ones(dim, 1)) + c010*diag(ones(dim-1, 1), 1) + c010*diag(ones(dim-1, 1), -1) +... %center and x neighbor plane 0
    c100*diag(ones(dim-N_x, 1), -N_x) + c100*diag(ones(dim-N_x, 1), N_x) + ... %y neighbor plane 0
    c110*diag(ones(dim-center_trans_1, 1), -center_trans_1) + c110*diag(ones(dim-center_trans_1, 1), center_trans_1) + ... %diagonal 1 plane 0
    c110*diag(ones(dim-center_trans_2, 1), -center_trans_2) + c110*diag(ones(dim-center_trans_2, 1), center_trans_2); %diagonal 2 plane 0

if N_z > 1
    C = C + c001*diag(ones(dim-displace_one_plane, 1), -displace_one_plane) + c001*diag(ones(dim - displace_one_plane, 1), displace_one_plane) + ... %center plane 1
    c011*diag(ones(dim-displace_one_plane-1, 1), -displace_one_plane-1) + c011*diag(ones(dim-displace_one_plane+1, 1), -displace_one_plane+1) + ... %x neighbor 1 plane 1
    c011*diag(ones(dim-displace_one_plane-1, 1), displace_one_plane+1) + c011*diag(ones(dim-displace_one_plane+1, 1), displace_one_plane-1) + ... %x neighbor 2 plane 1
    c101*diag(ones(dim-displace_one_plane-N_x, 1), -displace_one_plane-N_x) + c101*diag(ones(dim-displace_one_plane-N_x, 1), displace_one_plane+N_x) + ... %y neighbor 1 plane 1
    c101*diag(ones(dim-displace_one_plane+N_x, 1), displace_one_plane-N_x) + c101*diag(ones(dim-displace_one_plane+N_x, 1), -displace_one_plane+N_x) + ... %y neighbor 2 plane 1
    c111*diag(ones(dim-displace_one_plane-center_trans_1, 1), -displace_one_plane-center_trans_1) + c111*diag(ones(dim-displace_one_plane+center_trans_1, 1), -displace_one_plane+center_trans_1) + ... %diagonal 1 plane 1
    c111*diag(ones(dim-displace_one_plane-center_trans_1, 1), displace_one_plane+center_trans_1) + c111*diag(ones(dim-displace_one_plane+center_trans_1, 1), displace_one_plane-center_trans_1) + ... %diagonal 2 plane 1
    c111*diag(ones(dim-displace_one_plane-center_trans_2, 1), -displace_one_plane-center_trans_2) + c111*diag(ones(dim-displace_one_plane+center_trans_2, 1), -displace_one_plane+center_trans_2) + ... %diagonal 3 plane 1
    c111*diag(ones(dim-displace_one_plane-center_trans_2, 1), displace_one_plane+center_trans_2) + c111*diag(ones(dim-displace_one_plane+center_trans_2, 1), displace_one_plane-center_trans_2); %diagonal 4 plane 1
end
if N_z > 3
    C = C + c002*diag(ones(dim-2*displace_one_plane, 1), -2*displace_one_plane) + c002*diag(ones(dim - 2*displace_one_plane, 1), 2*displace_one_plane) + ... %center plane 2
    c012*diag(ones(dim-2*displace_one_plane-1, 1), -2*displace_one_plane-1) + c012*diag(ones(dim-2*displace_one_plane+1, 1), -2*displace_one_plane+1) + ... %x neighbor 1 plane 2
    c012*diag(ones(dim-2*displace_one_plane-1, 1), 2*displace_one_plane+1) + c012*diag(ones(dim-2*displace_one_plane+1, 1), 2*displace_one_plane-1) + ... %x neighbor 2 plane 2
    c102*diag(ones(dim-2*displace_one_plane-N_x, 1), -2*displace_one_plane-N_x) + c102*diag(ones(dim-2*displace_one_plane-N_x, 1), 2*displace_one_plane+N_x) + ... %y neighbor 1 plane 2
    c102*diag(ones(dim-2*displace_one_plane+N_x, 1), 2*displace_one_plane-N_x) + c102*diag(ones(dim-2*displace_one_plane+N_x, 1), -2*displace_one_plane+N_x) + ... %y neighbor 2 plane 2
    c112*diag(ones(dim-2*displace_one_plane-center_trans_1, 1), -2*displace_one_plane-center_trans_1) + c112*diag(ones(dim-2*displace_one_plane+center_trans_1, 1), -2*displace_one_plane+center_trans_1) + ... %diagonal 1 plane 2
    c112*diag(ones(dim-2*displace_one_plane-center_trans_1, 1), 2*displace_one_plane+center_trans_1) + c112*diag(ones(dim-2*displace_one_plane+center_trans_1, 1), 2*displace_one_plane-center_trans_1) + ... %diagonal 2 plane 2
    c112*diag(ones(dim-2*displace_one_plane-center_trans_2, 1), -2*displace_one_plane-center_trans_2) + c112*diag(ones(dim-2*displace_one_plane+center_trans_2, 1), -2*displace_one_plane+center_trans_2) + ... %diagonal 3 plane 2
    c112*diag(ones(dim-2*displace_one_plane-center_trans_2, 1), 2*displace_one_plane+center_trans_2) + c112*diag(ones(dim-2*displace_one_plane+center_trans_2, 1), 2*displace_one_plane-center_trans_2); %diagonal 4 plane 2
end
if N_z > 5
    C = C + c003*diag(ones(dim-3*displace_one_plane, 1), -3*displace_one_plane) + c003*diag(ones(dim - 3*displace_one_plane, 1), 3*displace_one_plane) + ... %center plane 3
    c013*diag(ones(dim-3*displace_one_plane-1, 1), -3*displace_one_plane-1) + c013*diag(ones(dim-3*displace_one_plane+1, 1), -3*displace_one_plane+1) + ... %x neighbor 1 plane 3
    c013*diag(ones(dim-3*displace_one_plane-1, 1), 3*displace_one_plane+1) + c013*diag(ones(dim-3*displace_one_plane+1, 1), 3*displace_one_plane-1) + ... %x neighbor 2 plane 3
    c103*diag(ones(dim-3*displace_one_plane-N_x, 1), -3*displace_one_plane-N_x) + c103*diag(ones(dim-3*displace_one_plane-N_x, 1), 3*displace_one_plane+N_x) + ... %y neighbor 1 plane 3
    c103*diag(ones(dim-3*displace_one_plane+N_x, 1), 3*displace_one_plane-N_x) + c103*diag(ones(dim-3*displace_one_plane+N_x, 1), -3*displace_one_plane+N_x) + ... %y neighbor 2 plane 3
    c113*diag(ones(dim-3*displace_one_plane-center_trans_1, 1), -3*displace_one_plane-center_trans_1) + c113*diag(ones(dim-3*displace_one_plane+center_trans_1, 1), -3*displace_one_plane+center_trans_1) + ... %diagonal 1 plane 3
    c113*diag(ones(dim-3*displace_one_plane-center_trans_1, 1), 3*displace_one_plane+center_trans_1) + c113*diag(ones(dim-3*displace_one_plane+center_trans_1, 1), 3*displace_one_plane-center_trans_1) + ... %diagonal 2 plane 3
    c113*diag(ones(dim-3*displace_one_plane-center_trans_2, 1), -3*displace_one_plane-center_trans_2) + c113*diag(ones(dim-3*displace_one_plane+center_trans_2, 1), -3*displace_one_plane+center_trans_2) + ... %diagonal 3 plane 3
    c113*diag(ones(dim-3*displace_one_plane-center_trans_2, 1), 3*displace_one_plane+center_trans_2) + c113*diag(ones(dim-3*displace_one_plane+center_trans_2, 1), 3*displace_one_plane-center_trans_2); %diagonal 4 plane 3
end
if N_z > 7
    C = C + c004*diag(ones(dim-4*displace_one_plane, 1), -4*displace_one_plane) + c004*diag(ones(dim - 4*displace_one_plane, 1), 4*displace_one_plane) + ... %center plane 4
    c014*diag(ones(dim-4*displace_one_plane-1, 1), -4*displace_one_plane-1) + c014*diag(ones(dim-4*displace_one_plane+1, 1), -4*displace_one_plane+1) + ... %x neighbor 1 plane 4
    c014*diag(ones(dim-4*displace_one_plane-1, 1), 4*displace_one_plane+1) + c014*diag(ones(dim-4*displace_one_plane+1, 1), 4*displace_one_plane-1) + ... %x neighbor 2 plane 4
    c104*diag(ones(dim-4*displace_one_plane-N_x, 1), -4*displace_one_plane-N_x) + c104*diag(ones(dim-4*displace_one_plane-N_x, 1), 4*displace_one_plane+N_x) + ... %y neighbor 1 plane 4
    c104*diag(ones(dim-4*displace_one_plane+N_x, 1), 4*displace_one_plane-N_x) + c104*diag(ones(dim-4*displace_one_plane+N_x, 1), -4*displace_one_plane+N_x) + ... %y neighbor 2 plane 4
    c114*diag(ones(dim-4*displace_one_plane-center_trans_1, 1), -4*displace_one_plane-center_trans_1) + c114*diag(ones(dim-4*displace_one_plane+center_trans_1, 1), -4*displace_one_plane+center_trans_1) + ... %diagonal 1 plane 4
    c114*diag(ones(dim-4*displace_one_plane-center_trans_1, 1), 4*displace_one_plane+center_trans_1) + c114*diag(ones(dim-4*displace_one_plane+center_trans_1, 1), 4*displace_one_plane-center_trans_1) + ... %diagonal 2 plane 4
    c114*diag(ones(dim-4*displace_one_plane-center_trans_2, 1), -4*displace_one_plane-center_trans_2) + c114*diag(ones(dim-4*displace_one_plane+center_trans_2, 1), -4*displace_one_plane+center_trans_2) + ... %diagonal 3 plane 4
    c114*diag(ones(dim-4*displace_one_plane-center_trans_2, 1), 4*displace_one_plane+center_trans_2) + c114*diag(ones(dim-4*displace_one_plane+center_trans_2, 1), 4*displace_one_plane-center_trans_2); %diagonal 4 plane 4
end