% ****** Start of file aipsamp.tex ******
%
%   This file is part of the AIP files in the AIP distribution for REVTeX 4.
%   Version 4.1 of REVTeX, October 2009
%
%   Copyright (c) 2009 American Institute of Physics.
%
%   See the AIP README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex  aipsamp
%  2)  bibtex aipsamp
%  3)  latex  aipsamp
%  4)  latex  aipsamp
%
% Use this file as a source of example code for your aip document.
% Use the file aiptemplate.tex as a template for your document.
\documentclass[%
 aps,
 pra,
 floatfix,
 amsmath, amssymb,
 preprint,%
% reprint,%
%author-year,%
%author-numerical,%
]{revtex4-1}

\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\usepackage{graphicx}% Include figure files
\graphicspath{ {./figures/} }
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines
%\usepackage[caption=false]{subfig}
%\usepackage{subfig}
%\usepackage{fancyhdr}
%\pagestyle{fancy}
%\rhead{Draft copy}
\usepackage{braket} 
\usepackage{amsmath}
\usepackage{amsfonts}

% A simple alternative to e.g. the todonotes package.
\usepackage{xcolor}
\newcommand{\todo}[1]{{\color[rgb]{1,0,0}\textbf{$\blacktriangleright$#1$\blacktriangleleft$}}}
\newcommand{\tofig}[1]{{\color[rgb]{0,0.8,0}\textbf{$\blacktriangleright$#1$\blacktriangleleft$}}}
\newcommand{\tocite}[1]{{\color[rgb]{0,0,1}\textbf{$\blacktriangleright$#1$\blacktriangleleft$}}}
\newcommand{\changed}[1]{{\color[rgb]{1,0,0}#1}}


\begin{document}

%\preprint{APS/123-QED}

\title{Atom finding with linear algebra}


%\footnote{Error!}}% Force line breaks with \\
%\thanks{Footnote to title of article.}

\author{Carrie A. Weidner}
 \affiliation{ 
Institut for Fysik og Astronomi, Aarhus Universitet, 8000 Aarhus C, Denmark
}%

\date{\today}% It is always \today, today,
             %  but any date may be explicitly specified

\begin{abstract}

\end{abstract}

\pacs{Valid PACS appear here}% PACS, the Physics and Astronomy
                             % Classification Scheme.
\keywords{Quantum Gas Microscopes, Tomography }%Use showkeys class option if keyword
                              %display desired
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

Here I am writing out mathematics for how the light intensity from an atom in a given lattice site affects the intensities measured on other planes. Then, this is re-formulated into a linear algebra problem where, given the measured intensities (averaged over lattice sites of $5\times 5$ pixels) and a matrix of these correlations, one can extract the positions of the atoms.

This document does not contain any information on the method's efficacy and speed, as that will be posted elsewhere.

\section{Mathematics}

\subsection{Setting up the linear algebra}

Given a set of $M_z$ quantum gas microscopy images of atoms in an $N_x\times N_y$ grid of pixels, with each image is focused on a given $z$-plane $n_z \in \{0,...M_z\}$. Each image extends out $p_x$ and $p_y$ pixels in the $x$- and $y$-planes.

For a given image, we can average all pixels corresponding to a single lattice site, that is, a $\delta x \times \delta y = 532$~nm-by-$532$~nm grid centered around each lattice site. For the experiment, this corresponds a superpixel comprising a $5\times 5$ pixel grid, as a single pixel is $105\times 105$~nm$^2$. In what follows, we consider a transverse grid of $M_x\times M_y$ of these superpixel-averaged sites.

In order to apply linear algebraic techniques to this system, we must turn the images into a vector of length $M_x\times M_y \times M_z$. Note that in the following we assume that the image is transversally square, that is, $M_x = M_y$.

Therefore, for each superpixel-averaged image, we stack the columns on top of each other to create a vector $\vec{\beta}$ with $M_x^2$ entries. For each consecutive plane, we then stack the vectors corresponding to each image on top of one another, resulting in a vector of size $M_x^2 M_z$. We call this vector $\vec{b}$, and its entries are described in Table~\ref{tab:locs}. 

What we wish to obtain is a vector $\vec{a}$ of size $M_x^2 M_z$ where each entry $a_i$ corresponds to the same lattice site as in the corresponding vector $\vec{b}$. Each entry contains a value representing the amplitude of the emission from this site. We expect sites with atoms to have relatively high emission amplitudes, and sites without atoms to have relatively low amplitudes. Therefore, by thresholding the values in this matrix, we can predict where the atoms will be.

The coupling matrix, then, such that
\begin{equation}
\label{eq:LA_QGM}
C\vec{a} = \vec{b}
\end{equation}
is a $M_zM_x^2 \times M_zM_x^2$ matrix $C$ of entries $c_{i,j}$ corresponding to how much an atom in (vectorized) site $i$ couples to an atom in (vectorized) site $j$. These couplings are known in advance due to the theory of Fresnel diffraction of a Bessel beam of given wavelength $\lambda$ (here, $\lambda = 780$~nm) focused by an objective of numerical aperture $\mathrm{NA}$ (here, $NA = 0.7$).

We assume here that $C$ is invertible (that is, the rank of $C$ is the same as the size of $C$). Therefore, given $\vec{b}$ and $C$, we should be able to find an (unthresholded) $\vec{a}$ by
\begin{equation}
\label{eq:LA_QGM_inv}
\vec{a} = C^{-1}\vec{b}.
\end{equation}

\subsection{Bessel beam propagation}

Assuming a normalized intensity, a Bessel beam propagates transversally along $x$ and $y$ as
\begin{equation}
\label{eq:Bessel_trans}
I_\mathrm{trans}(\rho) = (2J_1(\rho)/\rho)^2,
\end{equation}
where $\rho = 2\pi r \mathrm{NA}/\lambda$, $r^2 = x^2 + y^2$, and $J_1$ is the first Bessel function of the first kind. The beam propagates longitudinally along $z$ as
\begin{equation}
\label{eq:Bessel_long}
I_\mathrm{long}(\rho) = (\sin(\zeta)/\zeta)^2,
\end{equation}
where $\zeta = \pi z \mathrm{NA}^2/2\lambda$.

The propagation in and outside of the focal plane can be calculated via Fresnel diffraction.

Therefore, because the transverse and longitudinal spread is non-negligible outside of a single lattice site, an emitter of a given intensity $I_0$ will contribute to the measured intensity on a different site. That is, a given atom at the origin will contribute an intensity $I_0$ to its own site. Setting this quantity to one, we can then calculate the relative contribution of the atom to its nearest neighbors. For this calculation, we assume no aberrations (e.g. coma) so the atoms' emission is symmetric transversally and longitudinally. We also neglect planes where all contributions fall below the $10\%$ level, so we consider only the four planes above and below the focal plane.

This leads to an effective 3x3x9 set of numbers that describe how an atom on a site $(i,j,k)$ effects an atom on site $(i', j', k')$. Due to symmetry, this is effectively only $41$ values that need to be tabulated, as below, for each column $i$. In the following table, we set $D = N_x^2 - 1$

\begin{table}
\label{tab:locs}
\caption{A table showing how the different lattice elements link together within a given column $i$ of the matrix $C$. Note that without the second index, the indices also denote the relative position of atoms in the vectors $\vec{a}$ and $\vec{b}$, where the center atom in the center plane is given the index $(0,0)$.}
\begin{center}
\begin{tabular}{| c | c | c |}
\hline	
Matrix element				& Value		& Corresponding location		\\
\hline	\hline
$c_{i,i}$					& $1$ 		& atom site						\\ \hline
$c_{i\pm 1,i}$				& $0.1516$	& y-neighbor, same plane		\\ \hline
$c_{i\pm N_x,i}$			& $0.1516$	& x-neighbor, same plane		\\ \hline
$c_{i\pm N_x\pm 1,i}$		& $0.0266$	& diagonal, same plane			\\ \hline
$c_{i\pm D,i}$				& $0.9147$	& on-site, one plane away		\\ \hline
$c_{i\pm D\pm 1,i}$			& $0.1511$	& y-neighbor, one plane away	\\ \hline
$c_{i\pm D\pm N_x,i}$		& $0.1511$	& x-neighbor, one plane away	\\ \hline
$c_{i\pm D\pm N_x\pm 1,i}$	& $0.0376$	& diagonal, one plane away		\\ \hline
$c_{i\pm 2D,i}$				& $0.6933$	& on-site, two planes away		\\ \hline
$c_{i\pm 2D\pm 1,i}$		& $0.1474$	& y-neighbor, two planes away	\\ \hline
$c_{i\pm 2D\pm N_x,i}$		& $0.1474$	& x-neighbor, two planes away	\\ \hline
$c_{i\pm 2D\pm N_x\pm 1,i}$	& $0.0639$	& diagonal, two planes away		\\ \hline
$c_{i\pm 3D,i}$				& $0.4202$	& on-site, three planes away	\\ \hline
$c_{i\pm 3D\pm 1,i}$		& $0.1361$	& y-neighbor, three planes away	\\ \hline
$c_{i\pm 3D\pm N_x,i}$		& $0.1361$	& x-neighbor, three planes away	\\ \hline
$c_{i\pm 3D\pm N_x\pm 1,i}$	& $0.0894$	& diagonal, four planes away	\\ \hline
$c_{i\pm 4D,i}$				& $0.1867$	& on-site, four planes away		\\ \hline
$c_{i\pm 4D\pm 1,i}$		& $0.1144$	& y-neighbor, four planes away	\\ \hline
$c_{i\pm 4D\pm N_x\pm1,i}$	& $0.1144$	& x-neighbor, four planes away 	\\ \hline
$c_{i\pm 4D\pm N_x\pm 1,i}$	& $0.0992$	& diagonal, four planes away	\\ \hline
\hline
\end{tabular}
\end{center}
\end{table}

\section{The results}

I will post the results on Basecamp and in the labbook.

%\bibliography{tomograpy_biblio.bib}
\end{document}
