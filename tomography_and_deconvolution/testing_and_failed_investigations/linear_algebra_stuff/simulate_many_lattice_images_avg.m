%Script to simulate the images from the QGM
%Generates multiple images
%Updated version of simulate_many_lattice_images.m
%Guarantees atoms are incoherent scatterers
%Slow if you have too many atoms
%Works on a grid and does a lot of averaging
%Carrie Weidner
%Aarhus University
%24 March 2020

close all; clc;
rng('shuffle')

file = 'more_atoms_Z7_0p000045_technoise_0p1_shotnoise_0p1';

%% TODO
% NB: to fix this I made the pixel size 106.4 nm
% get rid of stripes by distributing photon counts around nearest pixels
% based on atom position (could alternatively make pixel size 106.4 nm
% instead of 105 nm

%% Options

export_abs = 0; %exports images and plots
show_locations = 1; % show locations of atoms
save_test = 0; %save test images for deconvolution algorithm
do_averaging = 1;
plot_avg = 1;

% ideally, this should work just fine if we define a superpixel and do the
% convolution, but this does not work right now
% so keep this at zero
do_superpixel = 0;
% use the FFT method to do the Fresnel propagation if this is set to 1
% otherwise use the convolutional method
% FFT method is faster and thus recommended
do_FFT = 1;
% use a window (see relevant code below for more details)
do_window = 0;
% add phase front (model aberrations)
do_phase_front = 1;

%% Constants, etc

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 780e-9; % atoms
lambda = lambda_proj/X_SC;
k = 2*pi/lambda;
K = k;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
aa = 3;
zlim = aa*LAMBDA_LATT_SC; %this is give total planes
dx = LAMBDA_LATT_SC/10; % each hires pixel is 105 nm--let's change it to 106.4 for lattice reasons
dy = LAMBDA_LATT_SC/10;
px_x = 512; % total number of pixels
% the total span of each image is about 20 �m, so 10�m in each direction
xlim = px_x*dx/2;
ylim = xlim;
dz = LAMBDA_LATT_SC/2; %z-resolution is one plane

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -zlim:dz:zlim;
% z = 0:dz:zlim;
% z = 2*LAMBDA_LATT_SC-zlim:dz:2*LAMBDA_LATT_SC+zlim;
len_z = length(z);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);
R2D_sq = (X2D/xlim).^2 + (Y2D/ylim).^2; %scaled radius, for aberration)
% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

% set up grid in frequency space
fx = linspace(-pi/dx, pi/dx, len_x);
dfx = fx(2) - fx(1);
fy = linspace(-pi/dy, pi/dy, len_y);
dfy = fy(2) - fy(1);

% define window function for Fresnel kernel H
% this is important so that we do not have obnoxious repeating patterns in
% our Fresnel propagation/DMD convolution code
% basically, we need to force things to zero at the edge of the window if
% we have small patterns (like a single light pixel in the center of the DMD)
% one has to be very careful with this

if do_window == 1
    %WINDOW (DO THIS IF THINGS START REPEATING)
    divH = 6;
    window = zeros(len_x, len_y);
    winx = ceil(len_x/divH);
    winy = ceil(len_y/divH);
    window(x0_ind-ceil(winx):x0_ind+ceil(winx), y0_ind-ceil(winy):y0_ind+ceil(winy)) = ...
        coswin(length(x0_ind-ceil(winx):x0_ind+ceil(winx)), 11)*...
        coswin(length(y0_ind-ceil(winy):y0_ind+ceil(winy)), 11)';
else
    %NO WINDOW
    window = ones(size(X2D));
end

%% Field parameters

if do_phase_front == 0
    E = ones(size(X2D)); %amplitude of field (in lattice recoils)
else
    % add Zernike polynomials phase front
    % see https://www.researchgate.net/publication/233985158_Aberrated_laser_beams_in_terms_of_Zernike_polynomials
    R2D_sq = (X2D).^2 + (Y2D).^2; %scaled radius, for aberration)
    R2D_sq_minus = (X2D).^2 - (Y2D).^2; %scaled radius, for aberration)
    % Zp1 = 1; %piston
    Zp2 = 2*X2D; %x-tilt
    Zp3 = 2*Y2D; % y-tilt
    Zp4 = sqrt(3)*(2*R2D_sq - 1); %defocus
    % Zp5 = 2*sqrt(6)*X2D*Y2D; %astigmatism 1
    % Zp6 = sqrt(6)*R2D_sq_minus; %astigmatism 2
    Zp7 = sqrt(8)*(3*R2D_sq*Y2D-2*Y2D); %coma y
    % Zp8 = sqrt(8)*(3*R2D_sq*X2D-2*X2D); %coma x
    % Zp = {Zp1, Zp2, Zp3, Zp4, Zp5, Zp6, Zp7, Zp8};
    Zp = {Zp2, Zp3, Zp4, Zp7};
    
    phase_factor = zeros(size(X2D));
    
    %     coeffs = [0, 0, 0, 0.000045];
    coeffs = [0,0,0,0];
    
    for i = 1:length(Zp)
        phase_factor = phase_factor + coeffs(i)*Zp{i};
    end
    
    E = exp(1i*K*phase_factor);
end

n_imgs = 1; % number of images to generate

%% PSF parameters (Bessel beam)

%Bessel beam
%making the NA higher will make the beam itself smaller
%the maximum NA the system can have is 0.7
%our experimental measurements suggest our effective NA is about 0.68 for
% the 780 nm imaging system, which is why we see single atoms)
NA = 0.7; %Specified NA of hires objective (unitless)
a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
zp = f; %distance from the objective
PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

% where on our pixel grid do we see atoms?
% Because this is adapted from previous code, we call this the DMD
% IT'S REALLY ATOMS DON'T FALL FOR MY LIES
% NB: this program explodes if you have a blank grid of pixels
% to subvert this, turn at least one of the DMD pixels on!

% Where are holes?
% we have on average atoms_mean atoms per shot
% They are distributed throughout the planes as follows:
% -2: 7%
% -1: 23%
%  0: 47%
%  1: 18%
%  2: 5%
% we determine how many atoms we have by a Gaussian distribution
% with some width (keeps things interesting)
atoms_width = 1.5;
% plane_pcts = [0, 0, 0.07, 0.23, 0.47, 0.18, 0.05, 0, 0];
% plane_pcts = [0, 0, 1, 0, 0];
plane_pcts = zeros(len_z, 1);
n_planes = length(plane_pcts);
if n_planes ~= len_z
    error('This code is not equipped to handle scans where the z-dimension is longer than the number of planes.')
end
atoms_mean = 3;
planes_mean = atoms_mean*plane_pcts;

spx = 1; %was 7
%place the atoms only on valid lattice sites
ind0_x = ceil(len_x/2);
ind0_y = ceil(len_y/2);
n_px_grid = 5; % how many pixels in one lattice site?
atom_pix = 2*n_px_grid; %where we load atoms, total extent px_x pixels
lim = atom_pix*dx;
xlim_atoms = lim;
x_atoms = -xlim_atoms:dx:xlim_atoms;
x_latt = 532e-9/X_SC; %distance between lattice sites
sites = -lim:x_latt:lim; % where are the lattice sites within our limits?
diffs = abs(x - sites');
[~, site_inds] = min(diffs, [], 2);
len_sites = length(site_inds);
site_mesh = meshgrid(site_inds, site_inds); % make a mesh of all available sites

%% Fresnel propagation
% run this multiple times per image
for kk = 1:n_imgs
    
    tic
%     n_atoms_plane = round(atoms_width.*randn(5,1) + planes_mean'); %round to nearest atom
    n_atoms_plane = zeros(len_z, 1);
    plane_center = ceil(len_z/2);
    n_atoms_plane(plane_center-1) = 1;
    n_atoms_plane(plane_center) = 3;
    n_atoms_plane(plane_center+1) = 1;
    atom_locs = cell(1,len_z);
    
    % for each atom, randomly determine its location somewhere in the image
    I0 = cell(1,len_z);
    for i = 1:len_z
        I0{i} = zeros(size(X2D));
    end
    for i = 1:len_z
        if n_atoms_plane(i) < 0
            n_atoms_plane(i) = 0;
        end
        atom_loc_plane = randi(len_sites, 2, n_atoms_plane(i)); %trying to get it to snap to a grid
        atom_locs{i} = atom_loc_plane;
        for ii = 1:len_sites
            for jj = 1:len_sites
                if any(all([ii;jj] == atom_loc_plane))
                    atom_px = zeros(size(X2D));
                    atom_px(site_inds(ii), site_inds(jj)) = sqrt(round(0.*randn(1) + 150));
                    E0 = conv2(PSF, atom_px/(spx^2), 'same');
                    for mm = 1:len_z
                        if mm == i
                            % in plane, just find the intensity
                            I0{mm} = I0{mm} + abs(E0).^2;
                        else
                            % Do Fresnel propagation
                            dz = z(i) - z(mm);
                            % THE LINE BELOW IS RIGHT DON'T MESS WITH IT
                            I0{mm} = I0{mm} + abs(propTF(E0,len_x,lambda,5*dz)).^2; %from file exchange
                        end
                    end
                end
            end
        end
    end
    n_atoms_total = sum(n_atoms_plane)
    
    toc
    
    %% Add noise
    %     rsq = X2D.^2 + Y2D.^2;
    Iplanes = zeros(len_x, len_y, len_z);
    for i = 1:len_z
        Iplanes(:,:,i) = I0{i};
    end
    %         aa = 0.1;
    %         bb = 0.1;
    aa = 0;
    bb = 0;
    % add shot noise (not sure if this is done correctly)
    Iplanes = Iplanes + bb*sqrt(Iplanes).*randn(size(Iplanes)) + abs(sqrt(aa)*randn(size(Iplanes)));
    
    %% Plotting
    
    % OPTIONS FOR FRESNEL PLOTTING
    % this works best if tot = len(z)
    nx = 2; %number of plots along the x-direction
    ny = 3; %number of plots along y-direction
    tot = nx*ny; %total number of plots
    if tot > len_z
        % basically, you can't plot any more points than you have calculated
        % this will leave blank plots but it's better than stupid errors
        tot = len_z;
    end
    
    % plotting limits
    dim_x = 5*dx;
    dim_y = 5*dx;
    plotlim_x = dim_x;
    plotlim_y = dim_y;
    [~, xlim_neg_ind] = min(abs(x + dim_x));
    [~, xlim_pos_ind] = min(abs(x - dim_x));
    [~, ylim_neg_ind] = min(abs(y + dim_y));
    [~, ylim_pos_ind] = min(abs(y - dim_y));
    
    image_aspect = plotlim_x/plotlim_y;
    
    % plot the magnitude of the field at all Fresnel points
    % and export the images
    %     Lmax = 750;
    
    if save_test == 1
        img_cell = cell(1,5);
    end
    
    Lmax = 1;
    if export_abs == 1
        for i = 1:n_planes
            img = Iplanes(xlim_neg_ind:xlim_pos_ind,ylim_neg_ind:ylim_pos_ind,i)/Lmax;
            %             img = Lmax/max(max(img));
            if save_test == 1
                img_cell{i} = img;
            end
            figure(i)
            imagesc(x(xlim_neg_ind:xlim_pos_ind)*X_SC*1e6,y(ylim_neg_ind:ylim_pos_ind)*X_SC*1e6, imrotate(img, 90))
            colorbar
            set(gca, 'FontSize', 8, 'FontWeight', 'bold')
            title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
            xlabel('x (�m)')
            ylabel('y (�m)')
            pbaspect([1 image_aspect 1])
            caxis([0 200])
            colormap(hires_color_map(90))
            % if you want to invert the colormap uncomment the below line
            % colormap(flipud(hires_color_map(90)))
            if show_locations == 1
                hold on
                for j = 1:n_planes
                    atom_loc_plane = atom_locs{j};
                    for k = 1:n_atoms_plane(j)
                        if j ~= i
                            plot(y(site_inds(atom_loc_plane(1,k)))*X_SC*1e6, -x(site_inds(atom_loc_plane(2,k)))*X_SC*1e6, 'ro', 'MarkerSize', 10)
                        else
                            plot(y(site_inds(atom_loc_plane(1,k)))*X_SC*1e6, -x(site_inds(atom_loc_plane(2,k)))*X_SC*1e6, 'bo', 'MarkerSize', 10)
                        end
                    end
                end
            end
            % uncomment me to write images
            % imwrite(img, ['Z:\experiment\Images\simulated_QGM_images\', file, '\', file, '_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
            %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
            %             imwrite(img, ['C:\Users\au605200\Dropbox\CW\UA\MATLAB\Fresnel_prop\for_git\DMD_Fresnel_prop\latt_images\test_images_', num2str(kk), '_', num2str(i), '.tif'], 'Compression', 'none')
        end
    end
    
    if do_averaging == 1
        tic
        ind0_x = ceil(len_x/2);
        ind0_y = ceil(len_y/2);
        d_site = 5; %pixels per site
        n_sites = 9;
        x_ex_avg_n = ind0_x - 2 - 5*n_sites;
        x_ex_avg_p = ind0_x + 2 + 5*n_sites;
        len_avgx = length(x_ex_avg_n:x_ex_avg_p);
        len_avgx_avgd = len_avgx/d_site;
        midlen = ceil(len_avgx_avgd/2);
        img_avg = zeros(len_avgx_avgd, len_avgx_avgd, n_planes);
        for i = 1:n_planes
            img = Iplanes(x_ex_avg_n:x_ex_avg_p,x_ex_avg_n:x_ex_avg_p,i);
%             img = img/max(max(Iplanes(x_ex_avg_n:x_ex_avg_p,x_ex_avg_n:x_ex_avg_p,ceil(n_planes/2))));
            for j = 1:len_avgx_avgd
                for k = 1:len_avgx_avgd
                    img_avg(j,k,i) = mean(mean(img(d_site*(j-1)+1:d_site*j, d_site*(k-1)+1:d_site*k)));
                end
            end
        end
        img_avg = img_avg./img_avg(midlen, midlen,ceil(n_planes/2));
        
        N_x = 2*n_sites + 1;
%         b = [];
%         for i = 1:n_planes
%             bvec = reshape(img_avg(:,:,i), N_x^2, 1);
%             b = [b; bvec];
%         end
%         b
        size(img_avg)
        img_avg_trans = reshape(img_avg, N_x^2*n_planes, 1);
        C = build_C_matrix(N_x, n_planes);
        a = C\img_avg_trans;
        a = reshape(a, N_x, N_x, n_planes);
        thresh = max(max(max(a)))/3;
        a(a < thresh) = 0;
        planes = 1:n_planes;
        figure('Position', [0, 0, 600, 1000])
        if plot_avg == 1
            for i = 1:5
                subplot(5,2,2*i-1)
                imagesc(imrotate(img_avg(:,:,planes(plane_center - 3 + i)), 90))
                colorbar
                set(gca, 'FontSize', 8, 'FontWeight', 'bold')
                title(['z = ', num2str(z(plane_center - 3 + i)*X_SC*1e6), ' �m'])
                xlabel('x (�m)')
                ylabel('y (�m)')
                pbaspect([1 image_aspect 1])
                colormap(hires_color_map(90))
                subplot(5,2,2*i)
                imagesc(imrotate(a(:,:,planes(plane_center - 3 + i)), 90))
                colorbar
                set(gca, 'FontSize', 8, 'FontWeight', 'bold')
                title(['z = ', num2str(z(plane_center - 3 + i)*X_SC*1e6), ' �m'])
                xlabel('x (�m)')
                ylabel('y (�m)')
                pbaspect([1 image_aspect 1])
                colormap(hires_color_map(90))
%                 caxis([0,1])
            end
        end
    end
    toc
end

if save_test == 1
    save(['img_cell_aa0p1_bb0p1_', num2str(n_atoms_total), '.mat'], 'img_cell')
end