function out = Fresnel_prop_Zernike(coeffs, E0)

%for Zernike optimization

global x y z X2D Y2D

len_x = length(x);
len_y = length(y);
len_z = length(z);

global LAMBDA K z0

% 3D field grid
[X,Y,Z] = meshgrid(x,y,z);

%% Zernike polynomials
% see https://www.researchgate.net/publication/233985158_Aberrated_laser_beams_in_terms_of_Zernike_polynomials
R2D_sq = (X2D).^2 + (Y2D).^2; %scaled radius, for aberration)
R2D_sq_minus = (X2D).^2 - (Y2D).^2; %scaled radius, for aberration)
% Zp1 = 1; %piston
Zp2 = 2*X2D; %x-tilt
Zp3 = 2*Y2D; % y-tilt
Zp4 = sqrt(3)*(2*R2D_sq - 1); %defocus
% Zp5 = 2*sqrt(6)*X2D*Y2D; %astigmatism 1
% Zp6 = sqrt(6)*R2D_sq_minus; %astigmatism 2
Zp7 = sqrt(8)*(3*R2D_sq*Y2D-2*Y2D); %coma y
% Zp8 = sqrt(8)*(3*R2D_sq*X2D-2*X2D); %coma x
% Zp = {Zp1, Zp2, Zp3, Zp4, Zp5, Zp6, Zp7, Zp8};
Zp = {Zp2, Zp3, Zp4, Zp7};

phase_factor = zeros(size(X2D));

for i = 1:length(Zp)
    phase_factor = phase_factor + coeffs(i)*Zp{i};
end

E = exp(1i*K*phase_factor);

% %% PSF parameters (Bessel beam)
% 
% %Bessel beam
% %making the NA higher will make the beam itself smaller
% %the maximum NA the system can have is 0.7
% %our experimental measurements suggest our effective NA is about 0.43 for
% %787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
% %is why we see single atoms)
% NA = 0.69; %Specified NA of hires objective (unitless)
% % NA = 0.51; %Measured effective NA of the hires objective (787 system)
% a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
% f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
% zp = f; %distance from the objective
% PSF = E.*make_bessel(NA, a, zp, f, lambda, 0, 0);

%% preallocate

Ediff = ones(len_x, len_y, len_z); %empty vector to store E field at different points on grid
Ediff(:,:,z0) = E.*E0(:,:,z0);
Idiff = abs(Ediff).^2;
diff = zeros(size(z));

%% Do Fresnel propagation

% Wikipedia to the rescue
% see https://en.wikipedia.org/wiki/Fresnel_diffraction#Alternative_forms

for i = 1:len_z
    if i ~= z0
        [Ediff(:,:,i)] = propTF(Ediff(:,:,z0), length(x), LAMBDA, 5*z(i));
        Idiff(:,:,i) = abs(Ediff(:,:,i)).^2;
        diff(i) = sum(sum(abs(Idiff(:,:,i) - E0(:,:,i))));
    end
end

% out = cell(1,2);
% out{1} = Idiff;
% out{2} = diff;

out = Ediff;

