function out = objective_fcn_Zernike(x, xc)

tmp = Fresnel_prop_Zernike(x, xc);

out = sum(abs(tmp{2})); % sum all the differences