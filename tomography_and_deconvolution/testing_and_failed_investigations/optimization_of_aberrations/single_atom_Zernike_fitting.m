%Script to generate optical potential from DMD
%Tried to fit data to Zernike polynomials
%Did not really work
%Carrie Weidner
%Aarhus University
%13 April 2019

close all; clc

rng('shuffle')

%% Options

plot_DMD = 0; %plots image loaded onto DMD
plot_init = 0;
plot_abs = 1; %plots magnitude of image
plot_phase = 0; %plots phase of image

%% PSF parameters (Bessel beam)

global X_SC

% SCALING
% Set recoil energy, Planck constant, atomic mass = 1
% This sets k (of the lattice) = 2*pi/lambda = sqrt(2)
% Therefore, wavelength (of the lattice) = 2*pi/k = sqrt(2)*pi
% This sets our distance scale, timescale, and energy scale
% NB: The dimple beams are such that for NA = 0.4, a fully illuminated
% superpixel with a dimple power of 1 V (regulation signal) = 7 �W power
% and the effective depth of the trap is about 4 �K, or 41 recoils
% max (regulatable) power in a single superpixel dimple is about 2.5 V,
% or 17.5 �W

% scaling related variables (related to the lattice light) are capitalized
% to distinguish them from the variables related to the projection light
ER = 1; %recoil energy is unity
HBAR = 1; %hbar is unity
M = 1; %mass of Rb-87 atom is unity
K_LATT = sqrt(2*M*ER/(HBAR^2)); %this is a fancy way of writing sqrt(2)
LAMBDA_LATT_REAL = 1064e-9;
LAMBDA_LATT_SC = 2*pi/K_LATT; %sets scaled distance (this is 1064 nm)
X_SC = LAMBDA_LATT_REAL/LAMBDA_LATT_SC; %this is about 239/2 nm

% the bare variable "lambda" is the (scaled) wavelength of the projection
% light ("lambda_proj")
% likewise, the bare variable "k" is the (scaled) wavenumber of the
% projection light
lambda_proj = 780e-9; % atoms
% lambda_proj = 787e-9; % spin-addressing light
% lambda_proj = 940e-9; % potential-changing light
% lambda_proj = 1064e-9; % lattice wavelength
% make lambda global
global LAMBDA K
LAMBDA = lambda_proj/X_SC;
K = 2*pi/LAMBDA;

% set up spatial grid (scaled units)
% for reference, the lattice spacing is LAMBDA_LATT_REAL/2 which is about
% 4.44 in scaled units
% dx = exp_spacing*1e-2/X_SC;
dx = 105e-9/X_SC; % each hires pixel is 105 nm
dy = 105e-9/X_SC;
% the total span of each image is about 20 �m, so 10�m in each direction
px = 119; %number of pixels
% px = 51;
xlim = floor(px/2)*dx;
ylim = floor(px/2)*dy;
dz = LAMBDA_LATT_SC/2; %z-resolution is one plane

% these are big, don't change, and we pass them into
% functions all the time, so let's just make them global
global x y z X2D Y2D z0

x = -xlim:dx:xlim;
len_x = length(x);
y = -ylim:dy:ylim;
len_y = length(y);
% Fresnel propagation will propagate the field to all points along z
z = -3*LAMBDA_LATT_SC/2:dz:LAMBDA_LATT_SC/2;
len_z = length(z);

z0 = find(z == 0);

% finds index of zero point
[~, x0_ind] = min(abs(x));
[~, y0_ind] = min(abs(y));

% 2D field grid
[X2D, Y2D] = meshgrid(x,y);

% %Bessel beam
% %making the NA higher will make the beam itself smaller
% %the maximum NA the system can have is 0.7
% %our experimental measurements suggest our effective NA is about 0.43 for
% %787 nm system, 0.51 for 940 nm system (0.68 for 780 nm imaging system, which
% %is why we see single atoms)
% NA = 0.69; %Specified NA of hires objective (unitless)
% % NA = 0.51; %Measured effective NA of the hires objective (787 system)
% a = 2.54e-2/2/X_SC; %radius of objective (2.54 cm into scaled units)
% f = 12.95e-3/X_SC; %working focal distance (12.95 mm to scaled units)
% zp = f; %distance from the objective
% PSF = make_bessel(NA, a, zp, f, lambda, 0, 0);

E0 = zeros(len_x, len_y, len_z); % this is kinda dirty

% load('imgT.mat');
load imgdiff.mat

for i = 1:len_z
    imgT = imgtosave(:,:,i+5);
    
    %normalize
    % this assumes that the image is square
    floorpx1 = floor(len_x/2);
    floorpx2 = floor(max(size(imgT))/2);
    E0((floorpx1 - floorpx2+1):(floorpx1 + floorpx2+1),(floorpx1 - floorpx2+1):(floorpx1 + floorpx2+1),i) = sqrt(imgT/max(max(imgT)));
    
    %normalize
    E0(:,:,i) = E0(:,:,i)/max(max(abs(E0(:,:,i))));
end
    
xcon = E0; % constraint

%% Optimizer

%build optimizer
options = optimset('Algorithm', 'sqp', 'TolCon', 1e-8, 'Display','none');

% bounds
dim = 4;
a = 0.01;
lb = -Inf*ones(1,dim);
ub = Inf*ones(1,dim);
% lb = -a*ones(1, dim);
% ub = a*ones(1, dim);
% x0 = 2*a*rand(1, dim) - a;
x0 = a*randn(1, dim);

% no linear constraints
A = [];
b = [];
Aeq = [];
beq = [];

runs = 100;

fval_min = Inf;
x_min = Inf*ones(1,dim);

for i = 1:runs
    i
    [xx, fval] = fmincon(@(x) objective_fcn_Zernike(x,xcon), x0, A, b, Aeq, beq, lb, ub, [], options);
     if fval < fval_min
            fval_min = fval;
            x_min = xx;
     end
end

%% run once more to get the right image

% out = Fresnel_prop_Zernike([0,0,0,0,0,0,0,0], E0);

out = Fresnel_prop_Zernike(x_min, E0);

Idiff = out{1};

%% Plotting

% OPTIONS FOR FRESNEL PLOTTING
% this works best if tot = len(z)
nx = 3; %number of plots along the x-direction
ny = 2; %number of plots along y-direction

% displacement points for the x- and y-cuts
xd = 0;
yd = 0;

% aspect ratios
image_aspect = len_x/len_y;

% plot the magnitude of the field at all Fresnel points
if plot_abs == 1
    figure('Position', [0,0,400*ny,300*nx])
    for i = 1:5
        subplot(nx,ny,i)
        imagesc(x*X_SC*1e6,y*X_SC*1e6,...
            Idiff(:,:,i))
        colorbar
        set(gca, 'FontSize', 8, 'FontWeight', 'bold')
        title(['z = ', num2str(z(i)*X_SC*1e6), ' �m'])
        xlabel('x (�m)')
        ylabel('y (�m)')
        pbaspect([1 image_aspect 1])
        max(max(Idiff(:,:,i)))
%         caxis([0 1])
    end
end

x_min

out{2}

sum(abs(out{2}))