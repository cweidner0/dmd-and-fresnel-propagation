%% Bessel function propagation theory
% longitudinal

% everything is analytic here, based on Rosa Gl�ckner's thesis
% find expressions in
% Z:\calculations\181001_tweezer_gauss_vs_bessel\Bessel_beam_calcs_v6_atom.nb

close all
clear all
clc

d_plane = 532e-9;
N_planes = 5;
dz = d_plane/100;
N_planes_half = floor(N_planes/2);

z = -N_planes_half*d_plane:dz:N_planes_half*d_plane;

NA = 0.7;
lambda = 780e-9;

zeta = pi*z*NA^2/(2*lambda);
d_plane_sc = pi*d_plane*NA^2/(2*lambda);

I_plane_n2 = sinc((zeta-2*d_plane_sc)/pi).^2;
I_plane_n1p5 = sinc((zeta-1.5*d_plane_sc)/pi).^2; % for centering
I_plane_n1 = sinc((zeta-d_plane_sc)/pi).^2;
I_plane_n0p5 = sinc((zeta-0.5*d_plane_sc)/pi).^2; % for centering
I_plane_0 = sinc(zeta/pi).^2;
I_plane_0p5 = sinc((zeta+0.5*d_plane_sc)/pi).^2; % for centering
I_plane_1 = sinc((zeta+d_plane_sc)/pi).^2;
I_plane_1p5 = sinc((zeta+1.5*d_plane_sc)/pi).^2; % for centering
I_plane_2 = sinc((zeta+2*d_plane_sc)/pi).^2;

%single stack
f1 = fit(zeta', I_plane_0', 'poly2');

% figure
% plot(f1, zeta, I_plane_0)
% grid on

%double stack
I_plane_2_stack = I_plane_0p5 + I_plane_n0p5;
f2 = fit(zeta', I_plane_2_stack', 'poly2');

% figure
% plot(f2, zeta, I_plane_2_stack)
% grid on

%triple stack (oh shit waddup)
I_plane_3_stack = I_plane_0 + I_plane_n1 + I_plane_1;
f3 = fit(zeta', I_plane_3_stack', 'poly2');

% figure
% plot(f3, zeta, I_plane_3_stack)
% grid on

%quad stackin
I_plane_4_stack = I_plane_0p5 + I_plane_n0p5 + I_plane_1p5 + I_plane_n1p5;
f4 = fit(zeta', I_plane_4_stack', 'poly2');

% figure
% plot(f4, zeta, I_plane_4_stack)
% grid on

%5
I_plane_5_stack = I_plane_0 + I_plane_n1 + I_plane_1 + I_plane_n2 + I_plane_2;
f5 = fit(zeta', I_plane_5_stack', 'poly2');

% figure
% plot(f5, zeta, I_plane_5_stack)
% grid on

curvatures = abs([f1.p1, f2.p1, f3.p1, f4.p1, f5.p1]);
maxes = [f1.p3 - f1.p2^2/(4*f1.p1), f2.p3 - f2.p2^2/(4*f2.p1), f3.p3 - f3.p2^2/(4*f3.p1), f4.p3 - f4.p2^2/(4*f4.p1),...
    f5.p3 - f5.p2^2/(4*f5.p1)];

fcurv = fit((1:5)', curvatures', 'poly2');
fmaxes = fit((1:5)', maxes', 'poly2');

figure(100)
hold on
h1 = plot(fcurv, 1:5, curvatures);
set(h1, 'LineWidth', 1.5, 'MarkerSize', 15)
uistack(h1(1), 'top')
grid on
xlabel('Number of atoms stacked')
ylabel('Curvature of quad fit')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')

figure(101)
hold on
h2 = plot(fmaxes, 1:5, maxes);
set(h2, 'LineWidth', 1.5, 'MarkerSize', 15)
uistack(h2(1), 'top')
grid on
xlabel('Number of atoms stacked')
ylabel('Maximum of quad fit')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')

% now skipping planes
I_skip_1 = I_plane_n1 + I_plane_1;
I_skip_2 = I_plane_n1p5 + I_plane_1p5;
I_skip_3 = I_plane_n2 + I_plane_2;

figure
hold on
plot(zeta, I_plane_n1, 'r--', 'LineWidth',1.5)
plot(zeta, I_plane_1, 'r--', 'LineWidth',1.5)
plot(zeta, I_skip_1, 'r', 'LineWidth', 1.5)
plot(zeta, I_plane_n2, 'b:', 'LineWidth',1.5)
plot(zeta, I_plane_1, 'b:', 'LineWidth',1.5)
plot(zeta, I_skip_2, 'b', 'LineWidth', 1.5)
plot(zeta, I_plane_n2, 'k.-', 'LineWidth',1.5)
plot(zeta, I_plane_2, 'k.-', 'LineWidth',1.5)
plot(zeta, I_skip_3, 'k', 'LineWidth', 1.5)
grid on

f_skip_1 = fit(zeta', I_skip_1', 'poly2');
f_skip_2 = fit(zeta', I_skip_2', 'poly2');
f_skip_3 = fit(zeta', I_skip_3', 'poly2');
skips_curves = abs([f2.p1, f_skip_1.p1, f_skip_2.p1, f_skip_3.p1]);
skips_maxes = [f2.p3 - f2.p2^2/(4*f2.p1), f_skip_1.p3 - f_skip_1.p2^2/(4*f_skip_1.p1), f_skip_2.p3 - f_skip_2.p2^2/(4*f_skip_2.p1), f_skip_3.p3 - f_skip_3.p2^2/(4*f_skip_3.p1)];

fscurv = fit((0:3)', skips_curves', 'poly2');
fsmaxes = fit((0:3)', skips_maxes', 'poly2');

I_skip_1_twice = I_plane_2 + I_plane_0 + I_plane_n2;

% figure
% hs1 = plot(f_skip_1, zeta, I_skip_1);
% set(hs1, 'LineWidth', 1.5)
% uistack(hs1(1), 'top')
% grid on
% 
% figure
% hs2 = plot(f_skip_2, zeta, I_skip_2);
% set(hs2, 'LineWidth', 1.5)
% uistack(hs2(1), 'top')
% grid on
% 
% figure
% hs3 = plot(f_skip_3, zeta, I_skip_3);
% set(hs3, 'LineWidth', 1.5)
% uistack(hs3(1), 'top')
% grid on

figure(200)
hold on
hs1 = plot(fscurv, 0:3, skips_curves);
set(hs1, 'LineWidth', 1.5, 'MarkerSize', 15)
uistack(hs1(1), 'top')
grid on
xlabel('Number of atoms skipped')
ylabel('Curvature of quad fit')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')

figure(201)
hold on
hs2 = plot(fsmaxes, 0:3, skips_maxes);
set(hs2, 'LineWidth', 1.5, 'MarkerSize', 15)
uistack(hs2(1), 'top')
grid on
xlabel('Number of atoms skipped')
ylabel('Maximum of quad fit')
set(gca, 'FontSize', 12, 'FontWeight', 'bold')

figure
hold on
plot(zeta, I_plane_0, 'r--', 'LineWidth', 1.5)
plot(zeta, I_plane_2_stack, 'b--', 'LineWidth', 1.5)
plot(zeta, I_plane_3_stack, 'k--', 'LineWidth', 1.5)
plot(zeta, I_plane_4_stack, 'm--', 'LineWidth', 1.5)
plot(zeta, I_plane_5_stack, 'c--', 'LineWidth', 1.5)
plot(zeta, I_skip_1, 'r', 'LineWidth', 1.5)
plot(zeta, I_skip_2, 'b', 'LineWidth', 1.5)
plot(zeta, I_skip_3, 'k', 'LineWidth', 1.5)
plot(zeta, I_skip_1_twice, 'r:', 'LineWidth', 1.5)
xlabel('\zeta')
ylabel('Amplitude')
legend('00100', '00110', '01110', '01111', '11111', '01010', '01001', '10001', '10101')
grid on
set(gca, 'FontSize', 12, 'FontWeight', 'bold')
title('Comparison of Different Configurations (centered on zero)')